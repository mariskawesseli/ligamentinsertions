import pymeshlab
import numpy as np
import trimesh
import nrrd
import re
import os
import pandas as pd
from tabulate import tabulate
from shutil import copyfile
from openpyxl import load_workbook

# femur
# PCL:                  [1,1,1,1,1,1,1,1,1,1]
# MCL-superficial:      [6,5,6,6,6,6,4,4,5,5]
# MCL-deep:             [3,2+8,5,3,3,2,2,-,3,3]
# posterior oblique:    [7,3,7+8,7,7,5,7,6,7,-]
# ACL:                  [4,6,3,5,4,-(4),-(5),3,4,4]
# LCL (prox):           [5,7,4,4,5,7,6,5,6,6]
# popliteus (dist):     [2,4,2,2,2,3,3,2,2,2]

# tibia
# PCL:                  [5,7,6,5,3,4,4,5,5,4]
# MCL-superficial:      [1,1,1,1,1,1,1,1,1,1]
# MCL-deep:             [3,3+4,8,3,5,3,5,-(4),3,3]
# posterior oblique:    [4,5+6,3+4+5+7,4,4,5,3,2,4,-]
# ACL:                  [6,8,9,6,6,6,6,6,6,5]
# LCL:                  [2,2,2,2,2,2,2,3,2,2]
# popliteus:            [-,-,-,-,-,-,-,-,-,-]


subjects = [9,13,19,23,26,29,32,35,37,41] #9,13,19,23,26,29,32,35,41
segments = ['fibula']  #'femur', fibula, 'tibia','femur'
short = 1
# the mesh number related to the ligament for each specimen
ligaments_fem = [[1,1,1,1,1,1,1,1,1,1],  # PCL
                [6,5,6,6,6,6,4,4,5,5],  # MCLp
                [3,2,5,3,3,2,2,0,3,3],  # MCLd
                [0,8,0,0,0,0,0,0,0,0],  # MCLd2
                [7,3,7,7,7,5,7,6,7,0],  # POL
                [0,0,8,0,0,0,0,0,0,0],  # POL2
                [0,0,0,0,0,0,0,0,0,0],  # POL3
                [0,0,0,0,0,0,0,0,0,0],  # POL4
                [4,6,3,5,4,0,0,3,4,4],  # ACL
                [5,7,4,4,5,7,6,5,6,6],  # LCL
                [2,4,2,2,2,3,3,2,2,2]]  # POP

ligaments_tib = [[5,7,6,5,3,4,4,5,5,4],  # PCL
                [1,1,1,1,1,1,1,1,1,1],  # MCLp
                [3,3,8,3,5,3,5,0,3,3],  # MCLd
                [0,4,0,0,0,0,0,0,0,0],  # MCLd2
                [4,5,3,4,4,5,3,2,4,0],  # POL
                [0,6,4,0,0,0,0,0,0,0],  # POL2
                [0,0,5,0,0,0,0,0,0,0],  # POL3
                [0,0,7,0,0,0,0,0,0,0],  # POL4
                [6,8,9,6,6,6,6,6,6,5],  # ACL
                [2,2,2,2,2,2,2,3,2,2],  # LCL
                [0,0,0,0,0,0,0,0,0,0]]  # POP

ligaments_fib = [[0,0,0,0,0,0,0,0,0,0],  # PCL
                [0,0,0,0,0,0,0,0,0,0],  # MCLp
                [0,0,0,0,0,0,0,0,0,0],  # MCLd
                [0,0,0,0,0,0,0,0,0,0],  # MCLd2
                [0,0,0,0,0,0,0,0,0,0],  # POL
                [0,0,0,0,0,0,0,0,0,0],  # POL2
                [0,0,0,0,0,0,0,0,0,0],  # POL3
                [0,0,0,0,0,0,0,0,0,0],  # POL4
                [0,0,0,0,0,0,0,0,0,0],  # ACL
                [2,2,2,2,2,2,2,3,2,2],  # LCL
                [0,0,0,0,0,0,0,0,0,0]]  # POP

book = load_workbook(os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData","surfaces4.xlsx"))
writer = pd.ExcelWriter(os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData","surfaces4.xlsx"), engine='openpyxl')
writer.book = book

for segment in segments:
    surface = np.empty((13, 10))
    surface[:] = np.nan
    center = np.empty((13, 10))
    center[:] = np.nan
    ML_size = np.empty((1, 10))
    ML_size[:] = np.nan
    AP_size = np.empty((1, 10))
    AP_size[:] = np.nan
    SI_size = np.empty((1, 10))
    SI_size[:] = np.nan
    bb_max = np.empty((3, 10))
    bb_max[:] = np.nan
    bb_min = np.empty((3, 10))
    bb_min[:] = np.nan
    ML_size_med = np.empty((1, 10))
    ML_size_med[:] = np.nan
    AP_size_med = np.empty((1, 10))
    AP_size_med[:] = np.nan
    SI_size_med = np.empty((1, 10))
    SI_size_med[:] = np.nan
    bb_max_med = np.empty((3, 10))
    bb_max_med[:] = np.nan
    bb_min_med = np.empty((3, 10))
    bb_min_med[:] = np.nan
    ML_size_lat = np.empty((1, 10))
    ML_size_lat[:] = np.nan
    AP_size_lat = np.empty((1, 10))
    AP_size_lat[:] = np.nan
    SI_size_lat = np.empty((1, 10))
    SI_size_lat[:] = np.nan
    bb_max_lat = np.empty((3, 10))
    bb_max_lat[:] = np.nan
    bb_min_lat = np.empty((3, 10))
    bb_min_lat[:] = np.nan
    dist_to_edge = np.empty((13, 10, 3))
    dist_to_edge[:] = np.nan
    perc_of_len = np.empty((13, 10, 3))
    perc_of_len[:] = np.nan
    perc_of_len_med = np.empty((13, 10, 3))
    perc_of_len_med[:] = np.nan
    perc_of_len_lat = np.empty((13, 10, 3))
    perc_of_len_lat[:] = np.nan
    center = np.empty((13, 10, 3))
    center[:] = np.nan
    if segment == 'femur':
        ligaments = ligaments_fem
    elif segment == 'tibia':
        ligaments = ligaments_tib
    else:
        ligaments = ligaments_fib

    for ind, subject in enumerate(subjects):
        path = os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData", str(subject))
        if subject in [9,13,26,29,32]:
            side = 'R'
            reflect = ''
        else:
            side = 'L'
            reflect = '.reflect'

        # transform femur to local coordinate system to get anatomical directions
        if segment=='fibula':
            rot_mat = np.linalg.inv(np.loadtxt(path + '\Segmentation_tibia_resample._ACS.txt'))
        else:
            rot_mat = np.linalg.inv(np.loadtxt(path + '\Segmentation_' + segment + '_resample._ACS.txt'))
        mesh2 = r'C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData/' + str(subject) + '\Segmentation_' + segment + '_sep.stl'
        ms5 = pymeshlab.MeshSet()
        # ms5.load_new_mesh(mesh2)
        # ms5.apply_filter('matrix_set_copy_transformation', transformmatrix=rot_mat)
        # ms5.save_current_mesh(path + '\Segmentation_' + segment + '_sep_transform.stl', binary=False)
        if segment=='tibia':
            ms5.load_new_mesh(path + '\Segmentation_' + segment + '_sep_transform.stl')
        else:
            ms5.load_new_mesh(path + '\Segmentation_' + segment + '_transform.stl')
        geometric_measures_femur = ms5.apply_filter('compute_geometric_measures')
        ML_size[0, ind] = geometric_measures_femur['bbox'].dim_x()
        AP_size[0, ind] = geometric_measures_femur['bbox'].dim_y()
        SI_size[0, ind] = geometric_measures_femur['bbox'].dim_z()
        # print('ML width femur: ' + str(ML_size[ind]) + ' mm')
        # print('AP width femur: ' + str(AP_size[ind]) + ' mm')
        bb_max[:, ind] = np.max(ms5.current_mesh().vertex_matrix(), 0)
        bb_min[:, ind] = np.min(ms5.current_mesh().vertex_matrix(), 0)

        if side == 'R':
            ms5.conditional_vertex_selection(condselect="x<0") #select lat (41=L)
            ms5.apply_filter('move_selected_vertices_to_another_layer') #0=med, 1=lat
        else:
            ms5.conditional_vertex_selection(condselect="x>0")  # select lat (41=L)
            ms5.apply_filter('move_selected_vertices_to_another_layer')  # 0=med, 1=lat

        ms5.set_current_mesh(0)
        geometric_measures_femur = ms5.apply_filter('compute_geometric_measures')
        ML_size_med[0,ind] = geometric_measures_femur['bbox'].dim_x()
        AP_size_med[0,ind] = geometric_measures_femur['bbox'].dim_y()
        SI_size_med[0,ind] = geometric_measures_femur['bbox'].dim_z()
        # print('ML width femur: ' + str(ML_size[ind]) + ' mm')
        # print('AP width femur: ' + str(AP_size[ind]) + ' mm')
        bb_max_med[:, ind] = np.max(ms5.current_mesh().vertex_matrix(), 0)
        bb_min_med[:, ind] = np.min(ms5.current_mesh().vertex_matrix(), 0)

        ms5.set_current_mesh(1)
        geometric_measures_femur = ms5.apply_filter('compute_geometric_measures')
        ML_size_lat[0, ind] = geometric_measures_femur['bbox'].dim_x()
        AP_size_lat[0, ind] = geometric_measures_femur['bbox'].dim_y()
        SI_size_lat[0, ind] = geometric_measures_femur['bbox'].dim_z()
        # print('ML width femur: ' + str(ML_size[ind]) + ' mm')
        # print('AP width femur: ' + str(AP_size[ind]) + ' mm')
        bb_max_lat[:, ind] = np.max(ms5.current_mesh().vertex_matrix(), 0)
        bb_min_lat[:, ind] = np.min(ms5.current_mesh().vertex_matrix(), 0)

        # determine surface area attachments
        for lig in range(0, 11):
            lig_no = ligaments[lig][ind]
            if not lig_no == 0:
                ms4 = pymeshlab.MeshSet()
                if segment == 'fibula':
                    ms4.load_new_mesh(path + '\Segmentation_' + segment + '_area' + str(lig_no) + '_transform.stl')
                else:
                    ms4.load_new_mesh(path + '\Segmentation_' + segment + '_area' + str(lig_no) + '.stl')
                    ms4.apply_filter('matrix_set_copy_transformation', transformmatrix=rot_mat)
                geometric_measures = ms4.apply_filter('compute_geometric_measures')
                surface[lig,ind] = geometric_measures['surface_area']
                # print('Surface area femur ligament' + str(lig_no) + ': ' + str(surface) + ' mm2')
                center[lig,ind,:] = geometric_measures['shell_barycenter']
                if side == 'R':
                    dist_to_edge[lig, ind, :] = center[lig, ind, :] - bb_min[:, ind]
                    dist_to_edge[lig, ind, 0] = center[lig, ind, 0] - bb_max[0, ind]
                else:
                    dist_to_edge[lig,ind,:] = center[lig, ind,:] - bb_min[:, ind]

                if segment == 'tibia' or segment == 'fibula':
                    dist_to_edge[lig, ind, 2] = center[lig, ind, 2] - bb_max[2, ind]
                perc_of_len[lig,ind,:] = abs(dist_to_edge[lig,ind,:]/(ML_size[0,ind],AP_size[0,ind],AP_size[0,ind]))
                perc_of_len_med[lig, ind, :] = abs(dist_to_edge[lig, ind, :] / (ML_size_med[0, ind], AP_size_med[0, ind], AP_size_med[0, ind]))
                perc_of_len_lat[lig, ind, :] = abs(dist_to_edge[lig, ind, :] / (ML_size_lat[0, ind], AP_size_lat[0, ind], AP_size_lat[0, ind]))
        for lig_comb in [2, 4]:
            lig_no = ligaments[lig_comb][ind]
            if not lig_no == 0:
                if lig_comb == 2:
                    lig = 11
                    ms4 = pymeshlab.MeshSet()
                    ms4.load_new_mesh(path + '\Segmentation_' + segment + '_area' + str(lig_no) + '.stl')
                    try:
                        ms4.load_new_mesh(path + '\Segmentation_' + segment + '_area' + str(lig_no + 1) + '.stl')
                    except:
                        print('')
                if lig_comb == 4:
                    lig = 12
                    ms4 = pymeshlab.MeshSet()
                    if segment=='fibula':
                        ms4.load_new_mesh(path + '\Segmentation_tibia_area' + str(lig_no) + '.stl')
                    else:
                        ms4.load_new_mesh(path + '\Segmentation_' + segment + '_area' + str(lig_no) + '.stl')
                    try:
                        ms4.load_new_mesh(path + '\Segmentation_' + segment + '_area' + str(lig_no + 1) + '.stl')
                    except:
                        print('')
                    try:
                        ms4.load_new_mesh(path + '\Segmentation_' + segment + '_area' + str(lig_no + 2) + '.stl')
                    except:
                        print('')
                    try:
                        ms4.load_new_mesh(path + '\Segmentation_' + segment + '_area' + str(lig_no + 3) + '.stl')
                    except:
                        print('')
                ms4.apply_filter('flatten_visible_layers', deletelayer=True)
                ms4.apply_filter('matrix_set_copy_transformation', transformmatrix=rot_mat)
                geometric_measures = ms4.apply_filter('compute_geometric_measures')
                surface[lig, ind] = geometric_measures['surface_area']
                # print('Surface area femur ligament' + str(lig_no) + ': ' + str(surface) + ' mm2')
                center[lig, ind, :] = geometric_measures['shell_barycenter']
                dist_to_edge[lig, ind, :] = center[lig, ind, :] - bb_min[:, ind]
                if segment == 'tibia' or segment == 'fibula':
                    dist_to_edge[lig, ind, 2] = center[lig, ind, 2] - bb_max[2, ind]
                perc_of_len[lig, ind, :] = abs(dist_to_edge[lig, ind, :] / (ML_size[0, ind], AP_size[0, ind], AP_size[0, ind]))
                perc_of_len_med[lig, ind, :] = abs(dist_to_edge[lig, ind, :] / (
                    ML_size_med[0, ind],  AP_size_med[0, ind], AP_size_med[0, ind]))
                perc_of_len_lat[lig, ind, :] = abs(dist_to_edge[lig, ind, :] / (
                    ML_size_lat[0, ind], AP_size_lat[0, ind], AP_size_lat[0, ind]))

    df = pd.DataFrame({'PCLx': 100-perc_of_len[0,:,0]*100,
                       'MCL-sx': 100-perc_of_len[1, :, 0]*100,
                       'MCL-d1x': 100-perc_of_len[2, :, 0]*100,
                       'MCL-d2x': 100-perc_of_len[3, :, 0]*100,
                       'MCL-d2': 100-perc_of_len[11, :, 0]*100,
                       'posterior oblique1x': 100-perc_of_len[4, :, 0]*100,
                       'posterior oblique2x': 100-perc_of_len[5, :, 0]*100,
                       'posterior oblique3x': 100-perc_of_len[6, :, 0]*100,
                       'posterior oblique4x': 100-perc_of_len[7, :, 0]*100,
                       'posterior obliquex': 100-perc_of_len[12, :, 0]*100,
                       'ACLx': 100-perc_of_len[8, :, 0]*100,
                       'LCLx': 100-perc_of_len[9, :, 0]*100,
                       'popliteusx': 100-perc_of_len[10, :, 0]*100,
                       'PCLy': 100-perc_of_len[0,:,1]*100,
                       'MCL-sy': 100-perc_of_len[1, :, 1]*100,
                       'MCL-d1y': 100-perc_of_len[2, :, 1]*100,
                       'MCL-d2y': 100-perc_of_len[3, :, 1]*100,
                       'MCL-dy': 100-perc_of_len[11, :, 1]*100,
                       'posterior oblique1y': 100-perc_of_len[4, :, 1]*100,
                       'posterior oblique2y': 100-perc_of_len[5, :, 1]*100,
                       'posterior oblique3y': 100-perc_of_len[6, :, 1]*100,
                       'posterior oblique4y': 100-perc_of_len[7, :, 1]*100,
                       'posterior obliquey': 100-perc_of_len[12, :, 1]*100,
                       'ACLy': 100-perc_of_len[8, :, 1]*100,
                       'LCLy': 100-perc_of_len[9, :, 1]*100,
                       'popliteusy': 100-perc_of_len[10, :, 1]*100,
                       'PCLz': perc_of_len[0,:,2]*100,
                       'MCL-sz': perc_of_len[1,:,2]*100,
                       'MCL-d1z': perc_of_len[2,:,2]*100,
                       'MCL-d2z': perc_of_len[3, :, 2]*100,
                       'MCL-dz': perc_of_len[11, :, 2]*100,
                       'posterior oblique1z': perc_of_len[4,:,2]*100,
                       'posterior oblique2z': perc_of_len[5, :, 2]*100,
                       'posterior oblique3z': perc_of_len[6, :, 2]*100,
                       'posterior oblique4z': perc_of_len[7, :, 2]*100,
                       'posterior obliquez': perc_of_len[12, :, 2]*100,
                       'ACLz': perc_of_len[8,:,2]*100,
                       'LCLz': perc_of_len[9,:,2]*100,
                       'popliteusz': perc_of_len[10,:,2]*100
                         })
    means = df.mean(skipna=True).round(decimals=1)
    std = '±' + df.std(skipna=True).round(decimals=1).astype(str)
    r1 = '(' + df.max(skipna=True).round(decimals=1).astype(str) + '-'
    r2 = df.min(skipna=True).round(decimals=1).astype(str) + ')'
    # print(tabulate(df, headers='keys', tablefmt='psql'))

    summary_ave_data = df.copy()
    summary_ave_data = summary_ave_data.append(means, ignore_index=True)
    summary_ave_data = summary_ave_data.append(std, ignore_index=True)
    summary_ave_data = summary_ave_data.append(r1, ignore_index=True)
    summary_ave_data = summary_ave_data.append(r2, ignore_index=True)
    summary_ave_data = summary_ave_data.rename({10: 'mean', 11: 'std', 12: 'range1', 13: 'range2'}, axis='index')
    summary_ave_data = summary_ave_data.T
    summary_ave_data.to_excel(writer, sheet_name='perc_of_len ' + segment)

    means_table = df.mean(skipna=True).round(decimals=1).astype(str) + ' ±' + df.std(skipna=True).round(
        decimals=1).astype(
        str) + \
                  ' (' + df.min(skipna=True).round(decimals=1).astype(str) + '-' + df.max(skipna=True).round(
        decimals=1).astype(str) + ')'

    table_data = pd.DataFrame(means_table)
    # table_data = table_data.append(means_table, ignore_index=True)
    # table_data = table_data.T
    table_data = table_data.rename({0: 'POSITION (MEAN±STD, RANGE)'}, axis='columns')
    table_data.to_excel(writer, sheet_name='table perc_of_len ' + segment)

    df = pd.DataFrame({'PCLy': 100-perc_of_len_med[0, :, 1]*100,
                       'MCL-sy': 100-perc_of_len_med[1, :, 1]*100,
                       'MCL-d1y': 100-perc_of_len_med[2, :, 1]*100,
                       'MCL-d2y': 100-perc_of_len_med[3, :, 1]*100,
                       'MCL-dy': 100-perc_of_len_med[11, :, 1]*100,
                       'posterior oblique1y': 100-perc_of_len_med[4, :, 1]*100,
                       'posterior oblique2y': 100-perc_of_len_med[5, :, 1]*100,
                       'posterior oblique3y': 100-perc_of_len_med[6, :, 1]*100,
                       'posterior oblique4y': 100-perc_of_len_med[7, :, 1]*100,
                       'posterior obliquey': 100-perc_of_len_med[12, :, 1]*100,
                       'ACLy': 100-perc_of_len_lat[8, :, 1]*100,
                       'LCLy': 100-perc_of_len_lat[9, :, 1]*100,
                       'popliteusy': 100-perc_of_len_lat[10, :, 1]*100,
                       'PCLz': perc_of_len_med[0, :, 2]*100,
                       'MCL-sz': perc_of_len_med[1, :, 2]*100,
                       'MCL-d1z': perc_of_len_med[2, :, 2]*100,
                       'MCL-d2z': perc_of_len_med[3, :, 2]*100,
                       'MCL-dz': perc_of_len_med[11, :, 2]*100,
                       'posterior oblique1z': perc_of_len_med[4, :, 2]*100,
                       'posterior oblique2z': perc_of_len_med[5, :, 2]*100,
                       'posterior oblique3z': perc_of_len_med[6, :, 2]*100,
                       'posterior oblique4z': perc_of_len_med[7, :, 2]*100,
                       'posterior obliquez': perc_of_len_med[12, :, 2]*100,
                       'ACLz': perc_of_len_lat[8, :, 2]*100,
                       'LCLz': perc_of_len_lat[9, :, 2]*100,
                       'popliteusz': perc_of_len_lat[10, :, 2]*100
                       })
    means = df.mean(skipna=True).round(decimals=1)
    std = '±' + df.std(skipna=True).round(decimals=1).astype(str)
    r1 = '(' + df.max(skipna=True).round(decimals=1).astype(str) + '-'
    r2 = df.min(skipna=True).round(decimals=1).astype(str) + ')'
    # print(tabulate(df, headers='keys', tablefmt='psql'))

    summary_ave_data = df.copy()
    summary_ave_data = summary_ave_data.append(means, ignore_index=True)
    summary_ave_data = summary_ave_data.append(std, ignore_index=True)
    summary_ave_data = summary_ave_data.append(r1, ignore_index=True)
    summary_ave_data = summary_ave_data.append(r2, ignore_index=True)
    summary_ave_data = summary_ave_data.rename({10: 'mean', 11: 'std', 12: 'range1', 13: 'range2'}, axis='index')
    summary_ave_data = summary_ave_data.T
    summary_ave_data.to_excel(writer, sheet_name='perc_of_len med_lat ' + segment)

    means_table = df.mean(skipna=True).round(decimals=1).astype(str) + ' ±' + df.std(skipna=True).round(decimals=1).astype(
        str) + \
            ' (' + df.min(skipna=True).round(decimals=1).astype(str) + '-' + df.max(skipna=True).round(
        decimals=1).astype(str) + ')'

    table_data = pd.DataFrame(means_table)
    # table_data = table_data.append(means_table, ignore_index=True)
    table_data = table_data.rename({0: 'POSITION (MEAN±STD, RANGE)'}, axis='columns')
    # table_data = table_data.T
    table_data.to_excel(writer, sheet_name='table perc_of_len med_lat ' + segment)

    df = pd.DataFrame({'PCLx': 100-dist_to_edge[0,:,0],
                       'MCL-sx': 100-dist_to_edge[1, :, 0],
                       'MCL-d1x': 100-dist_to_edge[2, :, 0],
                       'MCL-d2x': 100-dist_to_edge[3, :, 0],
                       'MCL-dx': 100-dist_to_edge[11, :, 0],
                       'posterior oblique1x': 100-dist_to_edge[4, :, 0],
                       'posterior oblique2x': 100-dist_to_edge[5, :, 0],
                       'posterior oblique3x': 100-dist_to_edge[6, :, 0],
                       'posterior oblique4x': 100-dist_to_edge[7, :, 0],
                       'posterior obliquex': 100-dist_to_edge[12, :, 0],
                       'ACLx': 100-dist_to_edge[8, :, 0],
                       'LCLx': 100-dist_to_edge[9, :, 0],
                       'popliteusx': 100-dist_to_edge[10, :, 0],
                       'PCLy': 100-dist_to_edge[0,:,1],
                       'MCL-sy': 100-dist_to_edge[1, :, 1],
                       'MCL-d1y': 100-dist_to_edge[2, :, 1],
                       'MCL-d2y': 100-dist_to_edge[3, :, 1],
                       'MCL-dy': 100-dist_to_edge[11, :, 1],
                       'posterior oblique1y': 100-dist_to_edge[4, :, 1],
                       'posterior oblique2y': 100-dist_to_edge[5, :, 1],
                       'posterior oblique3y': 100-dist_to_edge[6, :, 1],
                       'posterior oblique4y': 100-dist_to_edge[7, :, 1],
                       'posterior obliquey': 100-dist_to_edge[12, :, 1],
                        'ACLy': 100-dist_to_edge[8, :, 1],
                       'LCLy': 100-dist_to_edge[9, :, 1],
                       'popliteusy': 100-dist_to_edge[10, :, 1],
                       'PCLz': dist_to_edge[0,:,2],
                       'MCL-sz': dist_to_edge[1,:,2],
                       'MCL-d1z': dist_to_edge[2,:,2],
                       'MCL-d2z': dist_to_edge[3, :, 2],
                       'MCL-dz': dist_to_edge[11, :, 2],
                       'posterior oblique1z': dist_to_edge[4,:,2],
                       'posterior oblique2z': dist_to_edge[5, :, 2],
                       'posterior oblique3z': dist_to_edge[6, :, 2],
                       'posterior oblique4z': dist_to_edge[7, :, 2],
                       'posterior obliquez': dist_to_edge[12, :, 2],
                       'ACLz': dist_to_edge[8,:,2],
                       'LCLz': dist_to_edge[9,:,2],
                       'popliteusz': dist_to_edge[10,:,2]
                         })
    means = df.mean(skipna=True).round(decimals=1)
    std = '±' + df.std(skipna=True).round(decimals=1).astype(str)
    r1 = '(' + df.max(skipna=True).round(decimals=1).astype(str) + '-'
    r2 = df.min(skipna=True).round(decimals=1).astype(str) + ')'
    # print(tabulate(df, headers='keys', tablefmt='psql'))

    summary_ave_data = df.copy()
    summary_ave_data = summary_ave_data.append(means, ignore_index=True)
    summary_ave_data = summary_ave_data.append(std, ignore_index=True)
    summary_ave_data = summary_ave_data.append(r1, ignore_index=True)
    summary_ave_data = summary_ave_data.append(r2, ignore_index=True)
    summary_ave_data = summary_ave_data.rename({10: 'mean', 11: 'std', 12: 'range1', 13: 'range2'}, axis='index')

    summary_ave_data = summary_ave_data.T
    summary_ave_data.to_excel(writer, sheet_name='distance_to_edge ' + segment)

    means_table = df.mean(skipna=True).round(decimals=1).astype(str) + ' ±' + df.std(skipna=True).round(
        decimals=1).astype(
        str) + \
                  ' (' + df.min(skipna=True).round(decimals=1).astype(str) + '-' + df.max(skipna=True).round(
        decimals=1).astype(str) + ')'

    table_data = pd.DataFrame(means_table)
    # table_data = table_data.append(means_table, ignore_index=True)
    table_data = table_data.rename({0: 'POSITION (MEAN±STD, RANGE)'}, axis='columns')
    # table_data = table_data.T
    table_data.to_excel(writer, sheet_name='table distance_to_edge ' + segment)

    MCLd = np.nansum([surface[2, :], surface[3, :]], 0)
    MCLd[MCLd == 0] = 'nan'
    pol = np.nansum([surface[4, :],surface[5, :],surface[6, :],surface[7, :]],0)
    pol[pol == 0] = 'nan'
    df = pd.DataFrame({'PCL': surface[0,:],
                         'MCL-s': surface[1,:],
                         'MCL-d1:': surface[2,:],
                        'MCL-d2:': surface[3, :],
                       'MCL-d:': MCLd,
                         'posterior oblique1': surface[4,:],
                       'posterior oblique2': surface[5, :],
                       'posterior oblique3': surface[6, :],
                       'posterior oblique4': surface[7, :],
                       'posterior oblique': pol,
                         'ACL': surface[8,:],
                         'LCL': surface[9,:],
                         'popliteus': surface[10,:],
                         })
    means = df.mean(skipna=True).round(decimals=1).astype(str) + ' ±' + df.std(skipna=True).round(decimals=1).astype(str) + \
            ' (' + df.min(skipna=True).round(decimals=1).astype(str) + '-' + df.max(skipna=True).round(decimals=1) .astype(str)+ ')'
    # print(tabulate(df, headers='keys', tablefmt='psql'))

    summary_ave_data = df.copy()
    summary_ave_data = summary_ave_data.append(means,ignore_index=True)
    # summary_ave_data = summary_ave_data.append(std,ignore_index=True)
    # summary_ave_data = summary_ave_data.append(r1, ignore_index=True)
    # summary_ave_data = summary_ave_data.append(r2, ignore_index=True)
    summary_ave_data = summary_ave_data.rename({10: 'ATTACHMENT AREA (MEAN±STD, RANGE)'},axis='index')

    summary_ave_data = summary_ave_data.T
    summary_ave_data.to_excel(writer, sheet_name='surface ' + segment)

writer.save()
writer.close()
