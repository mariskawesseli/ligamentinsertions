import pandas as pd
import os
import trimesh
import numpy as np
import matplotlib.path as plt


def heron(a, b, c):
	s = (a + b + c) / 2
	area = (s * (s - a) * (s - b) * (s - c)) ** 0.5
	return area


def distance3d(x1, y1, z1, x2, y2, z2):
	a = (x1 - x2) ** 2 + (y1 - y2) ** 2 + (z1 - z2) ** 2
	d = a ** 0.5
	return d


def area(x1, y1, z1, x2, y2, z2, x3, y3, z3):
	a = distance3d(x1, y1, z1, x2, y2, z2)
	b = distance3d(x2, y2, z2, x3, y3, z3)
	c = distance3d(x3, y3, z3, x1, y1, z1)
	A = heron(a, b, c)
	return A


# print("area of triangle is %r " %A)

# A utility function to calculate area
# of triangle formed by (x1, y1),
# (x2, y2) and (x3, y3)

# def area(x1, y1, x2, y2, x3, y3):
# 	return abs((x1 * (y2 - y3) + x2 * (y3 - y1)
# 	            + x3 * (y1 - y2)) / 2.0)


# A function to check whether point P(x, y)
# lies inside the triangle formed by
# A(x1, y1), B(x2, y2) and C(x3, y3)
def isInside(p1, p2, p3, p):
	x1 = p1[0]
	y1 = p1[1]
	z1 = p1[2]
	x2 = p2[0]
	y2 = p2[1]
	z2 = p2[2]
	x3 = p3[0]
	y3 = p3[1]
	z3 = p3[2]
	x = p[0]
	y = p[1]
	z = p[2]

	# Calculate area of triangle ABC
	A = area(x1, y1, z1, x2, y2, z2, x3, y3, z3)

	# Calculate area of triangle PBC
	A1 = area(x, y, z, x2, y2, z2, x3, y3, z3)

	# Calculate area of triangle PAC
	A2 = area(x1, y1, z1, x, y, z, x3, y3, z3)

	# Calculate area of triangle PAB
	A3 = area(x1, y1, z1, x2, y2, z2, x, y, z)

	# Check if sum of A1, A2 and A3
	# is same as A
	if abs(A - (A1 + A2 + A3) < 1e-6):
		return True
	else:
		return False


def intersection(planeNormal, planePoint, rayDirection, rayPoint):
	epsilon = 1e-6

	# Define plane
	# planeNormal = np.array([0, 0, 1])
	# planePoint = np.array([0, 0, 5]) #Any point on the plane

	# Define ray
	# rayDirection = np.array([0, -1, -1])
	# rayPoint = np.array([0, 0, 10]) #Any point along the ray

	ndotu = planeNormal.dot(rayDirection)

	if abs(ndotu) < epsilon:
		intersect = 0
	else:
		w = rayPoint - planePoint[0, :]
		si = -planeNormal.dot(w) / ndotu
		Psi = w + si * rayDirection + planePoint[0, :]
		if isInside(planePoint[0], planePoint[1], planePoint[2], Psi) == False:
			intersect = 0
		else:
			intersect = Psi[0]

	return intersect

intersection(np.array([0,0,1]), np.array([(1,1,0),(1,2,0),(2,1.5,0)]), np.array([0,0,1]), np.array((1.5,1.5,1)))
