import pymeshlab
import os.path
import trimesh
import numpy as np


def cylinder_between(p1, p2, r, path):
    dx = p2[0] - p1[0]
    dy = p2[1] - p1[1]
    dz = p2[2] - p1[2]
    dist = np.sqrt(dx**2 + dy**2 + dz**2)+0.5

    phi = np.arctan2(dy, dx)
    theta = np.arccos(dz/dist)

    T = trimesh.transformations.translation_matrix([dx/2 + p1[0], dy/2 + p1[1], dz/2 + p1[2]])
    origin, xaxis, yaxis, zaxis = [0,0,0], [1, 0, 0], [0, 1, 0], [0, 0, 1]
    Rz = trimesh.transformations.rotation_matrix(phi, zaxis)
    Ry = trimesh.transformations.rotation_matrix(theta, yaxis)
    R = trimesh.transformations.concatenate_matrices(T,Rz, Ry)

    cylinder = trimesh.creation.cylinder(r, height=dist, sections=None, segment=None, transform=R)
    cylinder.export(path)


def cut_mesh(out2, path, i):
    # load wire mesh in new meshlab file
    ms2 = pymeshlab.MeshSet()
    ms2.load_new_mesh(mesh1)
    # translate wire to mesh in direction of the normal of the plane and copy to create thick wire
    ms2.transform_translate_center_set_origin(traslmethod=0, axisx=-plane_normal[0, 0] ,
                                              axisy=-plane_normal[0, 1] ,
                                              axisz=-plane_normal[0, 2] ,
                                              freeze=True, alllayers=False)
    factor = 0.1
    no = 0
    for ind in range(0, 24):
        ms2.load_new_mesh(mesh1)
        factor += 0.25
        ms2.transform_translate_center_set_origin(traslmethod=0, axisx=-plane_normal[0, 0] * out2['mean'] * factor,
                                                  axisy=-plane_normal[0, 1] * out2['mean'] * factor,
                                                  axisz=-plane_normal[0, 2] * out2['mean'] * factor,
                                                  freeze=True, alllayers=False)
        ms2.apply_filter('mesh_boolean_union', first_mesh=no, second_mesh=no + 1)
        no += 2

    # save thick wire
    ms2.save_current_mesh(path + '\Segmentation_' + segment + '_wire' + str(i) + 'union.stl', binary=False)

    # load thick wire and area in new meshlab file
    ms4 = pymeshlab.MeshSet()
    ms4.load_new_mesh(path + '\Segmentation_' + segment + '_wire' + str(i) + 'union.stl')
    ms4.load_new_mesh(path + '\Segmentation_' + segment + '_area' + str(i) + '.stl')

    # compute signed distance
    # out3 = ms4.apply_filter('distance_from_reference_mesh', measuremesh=1, refmesh=0, signeddist=False)
    out3 = ms4.apply_filter('distance_from_reference_mesh', measuremesh=1, refmesh=0, signeddist=True)

    # select and delete vertices with negative distance
    # ms4.conditional_vertex_selection(condselect="q<0.15")
    ms4.conditional_vertex_selection(condselect="(q <0)")  # "(q <0) && (q >-0.4)"
    ms4.delete_selected_vertices()
    # split mesh
    out4 = ms4.apply_filter('split_in_connected_components')

    return ms4


subjects = [19]  # 9,13,19,23,26,29,32,35,37,41
segments = ['femur']  # ['femur', 'tibia']  # ['femur']  #
no_subjects = len(subjects)
no_segments = len(segments)

for subject in subjects:
    for segment in segments:

        # # split wires to seperate files
        path = r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData/" + str(subject) + '/'
        # mesh_all = path + 'Segmentation_' + segment + '_wires.stl'
        # ms3 = pymeshlab.MeshSet()
        # ms3.load_new_mesh(mesh_all)
        # ms3.apply_filter('split_in_connected_components')
        #
        # no_meshes = ms3.number_meshes()
        # for i in range(1, no_meshes):
        # 	ms3.set_current_mesh(i)
        # 	no_vertices = ms3.mesh(i).vertex_matrix().shape[0]
        # 	if no_vertices < 50:
        # 		ms3.delete_current_mesh()
        # 	else:
        # 		if not os.path.isfile(path + '\Segmentation_' + segment + '_wires' + str(i-1) + '.stl') and not i == 1:
        # 			no = i-1
        # 		else:
        # 			no = i
        # 		ms3.save_current_mesh(path + '\Segmentation_' + segment + '_wires' + str(no) + '.stl')
        #
        # no_meshes = no
        # # combine tibia and fibula
        # if segment == 'tibia':
        # 	ms1 = pymeshlab.MeshSet()
        # 	ms1.load_new_mesh(path + 'Segmentation_tibia_sep.stl')
        # 	ms1.load_new_mesh(path + 'Segmentation_fibula.stl')
        # 	ms1.apply_filter('mesh_boolean_union', first_mesh=0, second_mesh=1)
        # 	ms1.save_current_mesh(path + 'Segmentation_tibia.stl', binary=False)

        # run over all wires
        error = []
        mesh2 = path + 'Segmentation_' + segment + '.stl'
        # ms5 = pymeshlab.MeshSet()
        # ms5.load_new_mesh(mesh2)
        # ms5.apply_filter('uniform_mesh_resampling', cellsize=1)
        # # ms5.apply_filter('transform_rotate', rotaxis=2, angle=180)
        # ms5.save_current_mesh(path + '\Segmentation_' + segment + '_resample.stl', binary=False)

        for i in range(3,4): #range(1, no_meshes+1):  #range(3,4): #range(5,no_meshes+1): #
            mesh1 = path + '\Segmentation_' + segment + '_wires' + str(i) + '.stl'

            # load meshes in new meshlab file
            ms = pymeshlab.MeshSet()
            ms.load_new_mesh(mesh1)
            ms.load_new_mesh(mesh2)

            # calculate Hausdorff distance in both directions
            out2 = ms.apply_filter('hausdorff_distance', targetmesh=1, sampledmesh=0, savesample=False, maxdist=9)
            out1 = ms.apply_filter('hausdorff_distance', targetmesh=0, sampledmesh=1, savesample=False, maxdist=9)

            # select and delete all vertices far from the wire
            ms.conditional_vertex_selection(condselect="q>8.9")
            ms.delete_selected_vertices()
            # save section containing area
            ms.save_current_mesh(path + '\Segmentation_' + segment + '_area' + str(i) + '.stl')
            #fit plane through section containing area
            # ms.set_current_mesh(new_curr_id=0)
            ms.select_all()
            ms.fit_a_plane_to_selection()
            plane_normal = ms.mesh(2).vertex_normal_matrix()

            ms4 = cut_mesh(out2, path, i)

            # check the number of components and remove the ones with few vertices
            no_meshes = ms4.number_meshes()
            meshes_to_remove = no_meshes-4
            if meshes_to_remove > 0:
                for ind in range(0,no_meshes):
                    no_vertices = ms4.mesh(ind).vertex_matrix().shape[0]
                    if no_vertices < 50:
                        ms4.set_current_mesh(ind)
                        ms4.delete_current_mesh()
                    else:
                        last_mesh = ind
            else:
                last_mesh = 3
            # check the number of meshes
            # if there are less than 4, split is not done in 2 large surfaces and surface needs to be closed
            no_meshes = ms4.number_meshes()
            if no_meshes < 4: #no_vertices < 10:
                # load wire in new meshset
                ms6 = pymeshlab.MeshSet()
                ms6.load_new_mesh(path + '\Segmentation_' + segment + '_wires' + str(i) + '.stl')
                # find for each point the largest distance on mesh
                dist_matrix = []
                dist_matrix_ind = []
                start_ind = []
                verts = ms6.mesh(0).vertex_matrix()
                for ind in range(0, len(verts)):
                    ms6.apply_filter('colorize_by_geodesic_distance_from_a_given_point', startpoint=verts[ind], maxdistance=100)
                    dist_matrix.append(np.max(ms6.mesh(0).vertex_quality_array()))
                    dist_matrix_ind.append(np.argmax(ms6.mesh(0).vertex_quality_array()))
                    start_ind.append(ind)
                # find which point has largest distance
                max1 = np.argmax(dist_matrix)
                end_point = verts[dist_matrix_ind[max1]]
                start_point = verts[start_ind[max1]]
                # create cylinder between these points
                r = 0.5
                path_cylinder = path + '\Segmentation_' + segment + '_wires' + str(i) + 'cylinder.stl'
                cylinder_between(start_point, end_point, r, path_cylinder)
                # combine wire and cylinder
                ms6.load_new_mesh(path_cylinder)
                ms6.apply_filter('mesh_boolean_union', first_mesh=0, second_mesh=1)
                ms6.save_current_mesh(path + '\Segmentation_' + segment + '_wires' + str(i) + '.stl', binary=False)
                # split mesh again with closed wire
                ms4 = cut_mesh(out2, path, i)
                # remove meshes with few vertices
                no_meshes = ms4.number_meshes()
                for ind in range(0,no_meshes):
                    no_vertices = ms4.mesh(ind).vertex_matrix().shape[0]
                    if no_vertices < 50:
                        ms4.set_current_mesh(ind)
                        ms4.delete_current_mesh()
                    else:
                        last_mesh = ind
            # select last mesh to save
            no_meshes = ms4.number_meshes()
            # save only mesh part inside wire
            # ms4.set_current_mesh(new_curr_id=last_mesh)
            to_del = [0,1,2]
            for removes in range(len(to_del)):
                ms4.set_current_mesh(new_curr_id=to_del[removes])
                ms4.delete_current_mesh()
            print(ms4.number_meshes())
            ms4.apply_filter('flatten_visible_layers')
            try:
                ms4.save_current_mesh(path + '\Segmentation_' + segment + '_area' + str(i) + '.stl', binary=False)
                ms4.load_new_mesh(path + '\Segmentation_' + segment + '_area' + str(i) + '.stl')
                geometric_measures = ms4.apply_filter('compute_geometric_measures')
                surface = geometric_measures['surface_area']
                print('Surface area ' + segment + ' ligament' + str(i) + ': ' + str(surface) + ' mm2')
                ms4.save_project(path + '\Segmentation_' + segment + '_area' + str(i) + '.mlp')
            except:
                error.append(i)




# ms.select_all()
# ms.fit_a_plane_to_selection()
# plane_normal = ms.mesh(1).face_normal_matrix()

# vert_matrix_connect = ms.mesh(2).vertex_matrix()
# matrix = ms.mesh(1).vertex_matrix()
# points = []
# val = []
# for i in range(0,len(vert_matrix_connect)):
# 	points.append(np.argmin(np.abs(np.sum(matrix-vert_matrix_connect[i,:],axis=1))))
# 	val.append(np.amin(np.abs(np.sum(matrix-vert_matrix_connect[i,:],axis=1))))

# ms.conditional_vertex_selection(condselect="vi=="+points[0])
# ms.delete_selected_vertices()

# out4 = ms2.apply_filter('mesh_boolean_intersection', first_mesh=1, second_mesh=0)


# no_verts = ms2.mesh(1).selected_vertex_number()
# no_verts_new = ms2.mesh(1).selected_vertex_number()
# while no_verts/2 < no_verts_new:
# 	ms2.select_border()
# 	ms2.delete_selected_vertices()
# 	ms2.conditional_vertex_selection(condselect="q<0")
# 	no_verts_new = ms2.mesh(1).selected_vertex_number()

# matrix = ms2.mesh(1).face_matrix()
# unique, counts = np.unique(matrix, return_counts=True)
# # dict(zip(unique, counts))
# bla2 = np.where(counts < 5)
#
# ms2.conditional_vertex_selection(condselect="q<0")
# no_verts_new = ms2.mesh(1).selected_vertex_number()

# while no_verts_new>0:
# 	for i in range(0,len(bla2[0])):
# 		ms2.conditional_vertex_selection(condselect=("vi=="+str(bla2[0][i])))
# 		ms2.delete_selected_faces_and_vertices()
#
# 	ms2.conditional_vertex_selection(condselect="q<0")
# 	no_verts_new = ms2.mesh(1).selected_vertex_number()
# 	matrix = ms2.mesh(1).face_matrix()
# 	unique, counts = np.unique(matrix, return_counts=True)
# 	# dict(zip(unique, counts))
# 	bla2 = np.where(counts < 5)

#
# ms2.apply_filter('hausdorff_distance', targetmesh=0, sampledmesh=1, savesample=True, maxdist=5)

# ms2.conditional_vertex_selection(condselect="q>2")
# ms2.delete_selected_vertices()



#

# ms.apply_filter('select_faces_from_vertices', points, inclusive=True)
# ms.apply_filter('delete_selected_faces_and_vertices')

# vert_matrix_connect.apply_filter('surface_reconstruction_ball_pivoting')

# file = r'C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData\26\femur_wires.xyz'
# np.savetxt(file, vert_matrix_connect)


# ms2.save_project(r'C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData\26\Segmentation.mlp')