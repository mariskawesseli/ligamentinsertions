# Find most anterior  edge of the femoral notch roof - representation Blumensaat line for 3D shapes
# https://journals.lww.com/jbjsjournal/Fulltext/2010/06000/The_Location_of_Femoral_and_Tibial_Tunnels_in.10.aspx?__hstc=215929672.82af9c9a98fa600b1bb630f9cde2cb5f.1528502400314.1528502400315.1528502400316.1&__hssc=215929672.1.1528502400317&__hsfp=1773666937&casa_token=BT765BcrC3sAAAAA:Vu9rn-q5ng4c8339KQuq2mGZDgrAgBStwvn4lvYEbvCgvKQZkbJL24hWbKFdnHTc8VBmAIXA3HVvuWg22-9Mvwv1sw
# https://www.dropbox.com/sh/l7pd43t7c4hrjdl/AABkncBbleifnpLDKSDDc0dCa/D3%20-%20Dimitriou%202020%20-%20Anterior%20cruciate%20ligament%20bundle%20insertions%20vary.pdf?dl=0

import trimesh
import numpy as np
import os
import math
import pandas as pd
# import pymeshlab
import seaborn as sns


def findIntersection(x1, y1, x2, y2, x3, y3, x4, y4):
    px = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / (
            (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4))
    py = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / (
            (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4))

    ang = math.atan2(py - y3, px - x3) - math.atan2(y1 - y3, x1 - x3)

    l = math.cos(ang)*np.linalg.norm(np.asarray((x3,y3))-np.asarray((x4,y4)))

    return l, px, py

def split(start, end, segments):
    x_delta = (end[0] - start[0]) / float(segments)
    y_delta = (end[1] - start[1]) / float(segments)
    z_delta = (end[2] - start[2]) / float(segments)
    points = []
    for i in range(1, segments):
        points.append([start[0] + i * x_delta, start[1] + i * y_delta, start[2] + i * z_delta])
    return [start] + points + [end]


ligaments_fem = [[1,1,1,1,1,1,1,1,1,1],  # PCL
                [6,5,6,6,6,6,4,4,5,5],  # MCLp
                [3,2,5,3,3,2,2,0,3,3],  # MCLd
                [0,8,0,0,0,0,0,0,0,0],  # MCLd2
                [7,3,7,7,7,5,7,6,7,0],  # POL
                [0,0,8,0,0,0,0,0,0,0],  # POL2
                [0,0,0,0,0,0,0,0,0,0],  # POL3
                [0,0,0,0,0,0,0,0,0,0],  # POL4
                [4,6,3,5,4,0,0,3,4,4],  # ACL
                [5,7,4,4,5,7,6,5,6,6],  # LCL
                [2,4,2,2,2,3,3,2,2,2]]  # POP

ligaments = ligaments_fem

# find most ant point in yz plane
subjects = [100]  # [9,13,19,23,26,29,32,35,37,41]  #
lig = 'ACL'
segment = 'femur'

d = []
h = []
h_centriods = []
d_centriods = []
for ind, subject in enumerate(subjects):
    if subject in [9, 13, 26, 29, 32]:
        side = 'R'
        reflect = ''
    else:
        side = 'L'
        reflect = '.reflect'

    if subject == 100:
        path = os.path.join(
            r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone\new_bone\shape_models\mean_shape_rot.stl')
        path_col = r'C:\\Users\\mariskawesseli\\Documents\\GitLab\\knee_ssm\\OAI\\Output/tibia_bone\\new_bone\\shape_models'
        side = 'R'
    else:
        path = os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData", str(subject), 'Segmentation_femur_transform.STL')

    mesh = trimesh.load_mesh(path)
    verts = mesh.vertices
    AP = mesh.bounding_box.bounds[1, 1] - mesh.bounding_box.bounds[0, 1]
    ML = mesh.bounding_box.bounds[1, 0] - mesh.bounding_box.bounds[0, 0]
    bbox = mesh.bounding_box.bounds

    # posterior_mesh = trimesh.intersections.slice_mesh_plane(mesh, (0,-1,0), (0,20,0), cached_dots=None, return_both=False)
    # posterior_mesh.show()

    # find blumensaat line
    lines, to_3D, face_index = trimesh.intersections.mesh_multiplane(mesh, (0,0,0), (1,0,0), heights=np.linspace(-10, 10, 21))
    dist_point = []
    prox_point = []
    for i in range(0,len(face_index)):
        plane_verts = np.unique(mesh.faces[face_index[i]])
        plane_points = mesh.vertices[plane_verts]

        goon = 1
        tel = 2
        while goon == 1:
            min_z = np.where(plane_points[:,2] == np.partition(plane_points[:,2], tel)[tel])
            y_min = plane_points[min_z][0][1]
            min_z2 = np.where(plane_points[:,2] == np.partition(plane_points[:,2], tel+1)[tel+1])
            y_min2 = plane_points[min_z2][0][1]
            if y_min-y_min2 > -15:
                goon = 1
                tel += 1
            else:
                goon = 0
        dist_point.append(plane_points[min_z][0])
        min_y = np.where(plane_points[:,1] == plane_points[:,1].min())
        prox_point.append(plane_points[min_y][0])

    most_ant_ind1 = np.asarray(dist_point)[:, 1].argmax()
    most_ant_ind2 = np.asarray(prox_point)[:, 1].argmax()

    p1 = []
    p2 = []
    if most_ant_ind1 == most_ant_ind2:
        p1.append(dist_point[most_ant_ind1])
        p2.append(prox_point[most_ant_ind1])
        print('equal')
    else:
        p1.append(dist_point[most_ant_ind2])
        p2.append(prox_point[most_ant_ind2])
        print('not equal')

    if side == 'R':
        if lig == 'ACL':
            lateral_mesh = trimesh.intersections.slice_mesh_plane(mesh, (1,0,0), (0, 0, 0), cached_dots=None, return_both=False)
        else:
            lateral_mesh = trimesh.intersections.slice_mesh_plane(mesh, (-1, 0, 0), (0, 0, 0), cached_dots=None,
                                                                  return_both=False)
    else:
        if lig == 'ACL':
            lateral_mesh = trimesh.intersections.slice_mesh_plane(mesh, (-1, 0, 0), (0, 0, 0), cached_dots=None,
                                                              return_both=False)
        else:
            lateral_mesh = trimesh.intersections.slice_mesh_plane(mesh, (1, 0, 0), (0, 0, 0), cached_dots=None,
                                                                  return_both=False)

    # find height
    vec1 = (p1[0][0] - p2[0][0], p1[0][1] - p2[0][1], p1[0][2] - p2[0][2])
    norm = np.sqrt(vec1[0] ** 2 + vec1[1] ** 2 + vec1[2] ** 2)
    direction = [vec1[0] / norm, vec1[1] / norm, vec1[2] / norm]


    # segments = np.asarray([p1[-1], p2[-1]])
    # p = trimesh.load_path(segments)

    # trimesh.path.segments.parameters_to_segments(p1[-1], -1*direction, ((0,0,0),(0,1,0)))
    # trimesh.path.segments.segments_to_parameters(np.asarray(segments))

    # posterior_mesh = trimesh.intersections.slice_mesh_plane(mesh, direction, (0,0,10), cached_dots=None, return_both=False)

    lines, to_3D, face_index = trimesh.intersections.mesh_multiplane(lateral_mesh, (0,0,0), direction, heights=np.linspace(-10, 10, 21))

    dist = []
    p3 = []
    p4 = []
    p1_2d = p1[-1][1:3]
    p2_2d = p2[-1][1:3]
    for i in range(0,len(face_index)):
        plane_verts = np.unique(lateral_mesh.faces[face_index[i]])
        plane_points = lateral_mesh.vertices[plane_verts]

        min_y = np.where(plane_points[:,1] == np.partition(plane_points[:,1], 0)[0])
        max_y = np.where(plane_points[:,1] == np.partition(plane_points[:,1], -1)[-1])

        p3.append(plane_points[min_y][0])
        p4.append(plane_points[max_y][0])
        dist.append(np.linalg.norm(np.cross(p2_2d-p1_2d, p1_2d-p3[i][1:3]))/np.linalg.norm(p2_2d-p1_2d))
        # dist.append(np.linalg.norm(plane_points[min_y][0]-plane_points[max_y][0]))

    # segments = np.asarray([p3[np.asarray(dist).argmax()], p4[np.asarray(dist).argmax()]])
    # p_dist = trimesh.load_path(segments)
    dist1 = dist
    p3_2d = p3[np.asarray(dist1).argmax()][1:3]
    h.append(np.linalg.norm(np.cross(p2_2d-p1_2d, p1_2d-p3_2d))/np.linalg.norm(p2_2d-p1_2d))

    # find depth
    # lateral_mesh.show()
    lines, to_3D, face_index = trimesh.intersections.mesh_multiplane(lateral_mesh, (0, 0, 0), direction,
                                                                     heights=np.linspace(-30, -5, 41))

    dist = []
    p6 = []
    p4 = []
    p1_2d = p1[-1][1:3]
    p2_2d = p2[-1][1:3]
    for i in range(0, len(face_index)):
        plane_verts = np.unique(lateral_mesh.faces[face_index[i]])
        plane_points = lateral_mesh.vertices[plane_verts]

        min_y = np.where(plane_points[:, 1] == np.partition(plane_points[:, 1], 0)[0])
        max_y = np.where(plane_points[:, 1] == np.partition(plane_points[:, 1], -1)[-1])

        p6.append(plane_points[min_y][0])
        p4.append(plane_points[max_y][0])
        dist.append(np.linalg.norm(np.cross(p2_2d - p1_2d, p1_2d - p6[i][1:3])) / np.linalg.norm(p2_2d - p1_2d))
    # dist.append(np.linalg.norm(plane_points[min_y][0]-plane_points[max_y][0]))

    jump_ind = np.where(np.diff(np.asarray(p6), axis=0)[:,1] == np.min(np.diff(np.asarray(p6), axis=0)[:,1]))[0][0]

    # segments = np.asarray([p6[jump_ind+1], p4[jump_ind+1]])
    # p_dist = trimesh.load_path(segments)

    p6_2d = p6[jump_ind+1][1:3]
    # min_z = lateral_mesh.vertices[np.argmin(lateral_mesh.vertices[:,2])]
    # p5 = np.asarray(min_z)
    # p5_2d = p5[1:3]

    direction = np.asarray(direction) * -1
    direction_perp = np.array((direction[0], -direction[2], direction[1]))

    lines, to_3D, face_index = trimesh.intersections.mesh_multiplane(lateral_mesh, p1[0], direction_perp,
                                                                     heights=np.linspace(0, 1, 1))
    plane_verts = np.unique(lateral_mesh.faces[face_index[0]])
    plane_points = lateral_mesh.vertices[plane_verts]
    min_z = np.where(plane_points[:, 2] == np.partition(plane_points[:, 2], 0)[0])
    p5 = plane_points[min_z][0]
    p5_2d = p5[1:3]

    l, px, py = findIntersection(p1_2d[0], p1_2d[1], p2_2d[0], p2_2d[1], p6_2d[0], p6_2d[1], p5_2d[0], p5_2d[1])
    d.append(l)

    # visualization
    # p1[0][0] = 0
    # p2[0][0] = 0
    # p3[np.asarray(dist1).argmax()][0] = 0
    # p4[jump_ind + 1][0] = 0
    # p5[0] = 0
    # p6[jump_ind + 1][0] = 0

    points = trimesh.points.PointCloud(np.asarray((p1[0],p2[0],p6[jump_ind+1],p5, p3[np.asarray(dist1).argmax()])), colors=None, metadata=None)
    segments = np.asarray([p1[-1], p2[-1]])
    p = trimesh.load_path(segments)
    segments = np.asarray([p6[jump_ind+1], p5])
    p_dist = trimesh.load_path(segments)

    mesh.visual.face_colors[:] = np.array([227, 218, 201, 150])
    mesh.visual.vertex_colors[:] = np.array([227, 218, 201, 150])
    if lig == 'ACL':
        line = trimesh.path.segments.parameters_to_segments([p1[-1],p6[jump_ind+1],p3[np.asarray(dist1).argmax()],p5], [direction,direction_perp,direction,direction_perp],
                                                            np.array(((d[-1]-5,-14),(-12,h[-1]-10),(d[-1]-25.5,-23.5),(-1.5,h[-1]+1))).astype(float)) #ACL
        box_points = trimesh.load_path(np.squeeze(line)).vertices
        grid_points1 = split(box_points[0], box_points[4], 4)
        grid_points2 = split(box_points[0], box_points[3], 4)
        grid_line = trimesh.path.segments.parameters_to_segments([grid_points1[1], grid_points1[2], grid_points1[3]],
                                                                 [direction_perp], np.array(
                ((h[-1] + 2.5, -0), (h[-1] + 2.5, 0), (h[-1] + 2, -0))).astype(float))
        grid_line2 = trimesh.path.segments.parameters_to_segments([grid_points2[1], grid_points2[2], grid_points2[3]],
                                                                  [direction],
                                                                  np.array(((d[-1] - 1.5, 0), (d[-1] - 1.5, 0),
                                                                            (d[-1] - 2, 0))).astype(
                                                                      float))
    else:
        line = trimesh.path.segments.parameters_to_segments([p1[-1], p6[jump_ind + 1], p3[np.asarray(dist1).argmax()], p5],
                                                            [direction, direction_perp, direction, direction_perp],
                                                            np.array(((d[-1] -8, -16), (h[-1] - 11, -13.5),
                                                                      (d[-1] - 27, -25.5), (h[-1],-3))).astype(float)) #PCL
        box_points = trimesh.load_path(np.squeeze(line)).vertices
        grid_points1 = split(box_points[0], box_points[7], 4)
        grid_points2 = split(box_points[0], box_points[5], 4)

        grid_line = trimesh.path.segments.parameters_to_segments([grid_points1[1],grid_points1[2],grid_points1[3]],[direction_perp],np.array(((h[-1]+3,-0),(h[-1]+3,0),(h[-1]+2.5,-0))).astype(float))
        grid_line2 = trimesh.path.segments.parameters_to_segments([grid_points2[1], grid_points2[2], grid_points2[3]],
                                                                 [direction],
                                                                 np.array(((d[-1] -1,0), (d[-1]-1,0), (d[-1]-1.5,0))).astype(
                                                                     float))
    grid_line_path = trimesh.load_path(np.squeeze(grid_line), colors=((0.5,0.5,0.5,),(0.5,0.5,0.5),(0.5,0.5,0.5)))
    grid_line2_path = trimesh.load_path(np.squeeze(grid_line2),
                                       colors=((0.5, 0.5, 0.5,), (0.5, 0.5, 0.5), (0.5, 0.5, 0.5)))
    scene = trimesh.Scene([mesh, trimesh.load_path(np.squeeze(line)),grid_line_path,grid_line2_path])  #, points
    origin, xaxis, yaxis, zaxis = scene.camera_transform[0:3,3], [1, 0, 0], [0, 1, 0], [0, 0, 1]
    if lig == 'ACL':
        Rx = trimesh.transformations.rotation_matrix(np.radians(-90), xaxis)
        Ry = trimesh.transformations.rotation_matrix(np.radians(-90), yaxis)
    else:
        Rx = trimesh.transformations.rotation_matrix(np.radians(-90), xaxis)
        Ry = trimesh.transformations.rotation_matrix(np.radians(90), yaxis)
    R = trimesh.transformations.concatenate_matrices(Ry,Rx)
    scene.apply_transform(R)
    # scene.camera_transform = camera_trans
    scene.show()
    # mesh.vertices[:, 0] = 0
    # trimesh.Scene([mesh, points, trimesh.load_path(np.squeeze(line))]).show()

# posterior_mesh = trimesh.intersections.slice_mesh_plane(mesh, direction, (0,-30,0), cached_dots=None, return_both=False)
# posterior_mesh.show()
    if subject == 100:
        points_lig = trimesh.load_mesh(r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone\new_bone\shape_models\meanshape_ligs_rot.xyz')
        if lig == 'ACL':
            center = np.arange(341 - 263) + 263  # ACL
            mean = np.array((61.2, -78.9, 39.3)) / 100 * np.array((ML, AP, AP)) + np.array( #
                (bbox[0, 0], bbox[1, 1], bbox[0, 2]))
        else:
            center = np.arange(112)  # PCL
            mean = np.array((39.5, -63.4, 23.8)) / 100 * np.array((ML, AP, AP)) + np.array(
                (bbox[0, 2], bbox[1, 1], bbox[0, 2]))

        points_lig = points_lig[center]
        # origin, xaxis, yaxis, zaxis = [0, 0, 0], [1, 0, 0], [0, 1, 0], [0, 0, 1]
        # Rz = trimesh.transformations.rotation_matrix(180/np.pi, zaxis)
        # points_lig.apply_transform(Rz)
        color_file = np.loadtxt(path_col + '\meanshape_ligs_color.xyz')[:, 3]
        color_file = color_file[center]
        c = sns.color_palette("viridis_r", n_colors=10, as_cmap=False)

        color = []
        for ind_col, point in enumerate(points_lig):
            center_2d = point[1:3]
            h_centriods.append(np.linalg.norm(np.cross(p2_2d - p1_2d, p1_2d - center_2d)) / np.linalg.norm(p2_2d - p1_2d))
            l, px, py = findIntersection(p1_2d[0], p1_2d[1], p2_2d[0], p2_2d[1], center_2d[0], center_2d[1], p5_2d[0],
                                         p5_2d[1])
            d_centriods.append(l)
            vcolors = [c[int(color_file[ind_col] - 1)][0] * 255, c[int(color_file[ind_col] - 1)][1] * 255,
                       c[int(color_file[ind_col] - 1)][2] * 255]
            color.append(vcolors)
        p_lig = trimesh.points.PointCloud(points_lig, colors=color)
        p_mean = trimesh.primitives.Sphere(radius=1, center=mean, subdivisions=3, color=[255, 0, 0]) # trimesh.points.PointCloud([mean, mean], colors=[[255, 0, 0], [255, 0, 0]])
        p_mean.visual.face_colors = np.array([255, 0, 0, 255])
        # scene2 = trimesh.Scene([mesh, points, p_lig, trimesh.load_path(np.squeeze(line))])
        # scene2.apply_transform(R)
        # scene2.camera_transform = camera_trans
        # scene2.show()
        scene.add_geometry([p_lig, p_mean],transform=R)
        scene.show()
    else:
        if lig == 'ACL':
            lig_no = ligaments[8][ind]
        elif lig == 'PCL':
            lig_no = ligaments[0][ind]
        if not lig_no == 0:
            segment = 'femur'
            path = os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData", str(subject))

            rot_mat = np.linalg.inv(np.loadtxt(path + '\Segmentation_' + segment + '_resample._ACS.txt'))
            ms4 = pymeshlab.MeshSet()
            ms4.load_new_mesh(path + '\Segmentation_' + segment + '_area' + str(lig_no) + '.stl')

            ms4.apply_filter('flatten_visible_layers', deletelayer=True)
            ms4.apply_filter('matrix_set_copy_transformation', transformmatrix=rot_mat)
            geometric_measures = ms4.apply_filter('compute_geometric_measures')

            # print('Surface area femur ligament' + str(lig_no) + ': ' + str(surface) + ' mm2')
            center = geometric_measures['shell_barycenter']
            center_2d = center[1:3]
            h_centriods.append(np.linalg.norm(np.cross(p2_2d-p1_2d, p1_2d-center_2d))/np.linalg.norm(p2_2d-p1_2d))
            l, px, py = findIntersection(p1_2d[0], p1_2d[1], p2_2d[0], p2_2d[1], center_2d[0], center_2d[1], p5_2d[0], p5_2d[1])
            d_centriods.append(l)
        else:
            h_centriods.append(0)
            d_centriods.append(0)

[1-abs(i / j) for i, j in zip(d_centriods, d)]
[i / j for i, j in zip(h_centriods, h)]

d_centriods/np.asarray(d)
h_centriods/np.asarray(h)

np.mean(abs(np.asarray(d_centriods))/np.asarray(d))
np.mean(h_centriods/np.asarray(h))



