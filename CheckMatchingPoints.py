import trimesh
import os
import numpy as np

segments = ['femur']
subjects = ['9','13','19','23','26','29','32','35','37','41'] #, S0 [100]
lig = 'pop'
center_only = 1

if lig == 'PCL':
    center_tibia = np.arange(131)  # np.arange(470-341)+341 #np.concatenate((np.arange(131),np.arange(470-341)+341))  # PCL + ACL
    center_femur = np.arange(112)  # np.arange(341-263)+263 #np.concatenate((np.arange(112),np.arange(341-263)+263))  # PCL + ACL

if lig == 'LCL':
    center_femur = np.arange(706-641)+641  # np.arange(415-379)+379  # np.arange(370-341)+341 = 4096
    center_tibia = np.arange(242)
if lig == 'pop':
    center_femur = np.arange(776-706)+706  #np.arange(454-415)+415  # np.arange(401-370)+370 = 4096
    center_tibia = 0

no_points=[]
points_in_attachment = []
perc_points=[]
total_points=[]
no_all_points=[]
perc_points_in_area=[]

for segment in segments:
    if segment == 'tibia' or segment == 'fibula':
        center = center_tibia
    elif segment == 'femur':
        center = center_femur
    if segment == 'fibula':
        short = '_short'
    else:
        short = ''

    for ind, subject in enumerate(subjects):

        path = os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData", str(subject))
        points_lig = trimesh.load_mesh(path + '\SSM_' + segment + '_pred_points_color_8192.xyz')
        points_area = trimesh.load_mesh(path + '\8192\SSM_' + segment + short + '_areas_test.xyz')
        color = np.loadtxt(path + '\SSM_' + segment + '_pred_points_color_8192.xyz')[:, 3]
        if center_only == 1:
            points_lig = points_lig[center]
            color = color[center]
        print(color)
        corresponding_points = np.where(color>=7)
        all_points = np.where(color >= 0)
        all_points_lig = points_lig[all_points]
        points_lig = points_lig[corresponding_points]
        result = []
        result2 = []
        for i in range(0,len(points_area.vertices)):
            rows = np.where(points_lig[:, 0] == points_area.vertices[i,0])
            if len(rows[0])>=1:
                # print(points_lig[rows])
                # print(points_area.vertices[i,:])
                result.append(points_lig[rows])

            rows = np.where(all_points_lig[:, 0] == points_area.vertices[i, 0])
            if len(rows[0]) >= 1:
                # print(all_points_lig[rows])
                # print(points_area.vertices[i, :])
                result2.append(all_points_lig[rows])

        no_points.append(len(result))  #SSM_points_predicted
        no_all_points.append(len(result2))  #all points in area
        perc_points.append(len(result)/len(result2))  #perc points wrt all points

        total_points.append(len(points_area.vertices))
        points_in_attachment.append(result)

perc_points_in_area.append(np.asarray(no_points)/len(corresponding_points[0]))  # perc points inside area

print(str(np.average(no_all_points)) + ' (' + str(np.min(no_all_points)) + '-' + str(np.max(no_all_points)) + ')')
print(len(corresponding_points[0]))
print(str(np.average(no_points)) + ' (' + str(np.min(no_points)) + '-' + str(np.max(no_points)) + ')')
print(str(round(np.average(perc_points_in_area)*100,1)) + ' (' + str(round(np.min(perc_points_in_area)*100,1)) + '-' + str(round(np.max(perc_points_in_area)*100,1)) + ')')
print(str(round(np.average(perc_points)*100,1)) + ' (' + str(round(np.min(perc_points)*100,1)) + '-' + str(round(np.max(perc_points)*100,1)) + ')')

