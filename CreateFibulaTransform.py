import os
import trimesh
import numpy as np
import pymeshlab

subjects = [9,13,19,23,26,29,32,35,37,41]
ligaments_fib = [[2,2,2,2,2,2,2,3,2,2]]  # LCL

for ind, subject in enumerate(subjects):
    # path = os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData", str(subject))
    #
    # rot_mat = np.linalg.inv(np.loadtxt(path + '\Segmentation_tibia_resample._ACS.txt'))
    # mesh2 = path + '\Segmentation_tibia_fib.stl'
    # ms5 = pymeshlab.MeshSet()
    # ms5.load_new_mesh(mesh2)
    # ms5.apply_filter('matrix_set_copy_transformation', transformmatrix=rot_mat)
    # ms5.save_current_mesh(path + '\Segmentation_fibula_tib_frame.stl', binary=False)

    path = os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData", str(subject))

    fibula = trimesh.load_mesh(path + '\Segmentation_fibula_tib_frame.stl')
    most_prox_point = fibula.vertices[np.argmax(fibula.vertices[:,2]),:]
    T = trimesh.transformations.translation_matrix(-most_prox_point)
    fibula_area = trimesh.load_mesh(path + '\Segmentation_tibia_area' + str(ligaments_fib[0][ind]) + '_transform.stl')
    # fibula_wire = trimesh.load_mesh(path + '\Segmentation_fibula_wires_transform_lateral.stl')
    # center = np.array([-48.399971,-14.163541,-15.73211])

    fibula.apply_transform(T)
    # fibula.export(path + '\Segmentation_fibula_transform.stl')
    fibula_area.apply_transform(T)
    fibula_area.export(path + '\Segmentation_fibula_area' + str(ligaments_fib[0][ind]) + '_transform.stl')
    # fibula_wire.apply_transform(T)
    # fibula_wire.export(path + '\Segmentation_fibula_wire_transform_lateral.stl')
    # points = center-most_prox_point
    # print(points)
