import numpy as np
import pyvista as pv
import json
import os

subjects = [9,13,19,23,26,29,32,35,37,41]
segment = 'femur'

for ind, subject in enumerate(subjects):
    if subject in [9, 13, 26, 29, 32]:
        side = 'R'
        reflect = ''
    else:
        side = 'L'
        reflect = '.reflect'

    path = os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData", str(subject))
    file_resample = os.path.join(path, 'Segmentation_femur_transform.stl')
    file_wires = os.path.join(path, 'Segmentation_femur_wires_transform.stl')
    epicondyle_mw = os.path.join(path, 'Femur', 'FemurLateralCondyle', 'MarkupsFiducial.mrk.json')
    epicondyle_tv = os.path.join(path, 'Femur', 'FemurLateralCondyle', 'MarkupsFiducial' + str(subject) + 'TV.mrk.json')
    rot_mat = np.linalg.inv(np.loadtxt(path + '\Segmentation_' + segment + '_resample._ACS.txt'))

    mesh = pv.read(file_resample)
    if side == 'R':
        clipped = mesh.clip(normal=[-1, 0, 0], origin=[30, 0, 0])
    else:
        clipped = mesh.clip(normal=[1, 0, 0], origin=[-30, 0, 0])
    clipped = clipped.clip(normal=[0, 0, 1], origin=[0, 0, 40])
    z = clipped.points[:,0]
    mi, ma = round(min(z)), round(max(z))
    step = 1
    cntrs = np.arange(mi, ma + step, step)
    contours = clipped.contour(cntrs, scalars=clipped.points[:,0])

    wires = pv.read(file_wires)

    f = open(epicondyle_mw, "r")
    data = json.loads(f.read())
    position_mw = np.asarray(data['markups'][0]['controlPoints'][0]['position'])
    f.close()
    pos_mw = pv.wrap(position_mw).transform(rot_mat)
    pos_mw = pos_mw.glyph(scale=1000, geom=pv.Sphere())

    f = open(epicondyle_tv, "r")
    data = json.loads(f.read())
    position_tv = np.asarray(data['markups'][0]['controlPoints'][0]['position'])
    f.close()
    pos_tv = pv.wrap(position_tv).transform(rot_mat)
    pos_tv = pos_tv.glyph(scale=1000, geom=pv.Sphere())

    pv.set_plot_theme("document")
    pv.global_theme.auto_close = True
    p = pv.Plotter()
    p.add_mesh(contours, line_width=5, color="black")
    if side == 'R':
        p.add_mesh(clipped, colormap='terrain_r')
        p.camera_position = 'yz'
        p.camera.roll += 0
    else:
        p.add_mesh(clipped, colormap='terrain')
        p.camera_position = 'zy'
        p.camera.roll += 90
    p.show(screenshot=path+r'\Femur\elevation_map.png')

    p2 = pv.Plotter()
    p2.add_mesh(contours, line_width=5, color="black")
    if side == 'R':
        p2.add_mesh(clipped, colormap='terrain_r')
        p2.camera_position = 'yz'
        p2.camera.roll += 0
    else:
        p2.add_mesh(clipped, colormap='terrain')
        p2.camera_position = 'zy'
        p2.camera.roll += 90
    p2.add_mesh(pos_mw,color='tomato')
    p2.add_mesh(pos_tv,color='springgreen')
    p2.add_mesh(wires, opacity=0.50,color='cyan')
    p2.show(screenshot=path+r'\Femur\elevation_map_all.png', auto_close=True)