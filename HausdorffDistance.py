import pymeshlab
# from plyfile import PlyData, PlyElement
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
# from vtk import *
import nrrd
import re
import os
import pandas as pd
from tabulate import tabulate
from shutil import copyfile
import glob
import trimesh

def writeply(surface,filename):
    """Write mesh as ply file."""
    writer = vtkPLYWriter()
    writer.SetInputData(surface)
    writer.SetFileTypeToASCII()
    writer.SetFileName(filename)
    writer.Write()

def readVTK(file):
    reader = vtkDataSetReader()
    reader.SetFileName(file)
    reader.ReadAllVectorsOn()
    reader.ReadAllScalarsOn()
    reader.Update()

    data = reader.GetOutput()
    return data

subjects = [9,13,19,23,26,29,32,35,37,41] #
segments = ['femur']  #'femur','tibia'  ','tibia','fibula'

for segment in segments:
    RMS = []
    for ind, subject in enumerate(subjects):
        path = os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData", str(subject))
        if subject in [9, 13, 26, 29, 32]:
            side = 'R'
            reflect = ''
        else:
            side = 'L'
            reflect = '.reflect'
        if segment == 'fibula':
            remesh = '_remesh'
        else:
            remesh = ''

        # xyz_file = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone\new_bone\shape_models\Segmentation_' + segment + '_' + side + '_short_' + str(
        # 	subject) + reflect + '.isores.pad.com.center.aligned.clipped.cropped.tpSmoothDT_local.xyz'
        # points1 = trimesh.load_mesh(xyz_file)
        # # mesh = trimesh.load_mesh(path_bones + '\Segmentation_' + segment + '_' + side + '_short_' + str(subject) + '.STL')
        # points2 = trimesh.load_mesh(r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\femur_bone\new_bone\reconstructed\shape9.xyz')
        # kwargs = {"scale": True}
        # icp = trimesh.registration.icp(points2.vertices, points1.vertices, initial=np.identity(4), threshold=1e-5, max_iterations=20,**kwargs)
        # points2.apply_transform(icp[0])
        # # np.savetxt(r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\femur_bone\new_bone\reconstructed\9_reconstruct_transform_icp_test.xyz', points2.vertices, delimiter=" ")

        # files from SSM workflow shapeworks
        file_com = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone\new_bone\groomed\com_aligned\Segmentation_' + segment + '_' + side + '_short_' + str(
            subject) + remesh + reflect + '.isores.pad.com.txt'
        file_align = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone\new_bone\groomed\aligned\Segmentation_' + segment + '_' + side + '_short_' + str(
            subject) + remesh + reflect + '.isores.pad.com.center.aligned.txt'
        pad_file = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone\new_bone\groomed\padded\segementations\Segmentation_' + segment + '_' + side + '_short_' + str(
            subject) + remesh + reflect + '.isores.pad.nrrd'
        com_file = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone\new_bone\groomed\com_aligned\Segmentation_' + segment + '_' + side + '_short_' + str(
            subject) + remesh + reflect + '.isores.pad.com.nrrd'
        # particle_file = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone\new_bone\shape_models\4096\Segmentation_' + segment + '_' + side + '_short_' + str(
        #     subject) + remesh + reflect + '.isores.pad.com.center.aligned.clipped.cropped.tpSmoothDT_local.particles'
        reconstructed_file = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone\new_bone\reconstructed\mesh' + str(subject) + 'dt.stl'
        # align_file = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\femur_bone\new_bone\groomed\aligned\Segmentation_femur_' + side + '_short_' + str(subject) + reflect + '.isores.pad.com.center.aligned.nrrd'
        path_bones = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone\new_bone\input'
        if segment == 'fibula':
            org_mesh = path_bones + '/Segmentation_' + segment + '_' + side + '_short_' + str(subject) + '_remesh2.stl'
        else:
            org_mesh = path_bones + '/Segmentation_' + segment + '_' + side + '_short_' + str(subject) + '.stl'
        # get change in position from nrrd header files
        header = nrrd.read_header(pad_file)
        pad_position = header['space origin']
        header = nrrd.read_header(com_file)
        com_position = header['space origin']

        # get translation from align from rotation matrix
        rot_ssm = np.loadtxt(file_align)

        # translate reconstructed SSM instance to align with original mesh
        translate = pad_position - com_position + rot_ssm[3, :]
        mesh3 = reconstructed_file  # =local.particle file
        ms6 = pymeshlab.MeshSet()
        ms6.load_new_mesh(mesh3)

        max_val = 200
        iters = 1
        while translate[2] + max_val * iters < -max_val:
            ms6.apply_filter('transform_translate_center_set_origin', traslmethod=0, axisx=0, axisy=0, axisz=-max_val)
            iters = iters + 1
        # ms6.apply_filter('transform_translate_center_set_origin', traslmethod=0, axisx=0, axisy=0, axisz=-222)
        ms6.apply_filter('transform_translate_center_set_origin', traslmethod=0, axisx=0, axisy=0,
                         axisz=translate[2] + max_val * iters)
        ms6.apply_filter('invert_faces_orientation')
        ms6.apply_filter('simplification_quadric_edge_collapse_decimation', targetfacenum=7500)
        # ms6.save_current_mesh(path + '\8192\SSM_' + segment + '_reconstruct_transform.stl')

        ms6.apply_filter('transform_translate_center_set_origin', traslmethod=0, axisx=-15,
                         axisy=-90, axisz=-180)
        # ms6.apply_filter('transform_translate_center_set_origin', traslmethod=0, axisx=translate[0],
        #                  axisy=translate[1], axisz=-450)
        # ms6.apply_filter('transform_translate_center_set_origin', traslmethod=0, axisx=0, axisy=0, axisz=-450)
        # ms6.apply_filter('transform_translate_center_set_origin', traslmethod=0, axisx=0, axisy=0, axisz=-450)
        # ms6.apply_filter('transform_translate_center_set_origin', traslmethod=0, axisx=0, axisy=0,
        #                  axisz=translate[2] + 1350)
        ms6.save_current_mesh(path + '\8192\SSM_' + segment + '_reconstruct_transform.stl')

        # run ICP to get final position SSM point cloud on original mesh
        ms1 = pymeshlab.MeshSet()
        ms1.load_new_mesh(org_mesh)
        ms1.apply_filter('simplification_quadric_edge_collapse_decimation', targetfacenum=10000)
        ms1.save_current_mesh(path + '\8192\Segmentation_' + segment + '_resample.stl')

        mesh = trimesh.load_mesh(path + '\8192\Segmentation_' + segment + '_resample.stl')
        # mesh = trimesh.load_mesh(path_bones + '\Segmentation_' + segment + '_' + side + '_short_' + str(subject) + '.STL')
        points = trimesh.load_mesh(path + '\8192\SSM_' + segment + '_reconstruct_transform.stl')

        if reflect == '.reflect':
            M = trimesh.transformations.scale_and_translate((-1, 1, 1))
            points.apply_transform(M)
        kwargs = {"scale": False}
        icp = trimesh.registration.icp(points.vertices, mesh, initial=np.identity(4), threshold=1e-5, max_iterations=20,**kwargs)
        points.apply_transform(icp[0])
        icp = trimesh.registration.icp(points.vertices, mesh, initial=np.identity(4), threshold=1e-5, max_iterations=20, **kwargs)
        points.apply_transform(icp[0])
        points.export(path + '\8192\SSM_' + segment + '_reconstruct_transform_icp.stl')

        ms5 = pymeshlab.MeshSet()
        ms5.load_new_mesh(path + '\8192\SSM_' + segment + '_reconstruct_transform_icp.stl')
        ms5.load_new_mesh(org_mesh)
        out2 = ms5.apply_filter('hausdorff_distance', targetmesh=0, sampledmesh=1, savesample=True)
        out1 = ms5.apply_filter('hausdorff_distance', targetmesh=1, sampledmesh=0, savesample=True)

        RMS.append(max(out1['RMS'], out2['RMS']))

        print('max: ' + str(max(out1['max'], out2['max'])))
        print('min: ' + str(max(out1['min'], out2['min'])))
        print('mean: ' + str(max(out1['mean'], out2['mean'])))
        print('RMS: ' + str(max(out1['RMS'], out2['RMS'])))

        # dist_to_use = np.argmax([out1['max'], out2['max']])
        #
        # vq1 = ms5.mesh(2+dist_to_use*2).vertex_quality_array()
        #
        # samples = [sum(vq1 < 0.5), sum((vq1 > 0.5) & (vq1 < 1)), sum((vq1 > 1) & (vq1 < 1.5)),
        # 		   sum((vq1 > 1.5) & (vq1 < 2)), sum(vq1 > 2)]
        #
        # x = np.arange(5)  # the label locations
        # width = 0.35  # the width of the bars
        # fig, ax = plt.subplots()
        # rects1 = ax.bar(x, samples, width, label='femoral cartilage')

        ms5.save_current_mesh(path + r'/8192/Segmentation_' + segment + '_' + side + '_short_' + str(subject) + '_HD.ply', binary=False,
                             save_vertex_quality=True)
        np.save(path + r'/8192/' + segment + '_HD.np',[out1,out2])

    print('RMS ' + segment + str(np.average(RMS)))
