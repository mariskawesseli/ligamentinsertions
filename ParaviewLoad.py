import os
import glob

# subject = 9,13,19,23,26,29,32,35,37,41
subject = 41
segments = ['femur']  #['femur']  #
renderView1 = GetActiveViewOrCreate('RenderView')

for segment in segments:
    path = r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData/" + str(subject) + '/'
    Counter = len(glob.glob1(path, 'Segmentation_' + segment + '_area*.stl'))

    for count in range(1,Counter+1):

        segmentation_femur_area1stl = STLReader(registrationName='Segmentation_' + segment + '_area' + str(count) + '.stl', #_transform
                                                FileNames=[os.path.join(path,'Segmentation_' + segment + '_area' + str(count) + '.stl')]) #_transform
        # show data in view
        segmentation_femur_area1stlDisplay = Show(segmentation_femur_area1stl, renderView1, 'GeometryRepresentation')

        # trace defaults for the display properties.
        segmentation_femur_area1stlDisplay.Representation = 'Surface'
        segmentation_femur_area1stlDisplay.ColorArrayName = ['CELLS', 'STLSolidLabeling']
        segmentation_femur_area1stlDisplay.SelectTCoordArray = 'None'
        segmentation_femur_area1stlDisplay.SelectNormalArray = 'None'
        segmentation_femur_area1stlDisplay.SelectTangentArray = 'None'
        segmentation_femur_area1stlDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
        segmentation_femur_area1stlDisplay.SelectOrientationVectors = 'None'
        segmentation_femur_area1stlDisplay.ScaleFactor = 3.404553985595703
        segmentation_femur_area1stlDisplay.SelectScaleArray = 'None'
        segmentation_femur_area1stlDisplay.GlyphType = 'Arrow'
        segmentation_femur_area1stlDisplay.GlyphTableIndexArray = 'None'
        segmentation_femur_area1stlDisplay.GaussianRadius = 0.17022769927978515
        segmentation_femur_area1stlDisplay.SetScaleArray = [None, '']
        segmentation_femur_area1stlDisplay.ScaleTransferFunction = 'PiecewiseFunction'
        segmentation_femur_area1stlDisplay.OpacityArray = [None, '']
        segmentation_femur_area1stlDisplay.OpacityTransferFunction = 'PiecewiseFunction'
        segmentation_femur_area1stlDisplay.DataAxesGrid = 'GridAxesRepresentation'
        segmentation_femur_area1stlDisplay.PolarAxes = 'PolarAxesRepresentation'


    segmentation_femurstl = STLReader(registrationName='Segmentation_' + segment + '.stl', FileNames=[os.path.join(path,'Segmentation_' + segment + '.stl')]) #_transform
    segmentation_femur_wiresstl = STLReader(registrationName='Segmentation_' + segment + '_wires.stl', FileNames=[os.path.join(path,'Segmentation_' + segment + '_wires.stl')]) #_transform

    # show data in view
    segmentation_femurstlDisplay = Show(segmentation_femurstl, renderView1, 'GeometryRepresentation')

    # trace defaults for the display properties.
    segmentation_femurstlDisplay.Representation = 'Surface'
    segmentation_femurstlDisplay.ColorArrayName = [None, '']
    segmentation_femurstlDisplay.SelectTCoordArray = 'None'
    segmentation_femurstlDisplay.SelectNormalArray = 'None'
    segmentation_femurstlDisplay.SelectTangentArray = 'None'
    segmentation_femurstlDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
    segmentation_femurstlDisplay.SelectOrientationVectors = 'None'
    segmentation_femurstlDisplay.ScaleFactor = 10.438916015625
    segmentation_femurstlDisplay.SelectScaleArray = 'None'
    segmentation_femurstlDisplay.GlyphType = 'Arrow'
    segmentation_femurstlDisplay.GlyphTableIndexArray = 'None'
    segmentation_femurstlDisplay.GaussianRadius = 0.52194580078125
    segmentation_femurstlDisplay.SetScaleArray = [None, '']
    segmentation_femurstlDisplay.ScaleTransferFunction = 'PiecewiseFunction'
    segmentation_femurstlDisplay.OpacityArray = [None, '']
    segmentation_femurstlDisplay.OpacityTransferFunction = 'PiecewiseFunction'
    segmentation_femurstlDisplay.DataAxesGrid = 'GridAxesRepresentation'
    segmentation_femurstlDisplay.PolarAxes = 'PolarAxesRepresentation'

    # show data in view
    segmentation_femur_wiresstlDisplay = Show(segmentation_femur_wiresstl, renderView1, 'GeometryRepresentation')

    # trace defaults for the display properties.
    segmentation_femur_wiresstlDisplay.Representation = 'Surface'
    segmentation_femur_wiresstlDisplay.ColorArrayName = [None, '']
    segmentation_femur_wiresstlDisplay.SelectTCoordArray = 'None'
    segmentation_femur_wiresstlDisplay.SelectNormalArray = 'None'
    segmentation_femur_wiresstlDisplay.SelectTangentArray = 'None'
    segmentation_femur_wiresstlDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
    segmentation_femur_wiresstlDisplay.SelectOrientationVectors = 'None'
    segmentation_femur_wiresstlDisplay.ScaleFactor = 9.296994972229005
    segmentation_femur_wiresstlDisplay.SelectScaleArray = 'None'
    segmentation_femur_wiresstlDisplay.GlyphType = 'Arrow'
    segmentation_femur_wiresstlDisplay.GlyphTableIndexArray = 'None'
    segmentation_femur_wiresstlDisplay.GaussianRadius = 0.46484974861145023
    segmentation_femur_wiresstlDisplay.SetScaleArray = [None, '']
    segmentation_femur_wiresstlDisplay.ScaleTransferFunction = 'PiecewiseFunction'
    segmentation_femur_wiresstlDisplay.OpacityArray = [None, '']
    segmentation_femur_wiresstlDisplay.OpacityTransferFunction = 'PiecewiseFunction'
    segmentation_femur_wiresstlDisplay.DataAxesGrid = 'GridAxesRepresentation'
    segmentation_femur_wiresstlDisplay.PolarAxes = 'PolarAxesRepresentation'

    # update the view to ensure updated data information
    renderView1.Update()

    # change solid color
    segmentation_femur_wiresstlDisplay.AmbientColor = [1.0, 1.0, 0.0]
    segmentation_femur_wiresstlDisplay.DiffuseColor = [1.0, 1.0, 0.0]

    # ================================================================
    # addendum: following script captures some of the application
    # state to faithfully reproduce the visualization during playback
    # ================================================================

    # get layout
    layout1 = GetLayout()

    # --------------------------------
    # saving layout sizes for layouts

    # layout/tab size in pixels
    layout1.SetSize(866, 780)

    # -----------------------------------
    # saving camera placements for views

    # current camera placement for renderView1
    renderView1.CameraPosition = [230.80556325282282, -162.27554399127564, -1805.7694344571757]
    renderView1.CameraFocalPoint = [203.418271000369, -148.44984503432718, -1793.8450018543017]
    renderView1.CameraViewUp = [0.12379209123259044, -0.4960295986051203, 0.8594359519219018]
    renderView1.CameraParallelScale = 8.519062667601332

ResetCamera()
# --------------------------------------------
# uncomment the following to render all views
# RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).

# paraview.simple.Box(Center=(-0.3667695,10.1671895,95.5673735),XLength = 91.910263, YLength =  71.482658, ZLength = 71.482658)