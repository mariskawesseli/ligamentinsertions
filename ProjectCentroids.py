import pandas as pd
import os
import trimesh
import numpy as np
import matplotlib.path as plt
import copy
import time

def heron(a,b,c):
    s = (a + b + c) / 2
    area = (s*(s-a) * (s-b)*(s-c)) ** 0.5
    return area

def distance3d(x1,y1,z1,x2,y2,z2):
    a=(x1-x2)**2+(y1-y2)**2 + (z1-z2)**2
    d= a ** 0.5
    return d

def area(x1,y1,z1,x2,y2,z2,x3,y3,z3):
    a=distance3d(x1,y1,z1,x2,y2,z2)
    b=distance3d(x2,y2,z2,x3,y3,z3)
    c=distance3d(x3,y3,z3,x1,y1,z1)
    A = heron(a,b,c)
    return A
    # print("area of triangle is %r " %A)

# A utility function to calculate area
# of triangle formed by (x1, y1),
# (x2, y2) and (x3, y3)

# def area(x1, y1, x2, y2, x3, y3):
# 	return abs((x1 * (y2 - y3) + x2 * (y3 - y1)
# 	            + x3 * (y1 - y2)) / 2.0)


# A function to check whether point P(x, y)
# lies inside the triangle formed by
# A(x1, y1), B(x2, y2) and C(x3, y3)
def isInside(p1, p2, p3, p):
    x1 = p1[0]
    y1 = p1[1]
    z1 = p1[2]
    x2 = p2[0]
    y2 = p2[1]
    z2 = p2[2]
    x3 = p3[0]
    y3 = p3[1]
    z3 = p3[2]
    x = p[0]
    y = p[1]
    z = p[2]

    # Calculate area of triangle ABC
    A = area(x1, y1,z1, x2, y2,z2, x3, y3,z3)

    # Calculate area of triangle PBC
    A1 = area(x, y,z, x2, y2,z2, x3, y3,z3)

    # Calculate area of triangle PAC
    A2 = area(x1, y1,z1, x, y, z,x3, y3,z3)

    # Calculate area of triangle PAB
    A3 = area(x1, y1,z1, x2, y2,z2, x, y,z)

    # Check if sum of A1, A2 and A3
    # is same as A
    if abs(A - (A1 + A2 + A3)) < 1e-6:
        return True
    else:
        return False

def intersection(planeNormal,planePoint,rayDirection,rayPoint):
    epsilon=1e-6

    #Define plane
    # planeNormal = np.array([0, 0, 1])
    # planePoint = np.array([0, 0, 5]) #Any point on the plane

    #Define ray
    # rayDirection = np.array([0, -1, -1])
    # rayPoint = np.array([0, 0, 10]) #Any point along the ray

    ndotu = planeNormal.dot(rayDirection)

    if abs(ndotu) < epsilon:
        intersect = 0
    else:
        w = rayPoint - planePoint[0,:]
        si = -planeNormal.dot(w) / ndotu
        Psi = w + si * rayDirection + planePoint[0,:]
        if isInside(planePoint[0], planePoint[1], planePoint[2], Psi)  == False:
            intersect = 0
        else:
            intersect = Psi[0]

    return intersect


subjects = [9] #[9,13,19,]  #23,26,29,32,35,37,
segment = 'femur'

df = pd.read_excel(os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData","surfaces2.xlsx"),
                        sheet_name='perc_of_len femur')
lig_names = ['PCL']  #, 'MCL-p','MCL-d','posterior oblique','ACL','LCL (prox)','popliteus (dist)'

for subject in subjects:
    if subject in [9, 13, 26, 29, 32]:
        side = 'R'
    else:
        side = 'L'
    path = os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData", str(subject))

    mesh = path + '/Segmentation_' + segment + '_transform.stl'
    bone = trimesh.load_mesh(mesh)
    rot_mat = np.linalg.inv(np.loadtxt(path + '\Segmentation_' + segment + '_resample._ACS.txt'))

    AP_size = np.max(bone.vertices[:,1])-np.min(bone.vertices[:,1])  # AP length

    Rx = trimesh.transformations.rotation_matrix(1.57, [0, 1, 0])
    for name in lig_names:
        if side == 'R':
            most_med_point = np.min(bone.vertices[:, 0])  # med
            most_lat_point = np.max(bone.vertices[:, 0])  # lat
            med_section_dir = + 15
            lat_section_dir = - 15
        else:
            most_med_point = np.max(bone.vertices[:, 0])  # med
            most_lat_point = np.min(bone.vertices[:, 0])  # lat
            med_section_dir = - 10
            lat_section_dir = + 10
        if name == 'PCL' or name == 'ACL':
            most_med_point = most_med_point*0.10
            most_lat_point = most_lat_point*0.10
            med_section = most_med_point #- med_section_dir
            lat_section = most_lat_point #- lat_section_dir
        else:
            most_med_point = most_med_point
            most_lat_point = most_lat_point
            med_section = most_med_point + med_section_dir
            lat_section = most_lat_point + lat_section_dir

        if name == 'PCL' or name == 'MCL-p' or name == 'MCL-d' or name == 'posterior oblique':
            LCL_points = [most_med_point * np.ones(10),
                          np.min(bone.vertices[:, 1]) + np.asarray(AP_size * df[name + 'y'][0:10]),
                          np.min(bone.vertices[:, 2]) + np.asarray(AP_size * df[name + 'z'][0:10])]
            LCL_points = np.transpose(np.asarray(LCL_points))
        else:
            LCL_points = [most_lat_point*np.ones(10), np.min(bone.vertices[:,1])+np.asarray(AP_size*df[name+'y'][0:10]), np.min(bone.vertices[:,2])+np.asarray(AP_size*df[name+'z'][0:10])]
            LCL_points = np.transpose(np.asarray(LCL_points))

        for pts in range(0,10):
            if not np.isnan(LCL_points[pts,:]).any():
                intersect = []
                bone_part = copy.deepcopy(bone)
                top = max(LCL_points[:,2])+2.5
                far_verts = bone_part.vertices[:, 2] < top
                face_mask = far_verts[bone_part.faces].all(axis=1)
                bone_part.update_faces(face_mask)
                if name == 'PCL' or name == 'MCL-p' or name == 'MCL-d' or name == 'posterior oblique':
                    if side == 'R':
                        far_verts = bone_part.vertices[:, 0] < med_section
                    else:
                        far_verts = bone_part.vertices[:, 0] > med_section
                    face_mask = far_verts[bone_part.faces].all(axis=1)
                    bone_part.update_faces(face_mask)
                    # trimesh.Scene(bone_part).show()
                else:
                    if side == 'R':
                        far_verts = bone_part.vertices[:, 0] > lat_section
                    else:
                        far_verts = bone_part.vertices[:, 0] < lat_section
                    face_mask = far_verts[bone_part.faces].all(axis=1)
                    bone_part.update_faces(face_mask)
                    # trimesh.Scene(bone_part).show()
                # tic = time.perf_counter()
                for count, tr in enumerate(bone_part.face_normals):
                    intersect.append(intersection(tr, bone_part.vertices[bone_part.faces[count,:]], np.array([1,0,0]), LCL_points[pts,:]))
                # toc = time.perf_counter()
                # print(f"Downloaded the tutorial in {toc - tic:0.4f} seconds")

                # T = trimesh.transformations.translation_matrix(LCL_points[pts])
                # point = trimesh.creation.cylinder(0.5, height=0.5, sections=None, segment=None, transform=T)
                # trimesh.Scene([bone_part, point]).show()

                x_coord = [i for i in intersect if i != 0]
                if not len(x_coord) == 0:
                    if name == 'MCL-p' or name == 'MCL-d' or name == 'posterior oblique':
                        to_use = np.argmin(abs(x_coord - most_med_point))
                    elif name == 'PCL':
                        to_use = np.argmin(abs(x_coord - most_med_point))
                    elif name == 'ACL':
                        to_use = np.argmin(abs(x_coord - most_lat_point))
                    else:
                        to_use = np.argmax(abs(x_coord - most_lat_point))
                    if not abs(x_coord[to_use]-LCL_points[pts, 0]) > 20:
                        LCL_points[pts, 0] = x_coord[to_use]


    # points = trimesh.PointCloud(LCL_points, colors=None, metadata=None) # create point cloud

        points = []
        for ind in range(0,10):
            T = trimesh.transformations.translation_matrix(LCL_points[ind])
            R = np.linalg.inv(rot_mat)
            M = trimesh.transformations.concatenate_matrices(R, T, Rx)
            point = trimesh.creation.cylinder(0.5, height=0.5, sections=None, segment=None, transform=M)
            # point = trimesh.creation.icosphere(subdivisions=3, radius=1.0, color=None, transform=T)
            if ind == 0:
                points = point
            else:
                points = trimesh.boolean.union([points,point])

        points.export(os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData", str(subject),name+'centroids.stl'))
