# Estimate ligament attachment locations

---
## Overview
A tool to estimate the ligament attachment locations of the knee joint.
Included ligaments: ACL, PCL, LCL, MCL proximal, MCL distal, posterior oblique, popliteus

--- 
## Usage 
### Fit SSM to new bone
 Make sure you have a SSM of the specific bone in Shapeworks and know which points represent the ligament attachments (see https://gitlab.tudelft.nl/mariskawesseli/kneessm).

 - Segment bone from images.
 
 - Groom pipeline for new bone (see this repository: https://gitlab.tudelft.nl/mariskawesseli/kneessm).
 
 - Position the particle file (SSM point cloud) at the original bone location (FitSSM_mri.py)
 Make sure the original bone is not too large, to avoid very slow ICP. Aim for a file size below 10MB (about 20,000 faces) (e.g. using Meshlab - Simplificaion: Quadric Edge Collapse Decimation).
 Required variables:
 
	- subjects => name of the subjects you want to process
  
	- sides => for each subject the side that is being analyzed
  
	- segments => segments you want to analyze
  
	- short_ssm => for each segment, is the shorter SSM needed (0=false, 1=true)
  
	- no_particles => for each segment, number of particles in the SSM
 
 - Get the points associated with all ligament locations on the original bone mesh location
 
 - For each ligament, determine the SSM points associated to the ligament attachments (adaptLigaments.py)
 Interpolate the points to obtain the number of points needed for the OpenSim model
 Write points to osim file
 
 - Scale the ligament parameter based on the length of the ligament (still in Matlab)

---
## License
 https://creativecommons.org/licenses/by/4.0/