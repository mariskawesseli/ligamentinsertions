import glob
import shutil
import os
import DICOMScalarVolumePlugin
import slicer
import vtk
#exec(open(r'C:\Users\mariskawesseli\Documents\GitLab\Other\LigamentStudy\SlicerExportXray.py').read())

subjects = [13,19,23,26,29,32,35,37,41]  # 9
for subject in subjects:
    lig_names = ['PCL', 'MCL-p','MCL-d','posterior oblique','ACL','LCL (prox)','popliteus (dist)']
    path = os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData", str(subject),'DRR')
    slicer.mrmlScene.Clear(0)
    slicer.util.loadScene(glob.glob(os.path.join(path,"*.mrml"))[0])
    no_med=-1
    no_lat=-1
    for name in lig_names:
        slicer.util.loadSegmentation(os.path.join(r'C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData',str(subject),name+'centroids.stl'))

        if name == 'PCL' or name == 'MCL-p' or name == 'MCL-d' or name == 'posterior oblique':
            segmentationNode = slicer.util.getNode('Segmentation_med')
        else:
            segmentationNode = slicer.util.getNode('Segmentation_lat')
        segmentationNode.GetSegmentation().CopySegmentFromSegmentation(slicer.util.getNode(name+'centroids').GetSegmentation(),name+'centroids')

        labelmapVolumeNode = slicer.mrmlScene.AddNewNodeByClass('vtkMRMLLabelMapVolumeNode')
        # slicer.modules.segmentations.logic().ExportVisibleSegmentsToLabelmapNode(segmentationNode, labelmapVolumeNode)
        segmentIds = vtk.vtkStringArray()
        segmentIds.InsertNextValue(name + 'centroids')
        slicer.vtkSlicerSegmentationsModuleLogic.ExportSegmentsToLabelmapNode(segmentationNode, segmentIds, labelmapVolumeNode)

        outputvolumenode = slicer.mrmlScene.AddNewNodeByClass("vtkMRMLScalarVolumeNode", 'Labelmap'+name)
        sef = slicer.modules.volumes.logic().CreateScalarVolumeFromVolume(slicer.mrmlScene, outputvolumenode, labelmapVolumeNode)
        volumeNode = slicer.util.getNode("Labelmap"+name)
        voxels = slicer.util.arrayFromVolume(volumeNode)
        voxels[voxels==1] = 8000
        voxels[voxels==2] = 8000
        voxels[voxels==3] = 8000
        voxels[voxels==4] = 8000
        voxels[voxels==0] = -8000

        rtImagePlan = slicer.util.getNode("RTPlan")
        if name=='PCL' or name=='MCL-p' or name == 'MCL-d' or name=='posterior oblique':
            beam_name = "NewBeam_med"
            no_med +=1
            no=no_med
        else:
            beam_name = "NewBeam_lat"
            no_lat +=1
            no=no_lat
        rtImageBeam = rtImagePlan.GetBeamByName(beam_name)
        Volume = slicer.util.getNode("Labelmap"+name)
        # Create DRR image computation node for user imager parameters
        drrParameters = slicer.mrmlScene.AddNewNodeByClass('vtkMRMLDrrImageComputationNode', 'rtImageBeamParams')
        # Set and observe RTImage beam by the DRR node
        drrParameters.SetAndObserveBeamNode(rtImageBeam)
        # Get DRR computation logic
        drrLogic = slicer.modules.drrimagecomputation.logic()
        # Update imager markups for the 3D view and slice views (optional)
        drrLogic.UpdateMarkupsNodes(drrParameters)
        # Update imager normal and view-up vectors (mandatory)
        drrLogic.UpdateNormalAndVupVectors(drrParameters) # REQUIRED
        # Compute DRR image
        drr_image = drrLogic.ComputePlastimatchDRR(drrParameters, Volume)
        # slicer.mrmlScene.Clear(0)
        if no == 0:
            volumeNode = slicer.util.getNode("DRR : " + beam_name)
        else:
            volumeNode = slicer.util.getNode("DRR : " + beam_name + "_" + str(no))
        outputFolder = os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData", str(subject), "DRR")
        # Create patient and study and put the volume under the study
        shNode = slicer.vtkMRMLSubjectHierarchyNode.GetSubjectHierarchyNode(slicer.mrmlScene)
        patientItemID = shNode.CreateSubjectItem(shNode.GetSceneItemID(), "test patient")
        studyItemID = shNode.CreateStudyItem(patientItemID, "test study")
        volumeShItemID = shNode.GetItemByDataNode(volumeNode)
        shNode.SetItemParent(volumeShItemID, studyItemID)
        exporter = DICOMScalarVolumePlugin.DICOMScalarVolumePluginClass()
        exportables = exporter.examineForExport(volumeShItemID)
        for exp in exportables:
            exp.directory = outputFolder

        exporter.export(exportables)
        folders = [x[0] for x in os.walk(outputFolder)]
        im_folder = [s for s in folders if str(volumeShItemID) in s]
        shutil.move(im_folder[0] + '\IMG0001.dcm', outputFolder + '/' + name + '0001.dcm')
        os.rmdir(im_folder[0])

    names = ['all','med','lat']
    for name in names:
        volumeNode = slicer.util.getNode("Segmentation_"+name+'-label')
        voxels = slicer.util.arrayFromVolume(volumeNode)
        voxels[voxels==1] = 8000
        voxels[voxels==2] = 8000
        voxels[voxels==3] = 8000
        voxels[voxels==4] = 8000
        voxels[voxels == 5] = 8000
        voxels[voxels == 6] = 8000
        voxels[voxels == 7] = 8000
        voxels[voxels == 8] = 8000
        voxels[voxels==0] = -8000

    names = ["med_fem", "lat_fem", "med_wires", "lat_wires", "med_all_wires", "lat_all_wires"]
    for name in names:
        rtImagePlan = slicer.util.getNode("RTPlan")
        if 'lat' in name:
            beam_name = "NewBeam_lat"
            no_lat += 1
            no = no_lat
        else:
            beam_name = "NewBeam_med"
            no_med += 1
            no = no_med
        rtImageBeam = rtImagePlan.GetBeamByName(beam_name)
        if 'fem' in name:
            Volume = slicer.util.getNode("resampled06")
        elif 'med_wires' in name:
            Volume = slicer.util.getNode("Segmentation_med-label")
        elif 'lat_wires' in name:
            Volume = slicer.util.getNode("Segmentation_lat-label")
        elif 'all' in name:
            Volume = slicer.util.getNode("Segmentation_all-label")
        # Create DRR image computation node for user imager parameters
        drrParameters = slicer.mrmlScene.AddNewNodeByClass('vtkMRMLDrrImageComputationNode', 'rtImageBeamParams')
        # Set and observe RTImage beam by the DRR node
        drrParameters.SetAndObserveBeamNode(rtImageBeam)
        # Get DRR computation logic
        drrLogic = slicer.modules.drrimagecomputation.logic()
        # Update imager markups for the 3D view and slice views (optional)
        drrLogic.UpdateMarkupsNodes(drrParameters)
        # Update imager normal and view-up vectors (mandatory)
        drrLogic.UpdateNormalAndVupVectors(drrParameters) # REQUIRED
        # Compute DRR image
        drr_image = drrLogic.ComputePlastimatchDRR(drrParameters, Volume)
        # slicer.mrmlScene.Clear(0)

        if no == 0:
            volumeNode = slicer.util.getNode("DRR : " + beam_name)  #getNode("DRR : Beam_" + name)
        else:
            volumeNode = slicer.util.getNode("DRR : " + beam_name + '_' + str(no))
        outputFolder = os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData", str(subject), "DRR")
        # Create patient and study and put the volume under the study
        shNode = slicer.vtkMRMLSubjectHierarchyNode.GetSubjectHierarchyNode(slicer.mrmlScene)
        patientItemID = shNode.CreateSubjectItem(shNode.GetSceneItemID(), "test patient")
        studyItemID = shNode.CreateStudyItem(patientItemID, "test study")
        volumeShItemID = shNode.GetItemByDataNode(volumeNode)
        shNode.SetItemParent(volumeShItemID, studyItemID)
        exporter = DICOMScalarVolumePlugin.DICOMScalarVolumePluginClass()
        exportables = exporter.examineForExport(volumeShItemID)
        for exp in exportables:
            exp.directory = outputFolder

        exporter.export(exportables)
        folders = [x[0] for x in os.walk(outputFolder)]
        im_folder = [s for s in folders if str(volumeShItemID) in s]
        shutil.move(im_folder[0] + '\IMG0001.dcm', outputFolder+'/' + name + '0001.dcm')
        os.rmdir(im_folder[0])

# in slicer
# import resampled data
# import segmented seperate wires as segmentation
# create 3 new segmentations (all, med, lat) with resampled image as master volume
# in segmentations - copy wires segmentation to segmentation resampled volume
# add correct wires to med/lat/all
# export visible segments to label map
# in volumes - convert to scalar volume

# import resampled femur
# external beam planning
# Ref volume: resampled06
# Gantry: 101/281
# Structure set: segmentsation all
# DRR image computation
# export to DICOM - crate dicom series
