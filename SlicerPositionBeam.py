#exec(open(r'C:\Users\mariskawesseli\Documents\GitLab\Other\LigamentStudy\SlicerPositionBeam.py').read())
import os,glob
subject = 23

path = os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData", str(subject),'DRR')
slicer.mrmlScene.Clear(0)
slicer.util.loadScene(glob.glob(os.path.join(path,"*.mrml"))[0])
# Create dummy RTPlan
rtImagePlan = getNode("RTPlan")
rtImageBeam = rtImagePlan.GetBeamByName("NewBeam_lat")
# Set required beam parameters
current_angle = rtImageBeam.GetGantryAngle()
rtImageBeam.SetGantryAngle(current_angle-7)
rtImageBeam.SetCouchAngle(355)

rtImageBeam2 = rtImagePlan.GetBeamByName("NewBeam_med")
rtImageBeam2.SetGantryAngle(current_angle-7+180)
rtImageBeam2.SetCouchAngle(355)

# # Get CT volume
# ctVolume = getNode('resampled06')
# # Create DRR image computation node for user imager parameters
# drrParameters = slicer.mrmlScene.AddNewNodeByClass('vtkMRMLDrrImageComputationNode', 'rtImageBeamParams')
# # Set and observe RTImage beam by the DRR node
# drrParameters.SetAndObserveBeamNode(rtImageBeam)
# # Get DRR computation logic
# drrLogic = slicer.modules.drrimagecomputation.logic()
# # Update imager markups for the 3D view and slice views (optional)
# drrLogic.UpdateMarkupsNodes(drrParameters)
# # Update imager normal and view-up vectors (mandatory)
# drrLogic.UpdateNormalAndVupVectors(drrParameters) # REQUIRED
# # Compute DRR image
# drrLogic.ComputePlastimatchDRR(drrParameters, ctVolume)

# save scene
slicer.util.saveScene(glob.glob(os.path.join(path,"*.mrml"))[0])
