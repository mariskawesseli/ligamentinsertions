import DICOMScalarVolumePlugin
import slicer
import vtk

volumeNode = slicer.util.getNode("Volume")
voxels = slicer.util.arrayFromVolume(volumeNode)
voxels[voxels==0] = -1000
voxels[voxels==1] = 1000
voxels[voxels==2] = 2000

volumeNode = slicer.util.getNode("LCLpoints7_1-LCLpoints7-label")
voxels = slicer.util.arrayFromVolume(volumeNode)
voxels[voxels==1] = 8000
voxels[voxels==0] = -8000

import seaborn as sns
c = sns.color_palette("viridis_r", n_colors=101, as_cmap=False)
