# Find most anterior  edge of the femoral notch roof - representation Blumensaat line for 3D shapes
# https://journals.lww.com/jbjsjournal/Fulltext/2010/06000/The_Location_of_Femoral_and_Tibial_Tunnels_in.10.aspx?__hstc=215929672.82af9c9a98fa600b1bb630f9cde2cb5f.1528502400314.1528502400315.1528502400316.1&__hssc=215929672.1.1528502400317&__hsfp=1773666937&casa_token=BT765BcrC3sAAAAA:Vu9rn-q5ng4c8339KQuq2mGZDgrAgBStwvn4lvYEbvCgvKQZkbJL24hWbKFdnHTc8VBmAIXA3HVvuWg22-9Mvwv1sw
# https://www.dropbox.com/sh/l7pd43t7c4hrjdl/AABkncBbleifnpLDKSDDc0dCa/D3%20-%20Dimitriou%202020%20-%20Anterior%20cruciate%20ligament%20bundle%20insertions%20vary.pdf?dl=0

import trimesh
import numpy as np
import os
import math
import pandas as pd
import pymeshlab
import seaborn as sns


def findIntersection(x1, y1, x2, y2, x3, y3, x4, y4):
    px = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / (
            (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4))
    py = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / (
            (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4))

    ang = math.atan2(py - y3, px - x3) - math.atan2(y1 - y3, x1 - x3)

    l = math.cos(ang)*np.linalg.norm(np.asarray((x3,y3))-np.asarray((x4,y4)))

    return l, px, py

def split(start, end, segments):
    x_delta = (end[0] - start[0]) / float(segments)
    y_delta = (end[1] - start[1]) / float(segments)
    z_delta = (end[2] - start[2]) / float(segments)
    points = []
    for i in range(1, segments):
        points.append([start[0] + i * x_delta, start[1] + i * y_delta, start[2] + i * z_delta])
    return [start] + points + [end]


ligaments_tib = [[5,7,6,5,3,4,4,5,5,4],  # PCL
                [1,1,1,1,1,1,1,1,1,1],  # MCLp
                [3,3,8,3,5,3,5,0,3,3],  # MCLd
                [0,4,0,0,0,0,0,0,0,0],  # MCLd2
                [4,5,3,4,4,5,3,2,4,0],  # POL
                [0,6,4,0,0,0,0,0,0,0],  # POL2
                [0,0,5,0,0,0,0,0,0,0],  # POL3
                [0,0,7,0,0,0,0,0,0,0],  # POL4
                [6,8,9,6,6,6,6,6,6,5],  # ACL
                [2,2,2,2,2,2,2,3,2,2],  # LCL
                [0,0,0,0,0,0,0,0,0,0]]  # POP

ligaments = ligaments_tib

# find most ant point in yz plane
subjects = [100]  # [9,13,19,23,26,29,32,35,37,41]  #
lig = 'ACL'
segment = 'tibia'

d = []
h = []
h_centriods = []
d_centriods = []
for ind, subject in enumerate(subjects):
    if subject in [9, 13, 26, 29, 32]:
        side = 'R'
        reflect = ''
    else:
        side = 'L'
        reflect = '.reflect'

    if subject == 100:
        path = os.path.join(
            r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone\new_bone\shape_models\mean_shape_rot.stl')
        path_col = r'C:\\Users\\mariskawesseli\\Documents\\GitLab\\knee_ssm\\OAI\\Output/tibia_bone\\new_bone\\shape_models'
        side = 'R'
    else:
        path = os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData", str(subject), 'Segmentation_tibia_transform.STL')

    mesh = trimesh.load_mesh(path)
    verts = mesh.vertices
    AP = mesh.bounding_box.bounds[1,1] - mesh.bounding_box.bounds[0,1]
    ML = mesh.bounding_box.bounds[1,0] - mesh.bounding_box.bounds[0,0]
    bbox = mesh.bounding_box.bounds

    # posterior_mesh = trimesh.intersections.slice_mesh_plane(mesh, (0,-1,0), (0,20,0), cached_dots=None, return_both=False)
    # posterior_mesh.show()

    # find anterior line
    lines, to_3D, face_index = trimesh.intersections.mesh_multiplane(mesh, (0, 0, 0), (0, 0, 1),
                                                                     heights=np.linspace(-15, 5, 21))
    ant_point = []
    prox_point = []
    for i in range(0, len(face_index)):
        plane_verts = np.unique(mesh.faces[face_index[i]])
        plane_points = mesh.vertices[plane_verts]

        # goon = 1
        # tel = 2
        # while goon == 1:
        min_y = np.where(plane_points[:, 1] == plane_points[:, 1].max())
        ant_point.append(plane_points[min_y])
            # min_y2 = np.where(plane_points[:, 1] == np.partition(plane_points[:, 1], tel + 1)[tel + 1])
            # z_min2 = plane_points[min_y2][0][1]
            # if z_min - z_min2 > -15:
            # 	goon = 1
            # 	tel += 1
            # else:
            # 	goon = 0
        # dist_point.append(plane_points[min_y][0])
        # min_y = np.where(plane_points[:, 1] == plane_points[:, 1].min())
        # prox_point.append(plane_points[min_y][0])

    # most_ant_ind1 = np.asarray(dist_point)[:, 1].argmax()
    # most_ant_ind2 = np.asarray(prox_point)[:, 1].argmax()
    p1=ant_point[np.argmin(np.squeeze(np.array(ant_point))[:,1])][0]

    # min_y = mesh.vertices[np.argmin(mesh.vertices[:, 1])]
    # p1 = min_y
    p2 = np.array((0,p1[1],p1[2]))

    # find posterior line
    min_y = mesh.vertices[np.argmin(mesh.vertices[:, 1])]
    p3 = min_y
    p4 = np.array((0, min_y[1], min_y[2]))

    # find medial line
    min_x = mesh.vertices[np.argmin(mesh.vertices[:, 0])]
    p5 = min_x
    p6 = np.array((min_x[0], 0, min_x[2]))

    # find lateral line
    max_x = mesh.vertices[np.argmax(mesh.vertices[:, 0])]
    p7 = max_x
    p8 = np.array((max_x[0], 0, max_x[2]))


    # find height
    # vec1 = (p1[0][0] - p2[0][0], p1[0][1] - p2[0][1], p1[0][2] - p2[0][2])
    # norm = np.sqrt(vec1[0] ** 2 + vec1[1] ** 2 + vec1[2] ** 2)
    # direction = [vec1[0] / norm, vec1[1] / norm, vec1[2] / norm]


    # segments = np.asarray([p1[-1], p2[-1]])
    # p = trimesh.load_path(segments)

    # trimesh.path.segments.parameters_to_segments(p1[-1], -1*direction, ((0,0,0),(0,1,0)))
    # trimesh.path.segments.segments_to_parameters(np.asarray(segments))

    # posterior_mesh = trimesh.intersections.slice_mesh_plane(mesh, direction, (0,0,10), cached_dots=None, return_both=False)



    # segments = np.asarray([p3[np.asarray(dist).argmax()], p4[np.asarray(dist).argmax()]])
    # p_dist = trimesh.load_path(segments)
    p1_2d = p1[0:2]
    p2_2d = p2[0:2]
    p3_2d = p3[0:2]
    # d.append(np.linalg.norm(np.cross(p2_2d-p1_2d, p1_2d-p3_2d))/np.linalg.norm(p2_2d-p1_2d))
    d.append(p3[1]-p1[1])

    # find depth
    p5_2d = p5[0:2]
    p6_2d = p6[0:2]
    p7_2d = p7[0:2]
    # h.append(np.linalg.norm(np.cross(p6_2d - p5_2d, p5_2d - p7_2d)) / np.linalg.norm(p6_2d - p7_2d))
    h.append(p7[0] - p5[0])

    # visualization
    # p1[0][0] = 0
    # p2[0][0] = 0
    # p3[np.asarray(dist1).argmax()][0] = 0
    # p4[jump_ind + 1][0] = 0
    # p5[0] = 0
    # p6[jump_ind + 1][0] = 0

    points = trimesh.points.PointCloud(np.asarray((p1,p2,p3,p4,p5,p6,p7,p8)), colors=None, metadata=None)
    # segments = np.asarray([p1[-1], p2[-1]])
    # p = trimesh.load_path(segments)
    # segments = np.asarray([p6[jump_ind+1], p5])
    # p_dist = trimesh.load_path(segments)

    mesh.visual.face_colors[:] = np.array([227, 218, 201, 100])
    mesh.visual.vertex_colors[:] = np.array([227, 218, 201, 100])
    direction = (0,1,0)
    direction_perp = (1,0,0)
    line = trimesh.path.segments.parameters_to_segments([p5,p7,p1,p3], [direction,direction,direction_perp,direction_perp],
                                                        np.array(((27,d[-1]+30),(-26,-d[-1]-27),(-42,h[-1]-37),(48,-h[-1]+46))).astype(float))

    box_points = trimesh.load_path(np.squeeze(line)).vertices
    grid_points1 = split(box_points[0], box_points[5], 4)
    grid_points2 = split(box_points[0], box_points[2], 4)
    grid_line = trimesh.path.segments.parameters_to_segments([grid_points1[1], grid_points1[2], grid_points1[3]],
                                                             [direction_perp], np.array(
            ((h[-1] + 4, -0), (h[-1] + 2.5, 0), (h[-1] + 0.5, -0))).astype(float))
    grid_line2 = trimesh.path.segments.parameters_to_segments([grid_points2[1], grid_points2[2], grid_points2[3]],
                                                              [direction],
                                                              np.array(((d[-1] - 1.5, 0), (d[-1] - 1.5, 0),
                                                                        (d[-1] - 1.5, 0))).astype(
                                                                  float))
    grid_line_path = trimesh.load_path(np.squeeze(grid_line),
                                       colors=((0.5, 0.5, 0.5,), (0.5, 0.5, 0.5), (0.5, 0.5, 0.5)))
    grid_line2_path = trimesh.load_path(np.squeeze(grid_line2),
                                        colors=((0.5, 0.5, 0.5,), (0.5, 0.5, 0.5), (0.5, 0.5, 0.5)))

    scene = trimesh.Scene([mesh, trimesh.load_path(np.squeeze(line)),grid_line_path,grid_line2_path])  #, points
    scene.show()
    # mesh.vertices[:, 0] = 0
    # trimesh.Scene([mesh, points, trimesh.load_path(np.squeeze(line))]).show()

# posterior_mesh = trimesh.intersections.slice_mesh_plane(mesh, direction, (0,-30,0), cached_dots=None, return_both=False)
# posterior_mesh.show()
    if subject == 100:
        points_lig = trimesh.load_mesh(r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone\new_bone\shape_models\meanshape_ligs_rot.xyz')
        if lig == 'ACL':
            center = np.arange(470 - 341) + 341  # ACL
            mean = np.array((48.2, -45.1, -9.4))/100 * np.array((ML,AP,AP)) + np.array((bbox[0,0],bbox[1,1],bbox[1,2]))
        else:
            center = np.arange(131)  # PCL np.array((0,0,0)) #
            mean = np.array((50.8, -87.6, -23.9))/100 * np.array((ML,AP,AP)) + np.array((bbox[0,0],bbox[1,1],bbox[1,2]))
        points_lig = points_lig[center]
        # origin, xaxis, yaxis, zaxis = [0, 0, 0], [1, 0, 0], [0, 1, 0], [0, 0, 1]
        # Rz = trimesh.transformations.rotation_matrix(180/np.pi, zaxis)
        # points_lig.apply_transform(Rz)
        color_file = np.loadtxt(path_col + '\meanshape_ligs_color.xyz')[:, 3]
        color_file = color_file[center]
        c = sns.color_palette("viridis_r", n_colors=10, as_cmap=False)

        color = []
        for ind_col, point in enumerate(points_lig):
            center_2d = point[1:3]
            h_centriods.append(np.linalg.norm(np.cross(p2_2d - p1_2d, p1_2d - center_2d)) / np.linalg.norm(p2_2d - p1_2d))
            l, px, py = findIntersection(p1_2d[0], p1_2d[1], p2_2d[0], p2_2d[1], center_2d[0], center_2d[1], p5_2d[0],
                                         p5_2d[1])
            d_centriods.append(l)
            vcolors=[c[int(color_file[ind_col] - 1)][0] * 255, c[int(color_file[ind_col] - 1)][1] * 255,
                              c[int(color_file[ind_col] - 1)][2] * 255]
            color.append(vcolors)
        p_lig = trimesh.points.PointCloud(points_lig, colors=color)
        p_mean = trimesh.primitives.Sphere(radius=1, center=mean, subdivisions=3, color=[255, 0, 0])  # trimesh.points.PointCloud([mean,mean], colors=[[255,0,0],[255,0,0]])
        p_mean.visual.face_colors = np.array([255, 0, 0, 255])
        # scene2 = trimesh.Scene([mesh, points, p_lig, trimesh.load_path(np.squeeze(line))])
        # scene2.apply_transform(R)
        # scene2.camera_transform = camera_trans
        # scene2.show()
        scene.add_geometry([p_lig, p_mean])  #p_lig ,transform=R
        scene.show()
    else:
        if lig == 'ACL':
            lig_no = ligaments[8][ind]
        elif lig == 'PCL':
            lig_no = ligaments[0][ind]
        if not lig_no == 0:
            segment = 'femur'
            path = os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData", str(subject))

            rot_mat = np.linalg.inv(np.loadtxt(path + '\Segmentation_' + segment + '_resample._ACS.txt'))
            ms4 = pymeshlab.MeshSet()
            ms4.load_new_mesh(path + '\Segmentation_' + segment + '_area' + str(lig_no) + '.stl')

            ms4.apply_filter('flatten_visible_layers', deletelayer=True)
            ms4.apply_filter('matrix_set_copy_transformation', transformmatrix=rot_mat)
            geometric_measures = ms4.apply_filter('compute_geometric_measures')

            # print('Surface area femur ligament' + str(lig_no) + ': ' + str(surface) + ' mm2')
            center = geometric_measures['shell_barycenter']
            center_2d = center[1:3]
            h_centriods.append(np.linalg.norm(np.cross(p2_2d-p1_2d, p1_2d-center_2d))/np.linalg.norm(p2_2d-p1_2d))
            l, px, py = findIntersection(p1_2d[0], p1_2d[1], p2_2d[0], p2_2d[1], center_2d[0], center_2d[1], p5_2d[0], p5_2d[1])
            d_centriods.append(l)
        else:
            h_centriods.append(0)
            d_centriods.append(0)

[1-abs(i / j) for i, j in zip(d_centriods, d)]
[i / j for i, j in zip(h_centriods, h)]

d_centriods/np.asarray(d)
h_centriods/np.asarray(h)

np.mean(abs(np.asarray(d_centriods))/np.asarray(d))
np.mean(h_centriods/np.asarray(h))



