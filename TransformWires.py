import pymeshlab
import numpy as np
import trimesh
import nrrd
import re
import os
import pandas as pd
from tabulate import tabulate
from shutil import copyfile
from openpyxl import load_workbook

subjects = [9,13,19,23,26,29,32,35,37,41] #9,13,19,23,26,29,32,35,41
segments = ['tibia','femur']  #'femur',
short = 1
ligaments_fem = [[1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
				 [6, 5, 6, 6, 6, 6, 4, 4, 5, 5],
				 [3, 2, 5, 3, 3, 2, 2, 0, 3, 3],
				 [0, 8, 0, 0, 0, 0, 0, 0, 0, 0],  # MCLd2
				 [7, 3, 7, 7, 7, 5, 7, 6, 7, 0],
				 [0, 0, 8, 0, 0, 0, 0, 0, 0, 0],  # POL2
				 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # POL3
				 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # POL4
				 [4, 6, 3, 5, 4, 0, 0, 3, 4, 4],
				 [5, 7, 4, 4, 5, 7, 6, 5, 6, 6],
				 [2, 4, 2, 2, 2, 3, 3, 2, 2, 2]]

ligaments_tib = [[5, 7, 6, 5, 3, 4, 4, 5, 5, 4],
				 [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
				 [3, 3, 8, 3, 5, 3, 5, 0, 3, 3],
				 [0, 4, 0, 0, 0, 0, 0, 0, 0, 0],  # MCLd2
				 [4, 5, 3, 4, 4, 5, 3, 2, 4, 0],
				 [0, 6, 4, 0, 0, 0, 0, 0, 0, 0],  # POL2
				 [0, 0, 5, 0, 0, 0, 0, 0, 0, 0],  # POL3
				 [0, 0, 7, 0, 0, 0, 0, 0, 0, 0],  # POL4
				 [6, 8, 9, 6, 6, 6, 6, 6, 6, 5],
				 [2, 2, 2, 2, 2, 2, 2, 3, 2, 2],
				 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]

ligaments_fib = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # PCL
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # MCLp
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # MCLd
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # MCLd2
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # POL
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # POL2
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # POL3
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # POL4
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # ACL
                 [2, 2, 2, 2, 2, 2, 2, 3, 2, 2],  # LCL
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]  # POP

for segment in segments:
	if segment == 'femur':
		ligaments = ligaments_fem
	else:
		ligaments = ligaments_tib

	for ind, subject in enumerate(subjects):
		path = os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData", str(subject))
		if subject in [9,13,26,29,32]:
			side = 'R'
			reflect = ''
		else:
			side = 'L'
			reflect = '.reflect'

		rot_mat = np.linalg.inv(np.loadtxt(path + '\Segmentation_' + segment + '_resample._ACS.txt'))
		mesh2 = r'C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData/' + str(
			subject) + '\Segmentation_' + segment + '_wires.stl'
		ms5 = pymeshlab.MeshSet()
		ms5.load_new_mesh(mesh2)
		ms5.apply_filter('matrix_set_copy_transformation', transformmatrix=rot_mat)
		ms5.save_current_mesh(path + '\Segmentation_' + segment + '_wires_transform.stl', binary=False)

		for lig in range(0, 11):
			lig_no = ligaments[lig][ind]
			if not lig_no == 0:
				mesh2 = path + '\Segmentation_' + segment + '_area' + str(lig_no) + '.stl'
				# transform femur to local coordinate system to get anatomical directions
				ms5 = pymeshlab.MeshSet()
				ms5.load_new_mesh(mesh2)
				ms5.apply_filter('matrix_set_copy_transformation', transformmatrix=rot_mat)
				ms5.save_current_mesh(path + '\Segmentation_' + segment + '_area' + str(lig_no) + '_transform.stl', binary=False)
