import pymeshlab
import os
import vtk
from VisualiseSSM import create_pointcloud_polydata
import numpy as np
import glob


def load_stl(filename, rot_mat):
    reader = vtk.vtkSTLReader()
    reader.SetFileName(filename)

    transform = vtk.vtkTransform()
    transform.Identity()
    transform.SetMatrix([item for sublist in rot_mat for item in sublist])
    # transform.Translate(10, 0, 0)

    transformFilter = vtk.vtkTransformPolyDataFilter()
    transformFilter.SetInputConnection(reader.GetOutputPort())
    transformFilter.SetTransform(transform)
    transformFilter.Update()

    mapper = vtk.vtkPolyDataMapper()
    if vtk.VTK_MAJOR_VERSION <= 5:
        mapper.SetInput(reader.GetOutput())
    else:
        mapper.SetInputConnection(transformFilter.GetOutputPort())

    actor = vtk.vtkActor()
    actor.SetMapper(mapper)

    return actor


class MyInteractorStyle(vtk.vtkInteractorStyleTrackballCamera):

    def __init__(self,parent=None):
        self.parent = renderWindowInteractor

        self.AddObserver("KeyPressEvent",self.keyPressEvent)

    def keyPressEvent(self,obj,event):
        key = self.parent.GetKeySym()
        if key == 'b':
            vis = outlineActor.GetVisibility()
            if vis:
                outlineActor.SetVisibility(False)
            else:
                outlineActor.SetVisibility(True)

        return


subject = 35  # [9,13,19,23,26,29,32,35,37,41]
segment = 'femur'

ligaments_fem = [[1,1,1,1,1,1,1,1,1,1],
            [6,5,6,6,6,6,4,4,5,5],
            [3,2,5,3,3,2,2,0,3,3],
            [7,8,7,7,7,5,7,6,7,0],
            [4,6,3,5,4,0,0,3,4,4],
            [5,7,4,4,5,7,6,5,6,6],
            [2,4,2,2,2,3,3,2,2,2],
            [0,3,8,0,0,0,0,0,0,0]]
ligaments_tib = [[5,7,6,5,3,4,4,5,5,4],
            [3,3,7,3,5,3,5,4,3,3],
            [1,1,1,1,1,1,1,1,1,1],
            [4,5,3,4,4,5,3,2,4,3],
            [6,8,9,6,6,6,6,6,6,5],
            [2,2,2,2,2,2,2,3,2,2],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0]]

if segment == 'femur':
    ligaments = ligaments_fem
else:
    ligaments = ligaments_tib

ind = np.where(np.asarray([9,13,19,23,26,29,32,35,37,41]) == subject)
# Renderer
renderer = vtk.vtkRenderer()

path = os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData", str(subject))
rot_mat = np.linalg.inv(np.loadtxt(path + '\Segmentation_' + segment + '_resample._ACS.txt'))

for lig in range(0, 8):
    lig_no = ligaments[lig][ind[0][0]]
    if not lig_no == 0:
        ms4 = pymeshlab.MeshSet()
        ms4.load_new_mesh(path + '\Segmentation_' + segment + '_area' + str(lig_no) + '.stl')
        geometric_measures = ms4.apply_filter('compute_geometric_measures')
        surface = geometric_measures['surface_area']
        center= geometric_measures['shell_barycenter']

        point_cloud_lig = create_pointcloud_polydata(np.asarray([center,center]))

        transform = vtk.vtkTransform()
        transform.Identity()
        transform.SetMatrix([item for sublist in rot_mat for item in sublist])
        transformFilter = vtk.vtkTransformPolyDataFilter()
        transformFilter.SetTransform(transform)
        transformFilter.SetInputData(point_cloud_lig)
        transformFilter.Update()

        mapper2 = vtk.vtkPolyDataMapper()
        mapper2.SetInputConnection(transformFilter.GetOutputPort())
        actor2 = vtk.vtkActor()
        actor2.SetMapper(mapper2)
        actor2.GetProperty().SetColor(1, 0, 0)
        actor2.GetProperty().SetPointSize(10)

        # renderer.AddActor(actor)
        renderer.AddActor(actor2)

    Counter = len(glob.glob1(path, 'Segmentation_' + segment + '_area*.stl'))
    for count in range(1, Counter + 1):
        bone_actor = load_stl(path + '\Segmentation_' + segment + '_area' + str(count) + '.stl', rot_mat)
        bone_actor.GetProperty().SetOpacity(0.75)
        bone_actor.GetProperty().SetColor(0, 0, 1)
        renderer.AddActor(bone_actor)

wire_actor = load_stl(path + '/Segmentation_' + segment + '_wires.stl', rot_mat)
wire_actor.GetProperty().SetOpacity(1.0)
wire_actor.GetProperty().SetColor(1, 1, 0)
renderer.AddActor(wire_actor)

reader = vtk.vtkSTLReader()
reader.SetFileName(path + '/Segmentation_' + segment + '_resample.stl')
transform = vtk.vtkTransform()
transform.Identity()
transform.SetMatrix([item for sublist in rot_mat for item in sublist])
transformFilter = vtk.vtkTransformPolyDataFilter()
transformFilter.SetInputConnection(reader.GetOutputPort())
transformFilter.SetTransform(transform)
transformFilter.Update()
mapper = vtk.vtkPolyDataMapper()
mapper.SetInputConnection(transformFilter.GetOutputPort())
bone_actor = vtk.vtkActor()
bone_actor.SetMapper(mapper)
bone_actor.GetProperty().SetOpacity(0.75)

renderer.AddActor(bone_actor)

outline = vtk.vtkOutlineFilter()
outline.SetInputConnection(transformFilter.GetOutputPort())
outlineMapper = vtk.vtkPolyDataMapper()
outlineMapper.SetInputConnection(outline.GetOutputPort())
outlineActor = vtk.vtkActor()
outlineActor.SetMapper(outlineMapper)
outlineActor.GetProperty().SetColor(0,0,0)
outlineActor.SetVisibility(False)

renderer.AddActor(outlineActor)

renderer.SetBackground(1.0, 1.0, 1.0)
renderer.ResetCamera()

# Render Window
renderWindow = vtk.vtkRenderWindow()
renderWindow.AddRenderer(renderer)

# Interactor
renderWindowInteractor = vtk.vtkRenderWindowInteractor()
renderWindowInteractor.SetRenderWindow(renderWindow)
renderWindowInteractor.GetInteractorStyle().SetCurrentStyleToTrackballCamera()
renderWindowInteractor.SetInteractorStyle(MyInteractorStyle())

# Begin Interaction
renderWindow.Render()
renderWindow.SetWindowName("XYZ Data Viewer")
renderWindowInteractor.Start()