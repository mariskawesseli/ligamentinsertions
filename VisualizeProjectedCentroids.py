import vtk
import sys
import os
import vtk
from numpy import random, genfromtxt, size
import trimesh

class VtkPointCloud:
	def __init__(self, zMin=-10.0, zMax=10.0, maxNumPoints=1e6):
		self.maxNumPoints = maxNumPoints
		self.vtkPolyData = vtk.vtkPolyData()
		self.clearPoints()
		mapper = vtk.vtkPolyDataMapper()
		mapper.SetInputData(self.vtkPolyData)
		mapper.SetColorModeToDefault()
		mapper.SetScalarRange(zMin, zMax)
		mapper.SetScalarVisibility(1)
		self.vtkActor = vtk.vtkActor()
		self.vtkActor.SetMapper(mapper)

	def addPoint(self, point):
		if (self.vtkPoints.GetNumberOfPoints() < self.maxNumPoints):
			pointId = self.vtkPoints.InsertNextPoint(point[:])
			self.vtkDepth.InsertNextValue(point[2])
			self.vtkCells.InsertNextCell(1)
			self.vtkCells.InsertCellPoint(pointId)
		else:
			r = random.randint(0, self.maxNumPoints)
			self.vtkPoints.SetPoint(r, point[:])
		self.vtkCells.Modified()
		self.vtkPoints.Modified()
		self.vtkDepth.Modified()

	def clearPoints(self):
		self.vtkPoints = vtk.vtkPoints()
		self.vtkCells = vtk.vtkCellArray()
		self.vtkDepth = vtk.vtkDoubleArray()
		self.vtkDepth.SetName('DepthArray')
		self.vtkPolyData.SetPoints(self.vtkPoints)
		self.vtkPolyData.SetVerts(self.vtkCells)
		self.vtkPolyData.GetPointData().SetScalars(self.vtkDepth)
		self.vtkPolyData.GetPointData().SetActiveScalars('DepthArray')

def load_data(data, pointCloud):
	# data = genfromtxt(filename, dtype=float, usecols=[0, 1, 2])
	for k in range(size(data, 0)):
		point = data[k]  # 20*(random.rand(3)-0.5)
		pointCloud.addPoint(point)

	return pointCloud

def load_stl(filename):
	reader = vtk.vtkSTLReader()
	reader.SetFileName(filename)

	mapper = vtk.vtkPolyDataMapper()
	if vtk.VTK_MAJOR_VERSION <= 5:
		mapper.SetInput(reader.GetOutput())
	else:
		mapper.SetInputConnection(reader.GetOutputPort())

	actor = vtk.vtkActor()
	actor.SetMapper(mapper)

	return actor

def create_pointcloud_polydata(points, colors=None):
	"""https://github.com/lmb-freiburg/demon
	Creates a vtkPolyData object with the point cloud from numpy arrays

	points: numpy.ndarray
		pointcloud with shape (n,3)

	colors: numpy.ndarray
		uint8 array with colors for each point. shape is (n,3)

	Returns vtkPolyData object
	"""
	vpoints = vtk.vtkPoints()
	vpoints.SetNumberOfPoints(points.shape[0])
	for i in range(points.shape[0]):
		vpoints.SetPoint(i, points[i])
	vpoly = vtk.vtkPolyData()
	vpoly.SetPoints(vpoints)

	if not colors is None:
		vcolors = vtk.vtkUnsignedCharArray()
		vcolors.SetNumberOfComponents(3)
		vcolors.SetName("Colors")
		vcolors.SetNumberOfTuples(points.shape[0])
		for i in range(points.shape[0]):
			vcolors.SetTuple3(i, colors[0], colors[1], colors[2])
		vpoly.GetPointData().SetScalars(vcolors)

	vcells = vtk.vtkCellArray()

	for i in range(points.shape[0]):
		vcells.InsertNextCell(1)
		vcells.InsertCellPoint(i)

	vpoly.SetVerts(vcells)

	return vpoly


lig_names = ['PCL', 'MCL-p','MCL-d','posterior oblique','ACL','LCL (prox)','popliteus (dist)']
color = ((0.75,1,0.5),
         (0,0.5,0),
         (1,0,1),
         (0.5,1,1),
         (1,1,0),
         (1,0.5,0.75),
         (1,0.5,0))

if __name__ == '__main__':
	subjects = [35] #9,13,19,23,26,29,32,35,37,41

	segments = ['femur']  #'femur',
	ligaments_fem = [[1,1,1,1,1,1,1,1,1,1],
				[6,5,6,6,6,6,4,4,5,5],
				[3,2,5,3,3,2,2,0,3,3],
				[7,8,7,7,7,5,7,6,7,0],
				[4,6,3,5,4,0,0,3,4,4],
				[5,7,4,4,5,7,6,5,6,6],
				[2,4,2,2,2,3,3,2,2,2],
				[0,3,8,0,0,0,0,0,0,0]]
	ligaments_tib = [[5,7,6,5,3,4,4,5,5,4],
				[3,3,7,3,5,3,5,4,3,3],
				[1,1,1,1,1,1,1,1,1,1],
				[4,5,3,4,4,5,3,2,4,3],
				[6,8,9,6,6,6,6,6,6,5],
				[2,2,2,2,2,2,2,3,2,2],
				[0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0]]

	for segment in segments:
		SSMpoints = [[] for i in range(8)]
		for ind in range(0,8):
			SSMpoints[ind] = [[] for i in range(10)]

		for ind, subject in enumerate(subjects):
			if subject==100:
				path = os.path.join(r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone\new_bone\shape_models')
			else:
				path = os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData", str(subject))

			if subject in [9, 13, 26, 29, 32]:
				side = 'R'
				reflect = ''
			else:
				side = 'L'
				reflect = '.reflect'

			# points = trimesh.load_mesh(r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\femur_bone\new_bone\shape_models\meanshape_bone_no_lig.xyz')
			# point_cloud = create_pointcloud_polydata(points)
			# pointCloud = VtkPointCloud()
			# pointCloud = load_data(point_cloud, pointCloud)
			# points_lig = trimesh.load_mesh(r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\femur_bone\new_bone\shape_models\meanshape_ligs.xyz')
			if subject==100:
				points_lig = trimesh.load_mesh(path + '\meanshape_ligs.xyz')
				point_cloud_lig = create_pointcloud_polydata(points_lig)
				bone_actor = load_stl(path + '/mean_shape.stl')
				bone_actor.GetProperty().SetOpacity(0.75)
			else:
				points_lig = trimesh.load_mesh(path + '\SSM_' + segment + '_pred_points.xyz')  # _areas
				point_cloud_lig = create_pointcloud_polydata(points_lig)
				bone_actor = load_stl(path + '/Segmentation_' + segment + '_resample.stl')
				bone_actor.GetProperty().SetOpacity(0.75)
				wire_actor = load_stl(path + '/Segmentation_' + segment + '_wires.stl')
				wire_actor.GetProperty().SetColor(0, 0, 1)
			lig_actor = []
			for count, lig in enumerate(lig_names):
				lig_actor.append(load_stl(os.path.join(path,lig+'centroids.stl')))
				lig_actor[count].GetProperty().SetColor(color[count])

			mapper2 = vtk.vtkPolyDataMapper()
			mapper2.SetInputData(point_cloud_lig)
			actor2 = vtk.vtkActor()
			actor2.SetMapper(mapper2)
			actor2.GetProperty().SetColor(1, 0, 0)
			actor2.GetProperty().SetPointSize(5)

			# Renderer
			renderer = vtk.vtkRenderer()
			renderer.AddActor(bone_actor)
			if not subject==100:
				renderer.AddActor(wire_actor)
			for count, lig in enumerate(lig_names):
				renderer.AddActor(lig_actor[count])
			# renderer.AddActor(actor2)
			renderer.SetBackground(1.0, 1.0, 1.0)
			renderer.ResetCamera()

			# Render Window
			renderWindow = vtk.vtkRenderWindow()
			renderWindow.AddRenderer(renderer)

			# Interactor
			renderWindowInteractor = vtk.vtkRenderWindowInteractor()
			renderWindowInteractor.SetRenderWindow(renderWindow)
			renderWindowInteractor.GetInteractorStyle().SetCurrentStyleToTrackballCamera()

			# Begin Interaction
			renderWindow.Render()
			renderWindow.SetWindowName("XYZ Data Viewer")
			renderWindowInteractor.Start()

