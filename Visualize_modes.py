import vtk
import sys
import os
import vtk
from numpy import random, genfromtxt, size
import trimesh
import numpy as np
from vtk.util.numpy_support import vtk_to_numpy, numpy_to_vtk
import seaborn as sns

class VtkPointCloud:
    def __init__(self, zMin=-10.0, zMax=10.0, maxNumPoints=1e6):
        self.maxNumPoints = maxNumPoints
        self.vtkPolyData = vtk.vtkPolyData()
        self.clearPoints()
        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInputData(self.vtkPolyData)
        mapper.SetColorModeToDefault()
        mapper.SetScalarRange(zMin, zMax)
        mapper.SetScalarVisibility(1)
        self.vtkActor = vtk.vtkActor()
        self.vtkActor.SetMapper(mapper)

    def addPoint(self, point):
        if (self.vtkPoints.GetNumberOfPoints() < self.maxNumPoints):
            pointId = self.vtkPoints.InsertNextPoint(point[:])
            self.vtkDepth.InsertNextValue(point[2])
            self.vtkCells.InsertNextCell(1)
            self.vtkCells.InsertCellPoint(pointId)
        else:
            r = random.randint(0, self.maxNumPoints)
            self.vtkPoints.SetPoint(r, point[:])
        self.vtkCells.Modified()
        self.vtkPoints.Modified()
        self.vtkDepth.Modified()

    def clearPoints(self):
        self.vtkPoints = vtk.vtkPoints()
        self.vtkCells = vtk.vtkCellArray()
        self.vtkDepth = vtk.vtkDoubleArray()
        self.vtkDepth.SetName('DepthArray')
        self.vtkPolyData.SetPoints(self.vtkPoints)
        self.vtkPolyData.SetVerts(self.vtkCells)
        self.vtkPolyData.GetPointData().SetScalars(self.vtkDepth)
        self.vtkPolyData.GetPointData().SetActiveScalars('DepthArray')

def load_data(data, pointCloud):
    # data = genfromtxt(filename, dtype=float, usecols=[0, 1, 2])
    for k in range(size(data, 0)):
        point = data[k]  # 20*(random.rand(3)-0.5)
        pointCloud.addPoint(point)

    return pointCloud

def load_stl(filename,signed_distance):
    reader = vtk.vtkSTLReader()
    reader.SetFileName(filename)
    reader.Update()
    obj = reader.GetOutputDataObject(0)

    # mapper = vtk.vtkPolyDataMapper()
    # if vtk.VTK_MAJOR_VERSION <= 5:
    # 	mapper.SetInput(reader.GetOutput())
    # else:
    # 	mapper.SetInputConnection(reader.GetOutputPort())

    # vcolors = vtk.vtkUnsignedCharArray()
    # vcolors.SetNumberOfComponents(3)
    # vcolors.SetName("Colors")
    # vcolors.SetNumberOfTuples(signed_distance.shape[0])

    c = sns.color_palette("viridis_r", n_colors=round(max(signed_distance) - min(signed_distance)), as_cmap=False)
    lut = vtk.vtkLookupTable()
    lut.SetNumberOfTableValues(int(round(max(signed_distance) - min(signed_distance))))
    lut.SetTableRange(min(signed_distance),max(signed_distance))
    lut.Build()
    # Fill in a few known colors, the rest will be generated if needed
    for j in range(0,round(max(signed_distance) - min(signed_distance))):
        lut.SetTableValue(j, c[j][0]*255,c[j][1]*255,c[j][2]*255)

    heights = vtk.vtkDoubleArray()
    for i in range(obj.GetNumberOfPoints()):
        z = signed_distance[i]
        heights.InsertNextValue(z)
    obj.GetPointData().SetScalars(heights)
    # for i in range(signed_distance.shape[0]):
    # 	ind = round(signed_distance[i]-min(signed_distance))
    # 	# print(ind)
    # 	vcolors.SetTuple3(i, c[ind-1][0] * 255,
    # 	                  c[ind-1][1] * 255,
    # 	                  c[ind-1][2] * 255)
    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputDataObject(obj)
    mapper.SetScalarRange(min(signed_distance),max(signed_distance))
    mapper.SetLookupTable(lut)

    actor = vtk.vtkActor()
    actor.SetMapper(mapper)

    return actor

def load_vtk(filename,tx=0):
    reader = vtk.vtkPolyDataReader()
    reader.SetFileName(filename)

    transform = vtk.vtkTransform()
    transform.Identity()
    transform.Translate(tx, 0, 0)

    transformFilter = vtk.vtkTransformPolyDataFilter()
    transformFilter.SetInputConnection(reader.GetOutputPort())
    transformFilter.SetTransform(transform)
    transformFilter.Update()

    mapper = vtk.vtkPolyDataMapper()
    if vtk.VTK_MAJOR_VERSION <= 5:
        mapper.SetInput(reader.GetOutput())
    else:
        mapper.SetInputConnection(transformFilter.GetOutputPort())

    actor = vtk.vtkActor()
    actor.SetMapper(mapper)

    return actor

def create_pointcloud_polydata(points, colors=None):
    """https://github.com/lmb-freiburg/demon
    Creates a vtkPolyData object with the point cloud from numpy arrays

    points: numpy.ndarray
        pointcloud with shape (n,3)

    colors: numpy.ndarray
        uint8 array with colors for each point. shape is (n,3)

    Returns vtkPolyData object
    """
    vpoints = vtk.vtkPoints()
    vpoints.SetNumberOfPoints(points.shape[0])
    for i in range(points.shape[0]):
        vpoints.SetPoint(i, points[i])
    vpoly = vtk.vtkPolyData()
    vpoly.SetPoints(vpoints)

    if not colors is None:
        vcolors = vtk.vtkUnsignedCharArray()
        vcolors.SetNumberOfComponents(3)
        vcolors.SetName("Colors")
        vcolors.SetNumberOfTuples(points.shape[0])
        for i in range(points.shape[0]):
            vcolors.SetTuple3(i, colors[0], colors[1], colors[2])
        vpoly.GetPointData().SetScalars(vcolors)

    vcells = vtk.vtkCellArray()

    for i in range(points.shape[0]):
        vcells.InsertNextCell(1)
        vcells.InsertCellPoint(i)

    vpoly.SetVerts(vcells)

    return vpoly

segments = ['femur']  #'femur',
ligaments_fem = [[1,1,1,1,1,1,1,1,1,1],
            [6,5,6,6,6,6,4,4,5,5],
            [3,2,5,3,3,2,2,0,3,3],
            [7,8,7,7,7,5,7,6,7,0],
            [4,6,3,5,4,0,0,3,4,4],
            [5,7,4,4,5,7,6,5,6,6],
            [2,4,2,2,2,3,3,2,2,2],
            [0,3,8,0,0,0,0,0,0,0]]
ligaments_tib = [[5,7,6,5,3,4,4,5,5,4],
            [3,3,7,3,5,3,5,4,3,3],
            [1,1,1,1,1,1,1,1,1,1],
            [4,5,3,4,4,5,3,2,4,3],
            [6,8,9,6,6,6,6,6,6,5],
            [2,2,2,2,2,2,2,3,2,2],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0]]

for segment in segments:

    path = os.path.join(r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone/')
    mean_shape = 'mean_shape.stl'
    mode_plus = 'mode1_+2SD.stl'
    mode_min = 'mode1_-2SD.stl'
    plus2sd = trimesh.load_mesh(path + mode_plus)
    min2sd = trimesh.load_mesh(path + mode_min)
    signed_distance = trimesh.proximity.signed_distance(plus2sd, min2sd.vertices)

    colors = np.array(((241,163,64),
                       (247,247,247),
                       (153,142,195)))/255

    mode_plus = 'mode1_+2SD.vtk'
    mode_min = 'mode1_-2SD.vtk'

    bone_actor = load_stl(path + mean_shape,signed_distance)
    bone_actor.GetProperty().SetOpacity(1)

    # bone_actor.GetProperty().SetColor(colors[1])

    # plus_actor = load_vtk(path + mode_plus)
    # plus_actor.GetProperty().SetOpacity(1)
    # plus_actor.GetProperty().SetColor(colors[2])
    #
    # min_actor = load_vtk(path + mode_min)
    # min_actor.GetProperty().SetOpacity(0.8)
    # min_actor.GetProperty().SetColor(colors[0])

    # mapper = vtk.vtkPolyDataMapper()
    # mapper.SetInputData(point_cloud)
    # actor = vtk.vtkActor()
    # actor.SetMapper(mapper)
    # actor.GetProperty().SetColor(0,0,0)
    # actor.GetProperty().SetOpacity(1.0)


    # Renderer
    renderer = vtk.vtkRenderer()
    # renderer.AddActor(actor)
    renderer.AddActor(bone_actor)
    # renderer.AddActor(min_actor)
    # renderer.AddActor(plus_actor)

    # renderer.SetBackground(.2, .3, .4)
    renderer.SetBackground(1.0, 1.0, 1.0)
    renderer.ResetCamera()

    # Render Window
    renderWindow = vtk.vtkRenderWindow()
    renderWindow.AddRenderer(renderer)

    # Interactor
    renderWindowInteractor = vtk.vtkRenderWindowInteractor()
    renderWindowInteractor.SetRenderWindow(renderWindow)
    renderWindowInteractor.GetInteractorStyle().SetCurrentStyleToTrackballCamera()

    # Begin Interaction
    renderWindow.Render()
    renderWindow.SetWindowName("XYZ Data Viewer")
    renderWindowInteractor.Start()

