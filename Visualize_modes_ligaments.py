import os
import vtk
import trimesh
import numpy as np
from vtk.util.numpy_support import vtk_to_numpy, numpy_to_vtk
import seaborn as sns
from VisualiseSSM import create_pointcloud_polydata, load_stl
import math as m


def Rx(theta):
    return np.matrix([[1, 0, 0],
                      [0, m.cos(theta), -m.sin(theta)],
                      [0, m.sin(theta), m.cos(theta)]])


def Ry(theta):
    return np.matrix([[m.cos(theta), 0, m.sin(theta)],
                      [0, 1, 0],
                      [-m.sin(theta), 0, m.cos(theta)]])


def Rz(theta):
    return np.matrix([[m.cos(theta), -m.sin(theta), 0],
                      [m.sin(theta), m.cos(theta), 0],
                      [0, 0, 1]])


segment = 'femur'

rw = vtk.vtkRenderWindow()
# xmins = [0, .5, 0, .5, 0, .5]
# xmaxs = [0.5, 1, 0.5, 1, .5, 1]
# ymins = [.66, .66, .33, .33, 0, 0, ]
# ymaxs = [1, 1, .66, .66, 0.33, 0.33]

xmins = [0, 0, .33, .33, .66, .66]
xmaxs = [.33, .33, .66, .66, 1, 1]
ymins = [0, .5, 0, .5, 0, .5]
ymaxs = [0.5, 1, 0.5, 1, .5, 1]
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(rw)

renderer = vtk.vtkRenderer()

center_only = 0
lateral_only = 1
if center_only == 1:
    center_tibia = np.concatenate((np.arange(131),np.arange(470-341)+341))  # PCL + ACL
    center_femur = np.concatenate((np.arange(112),np.arange(341-263)+263))  # PCL + ACL
    # center_femur = np.concatenate((np.arange(64), np.arange(101 - 68) + 68))  # PCL + ACL
elif lateral_only == 1:
    center_femur = np.concatenate((np.arange(706-641)+641,np.arange(776-706)+706))  # np.concatenate((np.arange(370 - 341) + 341,np.arange(401-370)+370)) = 4096  # LCL+pop
    center_tibia = np.arange(242)  # LCL

tel=0

if segment == 'tibia':
    center = center_tibia
elif segment == 'femur':
    center = center_femur

for modes in range(1,4):

    if segment == 'fibula':
        d = -40
    else:
        d = -100

    path = os.path.join(r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone/')
    mean_shape = 'mean_shape.stl'
    mode_plus = 'mode' + str(modes) + '_+2SD_8192.stl'
    mode_min = 'mode' + str(modes) + '_-2SD_8192.stl'

    # ligament points
    points_lig = trimesh.load_mesh(path + '\SSM_' + segment + r'_pred_points_color_mode' + str(modes) + '_+2sd_8192.xyz')
    color = np.loadtxt(path + '\SSM_' + segment + r'_pred_points_color_mode' + str(modes) + '_+2sd_8192.xyz')[:, 3]
    if center_only == 1 or lateral_only == 1:
        points_lig = points_lig[center]
        color = color[center]
    R = Rx(90 * np.pi / 180)*Ry(180 * np.pi / 180) * Rz(0 * np.pi / 180)
    points_lig2 = []
    for point in points_lig:
        points_lig2.append(np.asarray(R * point[np.newaxis].T))
    points_lig2 = np.squeeze(np.asarray(points_lig2))
    # points_lig2 = points_lig2 + np.array((0, modes * d, 0))
    point_cloud_lig, rgb_col = create_pointcloud_polydata(points_lig2, color)

    points_lig_neg = trimesh.load_mesh(path + '\SSM_' + segment + r'_pred_points_color_mode' + str(modes) + '_-2sd_8192.xyz')
    color_neg = np.loadtxt(path + '\SSM_' + segment + r'_pred_points_color_mode' + str(modes) + '_-2sd_8192.xyz')[:, 3]
    if center_only == 1 or lateral_only == 1:
        points_lig_neg = points_lig_neg[center]
        color_neg = color_neg[center]
    R = Rx(90 * np.pi / 180) * Ry(180 * np.pi / 180) * Rz(0 * np.pi / 180)
    points_lig_neg2 = []
    for point in points_lig_neg:
        points_lig_neg2.append(np.asarray(R * point[np.newaxis].T))
    points_lig_neg2 = np.squeeze(np.asarray(points_lig_neg2))
    # points_lig_neg2 = points_lig_neg2 + np.array((d*-1, modes*d, 0))
    point_cloud_lig_neg, rgb_col_neg = create_pointcloud_polydata(points_lig_neg2, color_neg)

    bone_actor = load_stl(path + '/mean_shape.stl')
    bone_actor.GetProperty().SetOpacity(1.0)

    # load mesh via trimesh to get the correct order for distance transform
    reader = vtk.vtkSTLReader()
    reader.SetFileName(path + mode_plus)
    reader.Update()
    obj = reader.GetOutputDataObject(0)

    reader2 = vtk.vtkSTLReader()
    reader2.SetFileName(path + mode_min)
    reader2.Update()
    obj2 = reader2.GetOutputDataObject(0)

    # mapper
    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputDataObject(obj)
    mapper2 = vtk.vtkPolyDataMapper()
    mapper2.SetInputDataObject(obj2)

    # translation
    transform = vtk.vtkTransform()
    transform.Identity()
    # transform.Translate(0,modes * d, 0)
    transform.RotateX(90)
    transform.RotateY(180)
    transform.RotateZ(0)
    transformFilter = vtk.vtkTransformPolyDataFilter()
    transformFilter.SetInputConnection(reader.GetOutputPort())
    transformFilter.SetTransform(transform)
    transformFilter.Update()

    transform2 = vtk.vtkTransform()
    transform2.Identity()
    # transform2.Translate(d*-1, modes*d, 0)
    transform2.RotateX(90)
    transform2.RotateY(180)
    transform2.RotateZ(0)
    transformFilter2 = vtk.vtkTransformPolyDataFilter()
    transformFilter2.SetInputConnection(reader2.GetOutputPort())
    transformFilter2.SetTransform(transform2)
    transformFilter2.Update()

    # actors
    bone_actor = vtk.vtkActor()
    bone_actor.SetMapper(mapper)
    mapper.SetInputConnection(transformFilter.GetOutputPort())
    bone_actor.SetMapper(mapper)
    bone_actor.GetProperty().SetColor(0.89, 0.85, 0.79)
    bone_actor2 = vtk.vtkActor()
    mapper2.SetInputConnection(transformFilter2.GetOutputPort())
    bone_actor2.SetMapper(mapper2)
    bone_actor2.GetProperty().SetColor(0.89, 0.85, 0.79)

    mapper2lig = vtk.vtkPolyDataMapper()
    mapper2lig.SetInputData(point_cloud_lig)
    actor2lig = vtk.vtkActor()
    actor2lig.SetMapper(mapper2lig)
    actor2lig.GetProperty().SetColor(1, 0, 0)
    actor2lig.GetProperty().SetPointSize(7.5)

    mapper3 = vtk.vtkPolyDataMapper()
    mapper3.SetInputData(point_cloud_lig_neg)
    actor3 = vtk.vtkActor()
    actor3.SetMapper(mapper3)
    actor3.GetProperty().SetColor(1, 0, 0)
    actor3.GetProperty().SetPointSize(7.5)

    for ind in range(2):
        ren = vtk.vtkRenderer()
        rw.AddRenderer(ren)
        ren.SetViewport(xmins[tel], ymins[tel], xmaxs[tel], ymaxs[tel])

        # Share the camera between viewports.
        if tel == 0:
            camera = ren.GetActiveCamera()
        else:
            ren.SetActiveCamera(camera)

        # Create a mapper and actor
        if tel == 0 or tel == 2 or tel == 4:
            ren.AddActor(bone_actor)
            ren.AddActor(actor2lig)
        else:
            ren.AddActor(bone_actor2)
            ren.AddActor(actor3)

        ren.SetBackground(1.0, 1.0, 1.0)

        ren.ResetCamera()

        tel+=1

    # Renderer
    renderer.AddActor(bone_actor)
    renderer.AddActor(bone_actor2)
    renderer.AddActor(actor2lig)
    renderer.AddActor(actor3)
    # renderer.AddActor(legend)
    renderer.SetBackground(1.0, 1.0, 1.0)
    renderer.ResetCamera()

# Render Window
renderWindow = vtk.vtkRenderWindow()
renderWindow.AddRenderer(renderer)
renderWindow.SetSize(750, 750)

# Interactor
renderWindowInteractor = vtk.vtkRenderWindowInteractor()
renderWindowInteractor.SetRenderWindow(renderWindow)
renderWindowInteractor.GetInteractorStyle().SetCurrentStyleToTrackballCamera()

# Begin Interaction
renderWindow.Render()
renderWindow.SetWindowName("SSM distances")
renderWindowInteractor.Start()

rw.Render()
rw.SetWindowName('MultipleViewPorts')
rw.SetSize(1500, 650)
iren.GetInteractorStyle().SetCurrentStyleToTrackballCamera()
iren.Start()