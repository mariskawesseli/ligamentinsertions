import cv2
import numpy as np
import SimpleITK as sitk
import pydicom as dicom
import os
import glob

subjects = [9,13,19,23,26,29,32,35,37,41]
for subject in subjects:
	path_drr = r'C:/Users/mariskawesseli/Documents/LigamentStudy/ImageData/'+str(subject)+'/DRR/'
	images = ['med_fem0001.dcm','med_wires0001.dcm','lat_fem0001.dcm','lat_wires0001.dcm',
			  'med_fem0001.dcm','med_all_wires0001.dcm','lat_fem0001.dcm','lat_all_wires0001.dcm']
	lig_names = ['PCL', 'MCL-p','MCL-d','posterior oblique','ACL','LCL (prox)','popliteus (dist)']

	for file in glob.glob(os.path.join(path_drr,"*.dcm")):
		image = file
		ds = dicom.dcmread(image)
		pixel_array_numpy = ds.pixel_array
		image = image.replace('.dcm', '.jpg')
		cv2.imwrite(os.path.join(path_drr, image), pixel_array_numpy)


	for ind in range(0,4):
		if subject in [9, 13, 26, 29, 32]:
			side = 'R'
		else:
			side = 'L'

		mask2 = []
		src = cv2.imread(os.path.join(path_drr,images[0+2*ind].replace('.dcm', '.jpg')))
		mask = cv2.imread(os.path.join(path_drr, images[1+2*ind].replace('.dcm', '.jpg')))
		if 'med' in images[0+2*ind]:
			for ind2 in range(0,4):
				mask2.append(cv2.imread(os.path.join(path_drr, lig_names[ind2] + '0001.dcm'.replace('.dcm', '.jpg'))))
		else:
			for ind2 in range(4, 7):
				mask2.append(cv2.imread(os.path.join(path_drr, lig_names[ind2] + '0001.dcm'.replace('.dcm', '.jpg'))))

		# convert mask to gray and then threshold it to convert it to binary
		gray = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)
		gray2 = []
		for new_mask in mask2:
			gray2.append(cv2.cvtColor(new_mask, cv2.COLOR_BGR2GRAY))
		# ret, binary = cv2.threshold(gray, 40, 255, cv2.THRESH_BINARY)
		blur = cv2.GaussianBlur(gray, (3,3), 0)
		blur2 = []
		for new_gray in gray2:
			blur2.append(cv2.GaussianBlur(new_gray, (3, 3), 0))
		binary = cv2.threshold(blur, 250, 255, cv2.THRESH_BINARY_INV)[1]  # + cv2.THRESH_OTSU
		binary2 = []
		for new_blur in blur2:
			binary2.append(cv2.threshold(new_blur, 250, 255, cv2.THRESH_BINARY_INV)[1]) # + cv2.THRESH_OTSU

		# find contours of two major blobs present in the mask
		contours,hierarchy = cv2.findContours(binary, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
		contours2 = []
		for new_binary in binary2:
			contoursX, hierarchyX = cv2.findContours(new_binary, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
			contours2.append(contoursX)

		# draw the found contours on to source image
		for contour in contours:
			cv2.drawContours(src, contour, -1, (255,0,0), thickness = 1)
		colors = [(0,0,255),(0,255,0),(255,0,255),(0,255,255)]
		count = -1
		for new_contours in contours2:
			count += 1
			c = colors[count]
			for contour in new_contours:
				cv2.drawContours(src, contour, -1, c, thickness = 1)

		# split source to B,G,R channels
		b,g,r = cv2.split(src)
		b2, g2, r2 = cv2.split(src)

		# add a constant to R channel to highlight the selected area in red
		r = cv2.add(b, 30, dst = b, mask = binary, dtype = cv2.CV_8U)
		#r2 = []
		# for new_binary in binary2:
		# 	r2 = cv2.add(b, 30, dst=b, mask=new_binary, dtype=cv2.CV_8U)

		# merge the channels back together
		img_overlay = cv2.merge((b,g,r), src)
		# for new_r in r2:
		img_overlay = cv2.merge((b2, g2, r2), img_overlay)
		# cv2.imshow('overlay', img_overlay)
		if side == 'R':
			if (ind == 1) or (ind == 3):
				img_rot = cv2.rotate(img_overlay, cv2.ROTATE_180)
				img_rot = cv2.flip(img_rot, 1)
			else:
				img_rot = img_overlay
		else:
			if (ind == 0) or (ind == 2):
				img_rot = cv2.rotate(img_overlay, cv2.ROTATE_180)
				img_rot = cv2.flip(img_rot, 1)
			else:
				img_rot = img_overlay
		cv2.imwrite(os.path.join(path_drr, images[1+2*ind].replace('.dcm', '_combine.jpg')),img_rot)


