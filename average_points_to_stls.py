import trimesh
import numpy as np
import os

points_lig = trimesh.load_mesh(r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\femur_bone\new_bone\shape_models\meanshape_ligs_color_8192.xyz')
color = np.loadtxt(r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\femur_bone\new_bone\shape_models\meanshape_ligs_color_8192.xyz')[:, 3]

segment = 'femur'
subjects = ['100'] #, S0 [100]
lig = 'pop'
center_only = 1

if lig == 'LCL':
    center_femur = np.arange(706-641)+641  # np.arange(415-379)+379  # np.arange(370-341)+341 = 4096
    center_tibia = np.arange(242)
if lig == 'pop':
    center_femur = np.arange(776-706)+706  #np.arange(454-415)+415  # np.arange(401-370)+370 = 4096
    center_tibia = 0

if segment == 'tibia' or segment == 'fibula':
    center = center_tibia
elif segment == 'femur':
    center = center_femur

points10 = []
points9 = []
points8 = []
points7 = []
points6 = []
points5 = []
points4 = []
points3 = []
points2 = []
points1 = []

if center_only == 1:
    points_lig = points_lig[center]
    color = color[center]
print(color)
for ind in range(0,len(color)):
    T = trimesh.transformations.translation_matrix(points_lig[ind])
    point = trimesh.creation.cylinder(0.5, height=0.5, sections=None, segment=None, transform=T)
    # point = trimesh.creation.icosphere(subdivisions=3, radius=1.0, color=None, transform=T)

    if color[ind] == 10:
        if bool(points10):
            points10 = trimesh.boolean.union([points10, point])
        else:
            points10 = point
    elif color[ind] == 9:
        if bool(points9):
            points9 = trimesh.boolean.union([points9, point])
        else:
            points9 = point
    elif color[ind] == 8:
        if bool(points8):
            points8 = trimesh.boolean.union([points8, point])
        else:
            points8 = point
    elif color[ind] == 7:
        if bool(points7):
            points7 = trimesh.boolean.union([points7, point])
        else:
            points7 = point
    elif color[ind] == 6:
        if bool(points6):
            points6 = trimesh.boolean.union([points6, point])
        else:
            points6 = point
    elif color[ind] == 5:
        if bool(points5):
            points5 = trimesh.boolean.union([points5, point])
        else:
            points5 = point
    elif color[ind] == 4:
        if bool(points4):
            points4 = trimesh.boolean.union([points4, point])
        else:
            points4 = point
    elif color[ind] == 3:
        if bool(points3):
            points3 = trimesh.boolean.union([points3, point])
        else:
            points3 = point
    elif color[ind] == 2:
        if bool(points2):
            points2 = trimesh.boolean.union([points2, point])
        else:
            points2 = point
    elif color[ind] == 1:
        if bool(points1):
            points1 = trimesh.boolean.union([points1, point])
        else:
            points1 = point

points10.export(os.path.join(r"C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\femur_bone\new_bone\shape_models", lig+'points10.stl'))
points9.export(os.path.join(r"C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\femur_bone\new_bone\shape_models", lig+'points9.stl'))
points8.export(os.path.join(r"C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\femur_bone\new_bone\shape_models", lig+'points8.stl'))
points7.export(os.path.join(r"C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\femur_bone\new_bone\shape_models", lig+'points7.stl'))
points6.export(os.path.join(r"C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\femur_bone\new_bone\shape_models", lig+'points6.stl'))
points5.export(os.path.join(r"C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\femur_bone\new_bone\shape_models", lig+'points5.stl'))
points4.export(os.path.join(r"C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\femur_bone\new_bone\shape_models", lig+'points4.stl'))
points3.export(os.path.join(r"C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\femur_bone\new_bone\shape_models", lig+'points3.stl'))
points2.export(os.path.join(r"C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\femur_bone\new_bone\shape_models", lig+'points2.stl'))
points1.export(os.path.join(r"C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\femur_bone\new_bone\shape_models", lig+'points1.stl'))