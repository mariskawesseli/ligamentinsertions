import pymeshlab
import numpy as np
import trimesh

def cylinder_between(p1, p2, r):
	dx = p2[0] - p1[0]
	dy = p2[1] - p1[1]
	dz = p2[2] - p1[2]
	dist = np.sqrt(dx**2 + dy**2 + dz**2)+0.5

	phi = np.arctan2(dy, dx)
	theta = np.arccos(dz/dist)

	T = trimesh.transformations.translation_matrix([dx/2 + p1[0], dy/2 + p1[1], dz/2 + p1[2]])
	origin, xaxis, yaxis, zaxis = [0,0,0], [1, 0, 0], [0, 1, 0], [0, 0, 1]
	Rz = trimesh.transformations.rotation_matrix(phi, zaxis)
	Ry = trimesh.transformations.rotation_matrix(theta, yaxis)
	R = trimesh.transformations.concatenate_matrices(T,Rz, Ry)

	cylinder = trimesh.creation.cylinder(r, height=dist, sections=None, segment=None, transform=R)
	cylinder.export(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData\19\cylinder.stl")

path = r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData\19"
i=2

ms3= pymeshlab.MeshSet()
ms3.load_new_mesh(path + '\Segmentation_femur_wires' + str(i) + '.stl')
dist_matrix = []
dist_matrix_ind = []
start_ind = []
verts = ms3.mesh(0).vertex_matrix()
for ind in range(0,len(verts)):
	ms3.apply_filter('colorize_by_geodesic_distance_from_a_given_point', startpoint=verts[ind],maxdistance=100)
	dist_matrix.append(np.max(ms3.mesh(0).vertex_quality_array()))
	dist_matrix_ind.append(np.argmax(ms3.mesh(0).vertex_quality_array()))
	start_ind.append(ind)

max1 = np.argmax(dist_matrix)
end_point = verts[dist_matrix_ind[max1]]
start_point = verts[start_ind[max1]]
r = 0.5
cylinder_between(start_point, end_point, r)

path_cylinder = r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData\19\cylinder.stl"
ms3.load_new_mesh(path_cylinder)
ms3.apply_filter('mesh_boolean_union', first_mesh=0, second_mesh=1)
ms3.save_current_mesh(path + '\Segmentation_femur_wires' + str(i) + 'close.stl', binary=False)

# ms4 = pymeshlab.MeshSet()
# ms4.load_new_mesh(path + '\Segmentation_femur_wire' + str(i) + 'union.stl')
# ms4.load_new_mesh(path + '\Segmentation_femur_area' + str(i) + '.stl')
#
# # compute signed distance
# out3 = ms4.apply_filter('distance_from_reference_mesh', measuremesh=1, refmesh=0, signeddist=True)
#
# # select and delete vertices with negative distance
# ms4.conditional_vertex_selection(condselect="q<0")
# ms4.delete_selected_vertices()
# # split mesh
# out4 = ms4.apply_filter('split_in_connected_components')
#
# no_meshes = ms4.number_meshes()
# meshes_to_remove = no_meshes-4
# for ind in range(0,meshes_to_remove):
# 	no_vertices = ms4.mesh(ind+4).vertex_matrix().shape[0]
# 	ms4.set_current_mesh(ind+4)
# 	ms4.delete_current_mesh()
#
# no_vertices = ms4.mesh(3).vertex_matrix().shape[0]
# if no_vertices < 10:
# 	ms4.set_current_mesh(3)
# 	ms4.delete_current_mesh()
#
# ms4.set_current_mesh(2)
# ms4.apply_filter('select_border')
# ms4.mesh(2).selected_face_number()
# ms4.apply_filter('dilate_selection')
# ms4.mesh(2).selected_face_number()
# ms4.apply_filter('dilate_selection')
#
# geometric_measures = ms4.apply_filter('compute_geometric_measures')
# surface = geometric_measures['surface_area']
# print('Surface area femur ligament' + str(i) + ': ' + str(surface) + ' mm2')
# # ms4.save_project(path + '\Segmentation_femur_area' + str(i) + 'test.mlp')