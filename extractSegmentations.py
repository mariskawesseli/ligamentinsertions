# exec(open(r'C:\Users\mariskawesseli\Documents\GitLab\Other\LigamentStudy\extractSegmentations.py').read())

import os,glob
import shutil

dir = r"C:\Users\mariskawesseli\Documents\Data\OAI\segmentation\Fibula"

for name in os.listdir(dir):
	print(name)
	slicer.mrmlScene.Clear(0)
	path = os.path.join(dir, name)

	slicer.util.loadSegmentation(glob.glob(os.path.join(path, "Segmentation.seg.nrrd"))[0])
	slicer.util.loadVolume(glob.glob(os.path.join(path, "*RIGHT.nrrd"))[0])
	volumeNode = slicer.mrmlScene.GetFirstNodeByClass("vtkMRMLScalarVolumeNode")
	segmentationNode = getNode("Segmentation")

	# use islands to get noise out
	segmentEditorNode = slicer.mrmlScene.AddNewNodeByClass("vtkMRMLSegmentEditorNode")

	segmentEditorWidget = slicer.qMRMLSegmentEditorWidget()
	segmentEditorWidget.setMRMLScene(slicer.mrmlScene)
	segmentEditorWidget.setMRMLSegmentEditorNode(segmentEditorNode)
	segmentEditorWidget.setSegmentationNode(segmentationNode)
	segmentEditorWidget.setMasterVolumeNode(volumeNode)

	segmentEditorWidget.setActiveEffectByName("Islands")
	effect = segmentEditorWidget.activeEffect()
	effect.setParameter("Operation", 'KEEP_LARGEST_ISLAND')
	effect.self().onApply()

	# export segmentation to volume
	shNode = slicer.mrmlScene.GetSubjectHierarchyNode()

	exportFolderItemId = shNode.CreateFolderItem(shNode.GetSceneItemID(), "Segments")
	slicer.modules.segmentations.logic().ExportAllSegmentsToModels(segmentationNode, exportFolderItemId)

	outputFolder = r"C:\Users\mariskawesseli\Documents\Data\OAI\segmentation\2019_ATEZ_MEDIA-Supplementary-Material-OAI-ZIB\OAI-ZIB\segmentation\segmentation_meshes\fibula_bone\mesh"
	segmentationNode.SetName(str(name)+'_R')
	slicer.vtkSlicerSegmentationsModuleLogic.ExportSegmentsClosedSurfaceRepresentationToFiles(outputFolder,
	                                                                                          segmentationNode,
	                                                                                          None, "STL",
	                                                                                          True, 1.0, False)

	outputFolder_seg = r"C:\Users\mariskawesseli\Documents\Data\OAI\segmentation\2019_ATEZ_MEDIA-Supplementary-Material-OAI-ZIB\OAI-ZIB\segmentation\segmentation_meshes\fibula_bone\segmentation"
	slicer.util.saveNode(segmentationNode, outputFolder_seg + "/" + str(name) + "_R.nrrd")
	# shutil.copy(glob.glob(os.path.join(path, "Segmentation.seg.nrrd"))[0], outputFolder_seg + "/" + str(name) + "_R.nrrd")
