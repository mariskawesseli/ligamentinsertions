import numpy as np
import os
import trimesh
import pymeshlab
import glob

subjects = ['1']  #['S0']  # [9,13,19,23,26,29,32,35,37,41]
sides = ['R']
segments = ['femur','tibia', 'fibula']  #
short_ssm = [0, 1, 0]  #
data_folder = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/'
run_fit = 1

for subj_ind, subject in enumerate(subjects):

    if sides[subj_ind] == 'R':
        side = '_R'
        reflect = ''
    else:
        side = '_L'
        reflect = '.reflect'

    for seg_ind, segment in enumerate(segments):
        if short_ssm[seg_ind]:
            short = '_short'
        else:
            short = ''

        path = data_folder
        ssm_path = path + segment + '_bone' + short + r'\new_bone_mri\shape_models/'
        input_path = path + segment + '_bone' + short + r'\new_bone_mri\input/'
        ssm_files = glob.glob(ssm_path + "*.stl")
        input_files = glob.glob(input_path + "*.stl")

        # get ligament locations
        if segment == 'femur':
            no_pathpoint = 0
        else:
            no_pathpoint = 1
        if run_fit == 1:
            # run ICP to get final position SSM point cloud on original mesh
            # mesh OpenSim model -  make sure this is high quality
            mesh1 = trimesh.load_mesh(ssm_files[subj_ind])  # SSM mesh
            # mesh segmented from MRI
            mesh2 = trimesh.load_mesh(input_files[subj_ind])  # mesh in position MRI

            # Mirror if needed (only for left as SSM/model is right? - check how to deal with left model)
            if side == 'L':
                M = trimesh.transformations.scale_and_translate((-1, 1, 1))
            else:
                M = trimesh.transformations.scale_and_translate((1, 1, 1))
            # mesh2.apply_transform(M)
            # Rotate segmented bone (check why needed?)
            origin, xaxis, yaxis, zaxis = [0, 0, 0], [1, 0, 0], [0, 1, 0], [0, 0, 1]
            Rx = trimesh.transformations.rotation_matrix(-90 / (180 / np.pi), xaxis)
            Ry = trimesh.transformations.rotation_matrix(90 / (180 / np.pi), yaxis)
            # Rz = trimesh.transformations.rotation_matrix(180 / (180 / np.pi), zaxis)
            R = trimesh.transformations.concatenate_matrices(Ry, Rx)
            mesh1.apply_transform(M)
            # Translate segmented mesh to OpenSim bone location
            T = trimesh.transformations.translation_matrix(mesh2.center_mass - mesh1.center_mass)
            mesh1.apply_transform(T)

            new_path = ssm_path + '/fit/'
            if not os.path.exists(new_path):
                # If the path does not exist, create it
                os.makedirs(new_path)
            mesh1.export(new_path + os.path.split(ssm_files[subj_ind])[1])

            # ICP to fit segmented bone to OpenSim mesh
            kwargs = {"scale": False}
            # icp = trimesh.registration.icp(mesh1.vertices, mesh2, initial=np.identity(4), threshold=1e-5,
            #                                 max_iterations=20, **kwargs)

            icp = trimesh.registration.icp(mesh2.vertices, mesh1, initial=np.identity(4), threshold=1e-5, max_iterations=20,
                                           **kwargs)
            mesh1.apply_transform(icp[0])
            mesh1.export(new_path + 'icp_' + os.path.split(ssm_files[subj_ind])[1])

        # hausdorff distance
        ms5 = pymeshlab.MeshSet()
        ms5.load_new_mesh(new_path + 'icp_' + os.path.split(ssm_files[subj_ind])[1])
        ms5.load_new_mesh(input_files[subj_ind])
        out1 = ms5.apply_filter('hausdorff_distance', targetmesh=1, sampledmesh=0, savesample=True)
        out2 = ms5.apply_filter('hausdorff_distance', targetmesh=0, sampledmesh=1, savesample=True)

        print(segment + ' max: ' + str(max(out1['max'], out2['max'])))
        print(segment + ' min: ' + str(max(out1['min'], out2['min'])))
        print(segment + ' mean: ' + str(max(out1['mean'], out2['mean'])))
        print(segment + ' RMS: ' + str(max(out1['RMS'], out2['RMS'])))

        print(segment + ' max: ' + str(out1['max']))
        print(segment + ' min: ' + str(out1['min']))
        print(segment + ' mean: ' + str(out1['mean']))
        print(segment + ' RMS: ' + str(out1['RMS']))