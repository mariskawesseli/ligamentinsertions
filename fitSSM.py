import pymeshlab
import numpy as np
import trimesh
import nrrd
import re
import os
import pandas as pd
from tabulate import tabulate
from shutil import copyfile
import glob

subjects = [9,13,19,23,26,29,32,35,37,41]  #[9,13,19,23,26,29,32,35,37,41]
segments = ['tibia']  #'femur',
short = 1
run = 0

occurances=[]
all_occ = []
orders=[]

ligaments_fem = [[1,1,1,1,1,1,1,1,1,1],  # PCL
                [6,5,6,6,6,6,4,4,5,5],  # MCLp
                [3,2,5,3,3,2,2,0,3,3],  # MCLd
                [0,8,0,0,0,0,0,0,0,0],  # MCLd2
                [7,3,7,7,7,5,7,6,7,0],  # POL
                [0,0,8,0,0,0,0,0,0,0],  # POL2
                [0,0,0,0,0,0,0,0,0,0],  # POL3
                [0,0,0,0,0,0,0,0,0,0],  # POL4
                [4,6,3,5,4,0,0,3,4,4],  # ACL
                [5,7,4,4,5,7,6,5,6,6],  # LCL
                [2,4,2,2,2,3,3,2,2,2]]  # POP

ligaments_tib = [[5,7,6,5,3,4,4,5,5,4],  # PCL
                [1,1,1,1,1,1,1,1,1,1],  # MCLp
                [3,3,8,3,5,3,5,0,3,3],  # MCLd
                [0,4,0,0,0,0,0,0,0,0],  # MCLd2
                [4,5,3,4,4,5,3,2,4,0],  # POL
                [0,6,4,0,0,0,0,0,0,0],  # POL2
                [0,0,5,0,0,0,0,0,0,0],  # POL3
                [0,0,7,0,0,0,0,0,0,0],  # POL4
                [6,8,9,6,6,6,6,6,6,5],  # ACL
                [2,2,2,2,2,2,2,3,2,2],  # LCL
                [0,0,0,0,0,0,0,0,0,0]]  # POP

ligaments_fib = [[0,0,0,0,0,0,0,0,0,0],  # PCL
                [0,0,0,0,0,0,0,0,0,0],  # MCLp
                [0,0,0,0,0,0,0,0,0,0],  # MCLd
                [0,0,0,0,0,0,0,0,0,0],  # MCLd2
                [0,0,0,0,0,0,0,0,0,0],  # POL
                [0,0,0,0,0,0,0,0,0,0],  # POL2
                [0,0,0,0,0,0,0,0,0,0],  # POL3
                [0,0,0,0,0,0,0,0,0,0],  # POL4
                [0,0,0,0,0,0,0,0,0,0],  # ACL
                [2,2,2,2,2,2,2,3,2,2],  # LCL
                [0,0,0,0,0,0,0,0,0,0]]  # POP

for segment in segments:
    if segment == 'femur':
        ligaments = ligaments_fem
    elif segment == 'fibula':
        ligaments = ligaments_fib
    else:
        ligaments = ligaments_tib

    SSMpoints = [[] for i in range(11)]
    for ind in range(0,11):
        SSMpoints[ind] = [[] for i in range(10)]

    for ind, subject in enumerate(subjects):
        path = os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData", str(subject))
        if run == 1:
            if subject in [9,13,26,29,32]:
                side = 'R'
                reflect = ''
            else:
                side = 'L'
                reflect = '.reflect'
            if segment == 'fibula':
                points = str(2048)
            elif segment == 'femur':
                points = str(8192)  # 4096
            else:
                points = str(4096)
            """SSM part"""
            # files from SSM workflow shapeworks
            if short == 1:
                file_com = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone_short\new_bone\groomed\com_aligned\Segmentation_' + segment + '_' + side + '_short_' + str(
                    subject) + reflect + '.isores.pad.com.txt'
                file_align = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone_short\new_bone\groomed\aligned\Segmentation_' + segment + '_' + side + '_short_' + str(
                    subject) + reflect + '.isores.pad.com.center.aligned.txt'
                pad_file = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone_short\new_bone\groomed\padded\segementations\Segmentation_' + segment + '_' + side + '_short_' + str(
                    subject) + reflect + '.isores.pad.nrrd'
                com_file = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone_short\new_bone\groomed\com_aligned\Segmentation_' + segment + '_' + side + '_short_' + str(
                    subject) + reflect + '.isores.pad.com.nrrd'
                particle_file = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone_short\new_bone\shape_models/' + points + '\Segmentation_' + segment + '_' + side + '_short_' + str(
                    subject) + reflect + '.isores.pad.com.center.aligned.clipped.cropped.tpSmoothDT_local.particles'
                xyz_file = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone_short\new_bone\shape_models\Segmentation_' + segment + '_' + side + '_short_' + str(
                    subject) + reflect + '.isores.pad.com.center.aligned.clipped.cropped.tpSmoothDT_local.xyz'
                # align_file = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\femur_bone\new_bone\groomed\aligned\Segmentation_femur_' + side + '_short_' + str(subject) + reflect + '.isores.pad.com.center.aligned.nrrd'
                path_bones = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone_short\new_bone\input'
            else:
                file_resample = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone\new_bone\groomed\resampled\segmentations\Segmentation_' + segment + '_' + side + '_short_' +str(
                    subject) + reflect + '.isores.nrrd'
                file_crop = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone\new_bone\groomed\cropped\segmentations\Segmentation_' + segment + '_' + side + '_short_' +str(
                    subject) + reflect + '.isores.pad.com.center.aligned.clipped.cropped.nrrd'
                file_com = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone\new_bone\groomed\com_aligned\Segmentation_' + segment + '_' + side + '_short_' + str(subject) + reflect + '.isores.pad.com.txt'
                file_align = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone\new_bone\groomed\aligned\Segmentation_' + segment + '_' + side + '_short_' + str(subject) + reflect + '.isores.pad.com.center.aligned.txt'
                pad_file = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone\new_bone\groomed\padded\segementations\Segmentation_' + segment + '_short_' + side + '_' + str(subject) + reflect + '.isores.pad.nrrd'
                com_file = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone\new_bone\groomed\com_aligned\Segmentation_' + segment + '_' + side + '_short_' + str(subject) + reflect + '.isores.pad.com.nrrd'
                particle_file = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r"_bone\new_bone\shape_models/" + points + '\Segmentation_' + segment + '_' + side + '_short_' + str(subject) + reflect + '.isores.pad.com.center.aligned.clipped.cropped.tpSmoothDT_local.particles'
                xyz_file = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone\new_bone\shape_models\Segmentation_' + segment + '_' + side + '_short_' + str(subject) + reflect + '.isores.pad.com.center.aligned.clipped.cropped.tpSmoothDT_local.xyz'
                # align_file = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\femur_bone\new_bone\groomed\aligned\Segmentation_femur_' + side + str(subject) + reflect + '.isores.pad.com.center.aligned.nrrd'
                path_bones = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone\new_bone\input'

            # get change in position from nrrd header files
            # header = nrrd.read_header(pad_file)
            # pad_position = header['space origin']
            header = nrrd.read_header(com_file)
            com_position = header['space origin']
            header = nrrd.read_header(file_resample)
            resample_position = header['space origin']
            header = nrrd.read_header(file_crop)
            crop_position = header['space origin']

            # with open(file_com) as fobj:
            # 	for line in fobj:
            # 		line = line.replace('[',']')
            # 		line_data = re.split("]|,", line)
            #
            # NA = np.asarray(line_data[1:4])
            # trans_mat = NA.astype(float)

            # get translation from align from rotation matrix
            rot_ssm = np.loadtxt(file_align)

            # rot_mat_ssm = np.transpose(rot_ssm)
            # rot_mat_ssm = np.vstack((rot_mat_ssm, [0,0,0,1]))

            # translate points cloud SSM instance to align with original mesh
            # com_position[0] = com_position[0]*-1

            diff = resample_position -rot_ssm[3,:] -crop_position
            translate = diff
            translate[0] = diff[1]
            translate[1] = diff[0]
            translate[2] = diff[2]
            if subject == 32:
                translate[0] = translate[0]+40.5
                translate[1] = translate[1]-8
            # translate[1] = translate[1] - 2*rot_ssm[3, 1]
            # if reflect == '.reflect':
            # 	translate[0] = 0# -46 #-resample_position[0] -rot_ssm[3,0] +com_position[0]

            # r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\femur_bone\new_bone\Segmentation_femur_R_short_ssm_reconstruct3.stl' #
            pre, ext = os.path.splitext(xyz_file)
            copyfile(particle_file, pre + '.paticles')
            if not os.path.isfile(pre + '.xyz'):
                os.rename(pre + '.paticles', pre + '.xyz')
            mesh3 = xyz_file  # =local.particle file
            ms6 = pymeshlab.MeshSet()
            ms6.load_new_mesh(mesh3)
            max_val = 200
            ms6.apply_filter('transform_translate_center_set_origin', traslmethod=0, axisx=translate[0], axisy=translate[1], axisz=-max_val)

            iters = 1
            while translate[2]+max_val*iters < -max_val:
                ms6.apply_filter('transform_translate_center_set_origin', traslmethod =0, axisx=0, axisy=0, axisz=-max_val)
                iters=iters+1
            # ms6.apply_filter('transform_translate_center_set_origin', traslmethod=0, axisx=0, axisy=0, axisz=-222)
            ms6.apply_filter('transform_translate_center_set_origin', traslmethod =0, axisx=0, axisy=0, axisz=translate[2]+max_val*iters)
            ms6.save_current_mesh(path + '\SSM_' + segment + short_name + '_transform.xyz')

            # run ICP to get final position SSM point cloud on original mesh
            mesh = trimesh.load_mesh(path + '\Segmentation_' + segment + short_name + '_resample.stl')
            # mesh = trimesh.load_mesh(r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone\new_bone\input\Segmentation_' + segment + '_' + side + '_short_' + str(subject) + '_remesh.stl')
            # mesh = trimesh.load_mesh(path_bones + '\Segmentation_' + segment + '_' + side + '_short_' + str(subject) + '.STL')
            points = trimesh.load_mesh(path + '\SSM_' + segment + short_name + '_transform.xyz')
            if reflect == '.reflect':
                M = trimesh.transformations.scale_and_translate((-1,1,1))
                points.apply_transform(M)
            # np.savetxt(path + '\SSM_' + segment + '_short_transform_mirror.xyz', points.vertices, delimiter=" ")
            kwargs = {"scale": False}
            top_points = np.asarray(points.vertices)
            exclude_bottom = top_points[top_points[:, 2] > min(top_points[:, 2])+1]
            # icp = trimesh.registration.mesh_other(mesh, exclude_bottom, samples=2000, scale=False, icp_first=10, icp_final=50)
            # points.apply_transform(np.linalg.inv(icp[0]))
            icp = trimesh.registration.icp(exclude_bottom,mesh,initial=np.identity(4),threshold=1e-5,max_iterations=20,**kwargs)
            points.apply_transform(icp[0])

            # icp = trimesh.registration.icp(points.vertices, mesh, initial=np.identity(4), threshold=1e-5, max_iterations=20,**kwargs)
            # points.apply_transform(icp[0])

            np.savetxt(path + '\SSM_' + segment + short_name + '_transform_icp.xyz', points.vertices, delimiter=" ")

            # ms5 = pymeshlab.MeshSet()
            # ms5.load_new_mesh(path + '\SSM_' + segment + '_transform_icp.xyz')

        if short == 1:
            short_name = '_short'
        else:
            short_name = ''
        if run == 2:
            points = trimesh.load_mesh(path + '\SSM_' + segment + short_name + '_transform_icp.xyz') #_short
            if segment == 'fibula':
                segment_temp = 'tibia'
                Counter = len(glob.glob1(path, 'Segmentation_' + segment_temp + '_area' + str(ligaments_fib[9][ind]) + '*.stl'))
            else:
                segment_temp = segment
                Counter = len(glob.glob1(path, 'Segmentation_' + segment_temp + '_area*.stl'))
            close_verts = []
            close_verts_verts = np.empty([0,3])
            for count in range(1, int(np.ceil(Counter/2)) + 1):
                if segment == 'fibula':
                    count_n = count + ligaments_fib[9][ind] - 1
                else:
                    count_n = count
                mesh = trimesh.load_mesh(os.path.join(path,'Segmentation_' + segment_temp + '_area' + str(count_n) + '.stl'))
                # [closest, distance, id] = trimesh.proximity.closest_point(mesh, points.vertices)
                distance = trimesh.proximity.signed_distance(mesh, points.vertices)
                if segment == 'fibula':
                    max_dist = 1.5
                elif segment == 'tibia':
                    max_dist = 1
                else:
                    max_dist = 1
                close_verts.append(np.where(abs(distance)<max_dist))
                # close_verts = np.vstack([close_verts, np.where(abs(distance)<2)])
                # close_verts_verts.append(points.vertices[np.where(abs(distance)<2)])
                close_verts_verts = np.vstack([close_verts_verts, points.vertices[np.where(abs(distance)<max_dist)]])
            # np.savetxt(path + '\SSM_' + segment + '_areas_test.xyz', np.asarray(close_verts_verts), delimiter=" ")

            if segment == 'fibula':
                for lig in range(0, 11):
                    lig_no = ligaments[lig][ind]
                    if not lig_no == 0:
                        SSMpoints[lig][ind] = close_verts[0][0]
            else:
                for lig in range(0, 11):
                    lig_no = ligaments[lig][ind]
                    if not lig_no == 0:
                        SSMpoints[lig][ind] = close_verts[lig_no-1][0]

    # dupes = [x for n, x in enumerate(np.concatenate(SSMpoints[0])) if x in np.concatenate(SSMpoints[0])[:n]]
    from collections import Counter
    if run == 2:
        occurances = []
        all_occ = []
        orders = []
        for ind in range(0,11):
            count=0
            occur = []
            if ind == 2:
                occur = Counter(np.concatenate(SSMpoints[ind]+SSMpoints[ind+1], axis=0))
            elif ind == 4:
                occur = Counter(np.concatenate(SSMpoints[ind]+ SSMpoints[ind + 1]+ SSMpoints[ind + 2]+ SSMpoints[ind + 3], axis=0))
            elif ind == 3 or ind == 5 or ind == 6 or ind == 7:
                continue
            else:
                occur = Counter(np.concatenate(SSMpoints[ind], axis=0))
            order = occur.most_common()
            for j in range(0,10):
                if len(SSMpoints[ind][j]):
                    count=count+1
            if ind == 4:
                print(count)
            try:
                index = [x for x, y in enumerate(order) if y[1] == int(count/2)]
                most_occ = order[0:index[-1]]
            except:
                most_occ = order[0:1]
            all_occ.append(np.asarray([i[0] for i in order]))
            occurances.append(np.asarray([i[0] for i in most_occ]))
            orders.append(np.asarray([i[1] for i in order]))

    elif run == 0:
        bla = np.load(r'C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData\occurances_' + segment + short_name + '.npz')
        occurances.append(bla['PCL']); occurances.append(bla['MCLp']); occurances.append(bla['MCLd']); occurances.append(bla['post_obl']); occurances.append(bla['ACL']);\
            occurances.append(bla['LCL']); occurances.append(bla['pop'])
        bla = np.load(r'C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData\all_occurances_' + segment + short_name + '.npz')
        all_occ.append(bla['PCL']); all_occ.append(bla['MCLp']); all_occ.append(bla['MCLd']); all_occ.append(bla['post_obl']); all_occ.append(bla['ACL']);
        all_occ.append(bla['LCL']); all_occ.append(bla['pop'])
        bla = np.load(r'C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData\all_occurances_orders_' + segment + short_name + '.npz')
        orders.append(bla['PCL']); orders.append(bla['MCLp']); orders.append(bla['MCLd']); orders.append(bla['post_obl']); orders.append(bla['ACL']);
        orders.append(bla['LCL']); orders.append(bla['pop'])

    points = trimesh.load_mesh(r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone' + short_name + r'\new_bone\shape_models\mean_shape.xyz')
    np.savetxt(r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone' + short_name + r'\new_bone\shape_models\meanshape_ligs.xyz', points.vertices[np.hstack(occurances).astype(int)], delimiter=" ")

    pred_lig_points_color = np.c_[points.vertices[np.hstack(all_occ).astype(int)], np.hstack(orders).astype(int)]
    np.savetxt(
        r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone' + short_name + r'\new_bone\shape_models\meanshape_ligs_color.xyz',
        pred_lig_points_color, delimiter=" ")

    mask = np.ones(len(points.vertices), dtype=bool)
    mask[np.hstack(occurances).astype(int)] = False
    # np.savetxt(r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone\new_bone\shape_models\meanshape_bone_no_lig.xyz',
    #            points.vertices[np.where(mask)], delimiter=" ")
    np.savez(r'C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData\occurances' + segment + short_name + '_order.npy', PCL=orders[0],MCLp=orders[1],
             MCLd=orders[2],post_obl=orders[3],ACL=orders[4],LCL=orders[5],pop=orders[6])


    # print('Surface area femur ligament' + str(lig_no) + ': ' + str(surface) + ' mm2')
            # ms5.load_new_mesh(os.path.join(path,'Segmentation_femur_area' + str(count) + '.stl'))
            # no_meshes = ms5.number_meshes()
            # ms5.apply_filter('distance_from_reference_mesh', measuremesh=0, refmesh=no_meshes-1, signeddist=False)
            # ms5.set_current_mesh(0)
            # ms5.conditional_vertex_selection(condselect="q<1")
            # m = ms5.current_mesh()
    subjects = [9,13,19,23,26,29,32,35,37,41]
    for ind, subject in enumerate(subjects):
        path = os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData", str(subject))
        if subject in [9,13,26,29,32]:
            side = 'R'
            reflect = ''
        else:
            side = 'L'
            reflect = '.reflect'

        points = trimesh.load_mesh(path + '\SSM_' + segment + short_name +'_transform_icp.xyz') #_short
        pred_lig_points = points.vertices[np.hstack(occurances).astype(int)]
        np.savetxt(path + '\SSM_' + segment + short_name + '_pred_points.xyz', np.asarray(pred_lig_points), delimiter=" ")
        pred_lig_points_color = np.c_[points.vertices[np.hstack(all_occ).astype(int)],np.hstack(orders).astype(int)]
        np.savetxt(path + '\SSM_' + segment + short_name + '_pred_points_color.xyz', np.asarray(pred_lig_points_color), delimiter=" ")

    # for modes in range(1,4):
    #     points = trimesh.load_mesh(r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + '_bone\mode' + str(modes) + '_+2sd.xyz')
    #     pred_lig_points = points.vertices[np.hstack(occurances).astype(int)]
    #     np.savetxt(r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + '_bone\SSM_' + segment + '_pred_points_mode' + str(modes) + '_+2sd.xyz', np.asarray(pred_lig_points), delimiter=" ")
    #     pred_lig_points_color = np.c_[points.vertices[np.hstack(all_occ).astype(int)], np.hstack(orders).astype(int)]
    #     np.savetxt(r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + '_bone\SSM_' + segment + '_pred_points_color_mode' + str(modes) + '_+2sd.xyz',
    #                np.asarray(pred_lig_points_color), delimiter=" ")
    #
    #     points = trimesh.load_mesh(r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + '_bone\mode' + str(modes) + '_-2sd2.xyz')
    #     pred_lig_points = points.vertices[np.hstack(occurances).astype(int)]
    #     np.savetxt(r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + '_bone\SSM_' + segment + '_pred_points_mode' + str(modes) + '_-2sd.xyz',
    #                np.asarray(pred_lig_points), delimiter=" ")
    #     pred_lig_points_color = np.c_[points.vertices[np.hstack(all_occ).astype(int)], np.hstack(orders).astype(int)]
    #     np.savetxt(r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + '_bone\SSM_' + segment + '_pred_points_color_mode' + str(modes) + '_-2sd.xyz',
    #                np.asarray(pred_lig_points_color), delimiter=" ")




    np.savez(r'C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData\occurances_' + segment + short_name ,PCL=occurances[0],MCLp=occurances[1],
             MCLd=occurances[2],post_obl=occurances[3],ACL=occurances[4],LCL=occurances[5],pop=occurances[6])
    np.savez(r'C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData\all_occurances_' + segment + short_name, PCL=all_occ[0],MCLp=all_occ[1],
             MCLd=all_occ[2],post_obl=all_occ[3],ACL=all_occ[4],LCL=all_occ[5],pop=all_occ[6])
    np.savez(r'C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData\all_occurances_orders_' + segment + short_name, PCL=orders[0],
             MCLp=orders[1], MCLd=orders[2], post_obl=orders[3], ACL=orders[4], LCL=orders[5], pop=orders[6])
