# import pymeshlab
import numpy as np
import trimesh
import nrrd
import re
import os
import pandas as pd
from tabulate import tabulate
from shutil import copyfile
import glob


def csv2xyz(csv_path):
    import csv

    # Define the paths to the input and output files
    xyz_path = os.path.splitext(csv_path)[0] + ".xyz"

    # Load the CSV file and extract the relevant data
    data = []
    with open(csv_path, "r") as csvfile:
        reader = np.genfromtxt(csvfile)  # csv.reader(csvfile)
        data = reader
        # for i, row in enumerate(reader):
        #     if i == 0:
        #         continue  # Skip the first row
        #     x, y, *z = row[1:-1]  # Extract x, y, and any additional columns as z
        #     data.append([x, y, *z])

    # Write the data to the output file in XYZ format
    with open(xyz_path, "w") as xyzfile:
        for row in data:
            x, y, *z = row
            xyzfile.write(f"{x} {y} {z[0]}\n")

    return xyz_path


subjects = ['1L','2L','3L','4L','5L','6L','8L','9L','1R','2R','3R','4R','5R','6R','8R','9R']  # ['1']  # ['S0']  # [9,13,19,23,26,29,32,35,37,41]
sides = ['L','L','L','L','L','L','L','L', 'R','R','R','R','R','R','R','R']
segments = ['tibia']  # ['femur','tibia', 'fibula']  #['tibia']  #
short_ssm = [1]  # [0,1,0]  #[1]  #
no_particles = [4096]  # [4096,4096,2048]  #[4096]  #
data_folder = r"C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/"

for seg_ind, segment in enumerate(segments):
    if short_ssm[seg_ind]:
        short = '_short'
    else:
        short = ''

    # Load which SSM points are related to which ligaments
    occ = np.load(
        r'C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData\occurances_' + segment + short + '.npz')
    occurances = [occ['PCL'], occ['MCLp'], occ['MCLd'], occ['post_obl'], occ['ACL'], occ['LCL'], occ['pop']]

    all_occ = np.load(
        r'C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData\all_occurances_' + segment + short + '.npz')
    all_occ = [all_occ['PCL'], all_occ['MCLp'], all_occ['MCLd'], all_occ['post_obl'], all_occ['ACL'], all_occ['LCL'], all_occ['pop']]

    order = np.load(
        r'C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData\all_occurances_orders_' + segment + short + '.npz')
    orders = [order['PCL'], order['MCLp'], order['MCLd'], order['post_obl'], order['ACL'], order['LCL'],
               order['pop']]

    for subj_ind, subject in enumerate(subjects):
        path = data_folder
        if sides[subj_ind] == 'R':
            side = '_R'
            reflect = ''
        else:
            side = '_L'
            reflect = '.reflect'

        # files from SSM workflow shapeworks
        ssm_path = path + segment + '_bone' + short + r'\new_bone_4DCT/'
        ssm_files = glob.glob(ssm_path + "*.particles")
        # ssm_files = glob.glob(ssm_path + "*.csv")
        particle_file_name = ssm_files[subj_ind]
        shape_model_folder = ssm_path

        new_path = ssm_path + '/fit/'
        if not os.path.exists(new_path):
            # If the path does not exist, create it
            os.makedirs(new_path)

        # path_bones = path + segment + '_bone' + short + r'\new_bone_mri\shape_models/'
        input_files = glob.glob(ssm_path + "*.stl")
        mesh_inp = input_files[subj_ind]

        # Create xyz file from particles file
        xyz_file = csv2xyz(particle_file_name)

        # pre, ext = os.path.splitext(particle_file_name)
        # particle_file = os.path.join(shape_model_folder, str(no_particles[seg_ind]), particle_file_name)
        # xyz_file = os.path.join(shape_model_folder, pre + '.xyz')
        # copyfile(particle_file, xyz_file)

        # Reflect (mirror) points if needed and translate to the position of the original mesh
        mesh_inp = trimesh.load_mesh(mesh_inp)
        points_xyz = trimesh.load_mesh(xyz_file)
        if reflect == '.reflect':
            M = trimesh.transformations.scale_and_translate((-1, 1, 1))
            points_xyz.apply_transform(M)
            mesh_inp.apply_transform(M)
        translate = mesh_inp.center_mass-points_xyz.centroid
        points_xyz.apply_transform(trimesh.transformations.translation_matrix(translate))
        np.savetxt(new_path + '\SSM_' + segment + str(subject) + '_transform.xyz', points_xyz.vertices, delimiter=" ")  # save intermediate translation to check

        # run ICP to get final position SSM point cloud on original mesh
        kwargs = {"scale": False}
        icp = trimesh.registration.icp(points_xyz.vertices,mesh_inp,initial=np.identity(4),threshold=1e-5,max_iterations=40,**kwargs)
        # icp = trimesh.registration.icp(points_xyz.vertices, mesh_inp, initial=icp[0], threshold=1e-5, max_iterations=20,**kwargs)  # run icp twice to improve fit
        points_xyz.apply_transform(icp[0])
        np.savetxt(new_path + r'\SSM_' + segment + str(subject) + '_transform_icp.xyz', points_xyz.vertices, delimiter=" ")  # save position SSM points on original mesh

        mesh_inp.apply_transform(trimesh.transformations.translation_matrix(translate))
        mesh_inp.apply_transform(icp[0])
        mesh_inp.export(new_path + r'\SSM_' + segment + short + str(subject) + '.stl')

        # link which SSM points are related to which ligaments to new point cloud
        pred_lig_points = points_xyz.vertices[np.hstack(occurances).astype(int)]
        np.savetxt(new_path + r'\SSM_' + segment + short + str(subject) + '_pred_points.xyz', np.asarray(pred_lig_points), delimiter=" ")
        pred_lig_points_color = np.c_[points_xyz.vertices[np.hstack(all_occ).astype(int)],np.hstack(orders).astype(int)]
        np.savetxt(new_path + '\SSM_' + segment + str(subject) + '_pred_points_color.xyz', np.asarray(pred_lig_points_color), delimiter=" ")

        print('processing ' + segment + ' done for ' + str(subject))
