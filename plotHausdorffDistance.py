import numpy as np
import trimesh
import os
from openpyxl import load_workbook
import pandas as pd


subjects = [9,13,19,23,26,29,32,35,37,41]
segments = ['femur','tibia','fibula']

data = []
for segment in segments:
    RMS = []
    for subject in subjects:
        path = os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData", str(subject))
        if segment=='femur':
            HD = np.load(path + r'/8192/' + segment + '_HD.np.npy', allow_pickle=True)
        else:
            HD = np.load(path + r'/' + segment + '_HD.np.npy',allow_pickle=True)
        RMS.append(HD[0]['RMS'])
        # RMS.append(max(HD[0]['RMS'], HD[1]['RMS']))

    data.append(np.mean(RMS).round(decimals=2).astype(str) + ' ±' + np.std(RMS).round(decimals=2).astype(str))

df = pd.DataFrame(data, index=['femur','tibia','fibula'], columns=['Hausdorff distance RMS (mm)'])

book = load_workbook(os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData","HausdorffDistance.xlsx"))
writer = pd.ExcelWriter(os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData","HausdorffDistance.xlsx"), engine='openpyxl')
writer.book = book
df.to_excel(writer, sheet_name='HD')
writer.save()
writer.close()
