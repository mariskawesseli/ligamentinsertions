import trimesh
import pymeshlab
import os
import numpy as np

# mesh = trimesh.load_mesh(r'C:\Users\mariskawesseli\Documents\Data\OAI\segmentation\2019_ATEZ_MEDIA-Supplementary-Material-OAI-ZIB\OAI-ZIB\segmentation\segmentation_meshes\femur_cartilage\mesh\9005075.segmentation_masks_femoral_cartilage_R.ply')
# trimesh.remesh.subdivide_to_size(mesh.vertices, mesh.faces, 5, max_iter=10, return_index=False)
# trimesh.exchange.export.export_mesh(mesh,r'C:\Users\mariskawesseli\Documents\Data\OAI\segmentation\2019_ATEZ_MEDIA-Supplementary-Material-OAI-ZIB\OAI-ZIB\segmentation\segmentation_meshes\femur_cartilage\mesh_resample\9005075.segmentation_masks_femoral_cartilage_R.ply', 'ply')

inputDir = r'C:\Users\mariskawesseli\Documents\Data\OAI\segmentation\2019_ATEZ_MEDIA-Supplementary-Material-OAI-ZIB\OAI-ZIB\segmentation'
datasetName = "tibia_cartilage"
mesh_dir = inputDir + r'/segmentation_meshes/' + datasetName + '/mesh/med/'
mesh_dir_out = inputDir + r'/segmentation_meshes/' + datasetName + '/mesh_resample/med/'

files_mesh = []
for file in sorted(os.listdir(mesh_dir)):
	files_mesh.append(mesh_dir + file)

pt_to_use = r'C:\Users\mariskawesseli\Documents\Data\OAI\segmentation\2019_ATEZ_MEDIA-Supplementary-Material-OAI-ZIB\healthyKL_pts.txt'
with open(pt_to_use) as f:
    pts = f.readlines()
pts = [i.split('\n')[0] for i in pts]
pts_use = pts

matches_mesh = []
for pt in pts_use:
	if any(pt in s for s in files_mesh):
		matches_mesh.append([match for match in files_mesh if pt in match])

files_mesh = [item for sublist in matches_mesh for item in sublist]

#
# for file in files_mesh:
# 	ms6 = pymeshlab.MeshSet()
# 	ms6.load_new_mesh(file)
# 	ms6.apply_filter('uniform_mesh_resampling', cellsize=1.05, offset=0.5, mergeclosevert=False)
# 	ms6.save_current_mesh(file_name=mesh_dir_out + file.split('/')[-1], save_textures=False)

	# for femur
	# m = ms6.current_mesh()
	# fm = m.face_matrix()
	#
	# # ms6 = pymeshlab.MeshSet()
	# # ms6.load_new_mesh(mesh_dir_out + file.split('/')[-1])
	# ms6.apply_filter('simplification_quadric_edge_collapse_decimation',targetfacenum=np.max(fm),targetperc=0,qualitythr=0.3,preserveboundary=True,preservenormal=True,preservetopology=True)
	# ms6.save_current_mesh(file_name=mesh_dir_out + file.split('/')[-1], save_textures=False)

datasetName = "tibia_cartilage"
side = 'med'  # 'lat' #
outputDirectory = r'C:/Users/mariskawesseli/Documents/GitLab/knee_ssm/OAI/Output/tibia_cartilage_' + side + '/groomed/'

origin, xaxis, yaxis, zaxis = [0, 0, 0], [1, 0, 0], [0, 1, 0], [0, 0, 1]
Rx = trimesh.transformations.rotation_matrix(np.pi/4, xaxis)

mesh_reg = trimesh.load_mesh(outputDirectory + 'reference.ply')
for file in files_mesh:
	mesh1 = trimesh.load_mesh(mesh_dir_out + file.split('/')[-1])

	T, cost = trimesh.registration.mesh_other(mesh1, mesh_reg, samples=500, scale=False, icp_first=10, icp_final=50)
	mesh1.apply_transform(T)
	mesh1.apply_transform(Rx)

	mesh1.export(outputDirectory + 'meshes/' + file.split('/')[-1])


