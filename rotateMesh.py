import trimesh
import os
import numpy as np
import glob


subjects = ['1']  #['S0']  # [9,13,19,23,26,29,32,35,37,41]
sides = ['R']
segments = ['femur','tibia', 'fibula']
short_ssm = [0, 1, 0]
no_particles = [4096, 4096, 2048]
opensim_meshes = ['smith2019-L-femur-bone_remesh.stl','smith2019-L-tibia-bone_remesh.stl',
                  'smith2019-L-fibula-bone_remesh.stl']
run_fit = 1
run_find_points = 1
create_cmd = 0
add_contact = 1
for_linux = 0

opensim_geometry_folder = r'C:\opensim-jam\jam-resources\jam-resources-main\models\knee_healthy\smith2019\Geometry'
input_file_folder = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output'
gen_model = r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\4DCT\1\lenhart2015_nocontact.osim'  # generic scaled model without contact

data_folder = r"C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\4DCT/"
path = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/'
cadaver_folder = r'C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData/'

for sample in range(0,1):  #100

    for subj_ind, subject in enumerate(subjects):

        if sides[subj_ind] == 'R':
            side = '_R'
            reflect = ''
        else:
            side = '_L'
            reflect = '.reflect'

        for seg_ind, segment in enumerate(segments):
            if short_ssm[seg_ind]:
                short = '_short'
            else:
                short = ''

            ssm_path = path + segment + '_bone' + short + r'\new_bone_mri\shape_models/'
            ssm_files = glob.glob(ssm_path + "*.stl")
            mesh_inp = ssm_files[subj_ind]
            new_path = ssm_path + '/fit/'

            out_file = os.path.join(data_folder, str(subject), 'input_mesh_' + segment + short + '_translated.stl')
            out_file2 = os.path.join(data_folder, str(subject), 'input_mesh_' + segment + short + '_icp.stl')

            origin, xaxis, yaxis, zaxis = [0,0,0], [1, 0, 0], [0, 1, 0], [0, 0, 1]
            Rz = trimesh.transformations.rotation_matrix(0 / (180 / np.pi), xaxis)
            Ry = trimesh.transformations.rotation_matrix(-90 / (180 / np.pi), zaxis)
            R = trimesh.transformations.concatenate_matrices(Rz, Ry)

            mesh = trimesh.load_mesh(mesh_inp)
            mesh.apply_transform(R)
            mesh.export(os.path.join(args.inputDir,mesh_path))
