import trimesh
import numpy as np
from scipy import interpolate
import matplotlib.pyplot as plt


def interpolate_lig_points(lig_points, no_points, plot=1):
    x = lig_points[:, 0]
    y = lig_points[:, 1]
    z = lig_points[:, 2]
    goon = 1
    n=0
    # Create a uniformly spaced grid
    while goon == 1:
        steps = no_points/2+n  # number of rows and columns for the grid
        grid_steps = complex(str(steps) + 'j')

        interp = interpolate.Rbf(x, y, z, function='thin_plate')
        yi, xi = np.mgrid[min(lig_points[:, 1]):max(lig_points[:, 1]):grid_steps,
                 min(lig_points[:, 0]):max(lig_points[:, 0]):grid_steps]
        zi = interp(xi, yi)
        inds_remove = []
        inds_nan = []
        diff_val = []
        xi_nan = xi
        yi_nan = yi
        zi_nan = zi
        for i in range(0, len(xi)):
            for j in range(0, len(xi)):
                diff = np.linalg.norm(lig_points[:, :] - np.asarray([xi[i, j], yi[i, j], zi[i, j]]), axis=1)
                diff_val.append(np.abs(np.amin(diff)))
                if np.amin(diff) > 1.5:
                    inds_remove.append([i * steps + j])
                    inds_nan.append([i, j])
                    xi_nan[i, j] = np.nan
                    yi_nan[i, j] = np.nan
                    zi_nan[i, j] = np.nan
        n=n+1
        if np.count_nonzero(~np.isnan(xi_nan)) >= no_points or n==10:
            # print(str(np.count_nonzero(~np.isnan(xi_nan))) + ' ' + str(no_points))
            goon = 0
        diff_val = np.zeros([len(xi_nan),len(xi_nan)])
        if np.count_nonzero(~np.isnan(xi_nan)) > no_points:
            to_remove = np.count_nonzero(~np.isnan(xi_nan))-no_points
            for i in range(0, len(xi_nan)):
                for j in range(0, len(xi_nan)):
                    diff = np.linalg.norm(lig_points[:, :] - np.asarray([xi_nan[i, j], yi_nan[i, j], zi_nan[i, j]]), axis=1)
                    diff_val[i,j] = np.abs(np.amin(diff))
            for k in range(0,to_remove):
                i,j = np.unravel_index(np.nanargmax(diff_val),diff_val.shape)
                diff_val[i,j] = np.nan
                xi_nan[i,j] = np.nan
                yi_nan[i,j] = np.nan
                zi_nan[i,j] = np.nan
            # print('-1 ' + str(np.count_nonzero(~np.isnan(xi_nan))) + ' ' + str(no_points))

    if len(lig_points) == 2:
        tck, u = interpolate.splprep([lig_points[:, 0], lig_points[:, 1], lig_points[:, 2]],s=10,k=1)
        x_knots, y_knots, z_knots = interpolate.splev(tck[0], tck)
        u_fine = np.linspace(0, 1, no_points)
        x_fine, y_fine, z_fine = interpolate.splev(u_fine, tck, der=0)
        # lig_points_osim = np.transpose(np.asarray([x_fine, y_fine, z_fine]))
        xi_nan, yi_nan, zi_nan = x_fine, y_fine, z_fine

    lig_points_osim = xi_nan[np.logical_not(np.isnan(xi_nan))]/1000, yi_nan[np.logical_not(np.isnan(yi_nan))]/1000, zi_nan[np.logical_not(np.isnan(zi_nan))]/1000

    if plot == 1:
        fig2 = plt.figure()
        ax3d = fig2.add_subplot(111, projection='3d')
        ax3d.plot(lig_points[:, 0], lig_points[:, 1], lig_points[:, 2], 'r*')
        # ax3d.plot(x_knots, y_knots, z_knots, 'bo')
        # ax3d.plot(x_fine, y_fine, z_fine, 'go')
        ax3d.scatter(xi_nan[:],yi_nan[:],zi_nan[:],c='g')
        fig2.show()
        plt.show()

    return lig_points_osim

osim_model = r'C:\Users\mariskawesseli\Documents\MOBI\data\S0_2_meniscus_lig.osim'
"""femur"""
# # run ICP to get final position SSM point cloud on original mesh
# mesh1 = trimesh.load_mesh('C:\opensim-jam\jam-resources\jam-resources-main\models\knee_healthy\smith2019\Geometry\smith2019-R-femur-bone_remesh.stl')
# mesh2 = trimesh.load_mesh(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\bone_femur2_2_bone_rot_remesh2.STL')
# # points = trimesh.load_mesh(path + '\SSM_' + segment + '_transform.xyz')
#
# M = trimesh.transformations.scale_and_translate((1,1,-1))
# mesh1.apply_transform(M)
# T = trimesh.transformations.translation_matrix([64.724205, -26.297621, -95.929390])
# origin, xaxis, yaxis, zaxis = [0,0,0], [1, 0, 0], [0, 1, 0], [0, 0, 1]
# Rx = trimesh.transformations.rotation_matrix(-90/(180/np.pi), xaxis)
# Ry = trimesh.transformations.rotation_matrix(90/(180/np.pi), yaxis)
# R = trimesh.transformations.concatenate_matrices(T, Ry, Rx)
# mesh2.apply_transform(R)
# mesh2.export(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\bone_femur2_2_bone_rot_test.STL')
#
# s, fi = trimesh.sample.sample_surface_even(mesh2, 32015, radius=None)
# mesh3 = trimesh.icp[0]imesh(vertices=s, faces=mesh2.faces[fi])
# mesh3.export(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\bone_femur2_2_bone_rot_test.STL')
# sort_index = np.argsort(fi)  # sort and group the face indices
# points = s[sort_index, :]
# faceIndices = fi[sort_index]
# uniqueFaceIndices = np.unique(faceIndices)
# allMeshPatches = trimesh.icp[0]imesh()
# pointGroups = [points[faceIndices == i] for i in uniqueFaceIndices]
# for faceIndex, pointsOnFace in zip(uniqueFaceIndices, pointGroups):
# 	meshpatch = trimesh.icp[0]imesh(mesh2.vertices[mesh2.faces[faceIndex, :]].reshape(3, 3),
# 	                                    np.array([0, 1, 2]).reshape(1, 3))
# 	allMeshPatches += meshpatch
# allMeshPatches.export(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\bone_femur2_2_bone_rot_remesh_test.STL')
#
# kwargs = {"scale": False}
# icp1 = trimesh.registration.icp(mesh2.vertices,mesh1,initial=np.identity(4),threshold=1e-5,max_iterations=20,**kwargs)
#
# kwargs = {"scale": True}
# mesh2.export(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\bone_femur2_2_bone_rot_icp.STL')
# icp = trimesh.registration.icp(mesh2.vertices, mesh1, initial=icp1[0], threshold=1e-5, max_iterations=20,**kwargs)
# mesh2.apply_transform(icp[0])
# mesh2.export(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\bone_femur2_2_bone_rot_icp.STL')
# scale, shear, angles, trans, persp = trimesh.transformations.decompose_matrix(icp[0])
# # mesh1.export(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\test.STL')
#
# ligs = trimesh.load_mesh(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\femur\SSM_femur_pred_points.xyz')
# ligs.apply_transform(R)
# ligs.apply_transform(icp[0])
# np.savetxt(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\femur\SSM_femur_pred_points_osim.xyz', ligs.vertices, delimiter=" ")

#
# ligs =  trimesh.load_mesh(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\femur\SSM_femur_pred_points_osim.xyz')
# PCL = ligs.vertices[0:61]
# al = [0,1,2,3,4,5,7,10,11,13,29,30,31,34,35,39,40,42,43,47,49,50,51,55,56,57,58,59,24,27,28,48]
# pm = [item for item in list(range(61)) if item not in al]
# PCLal_osim = interpolate_lig_points(PCL[al,:],5)
# PCLpm_osim = interpolate_lig_points(PCL[pm,:],5)
# MCLs = ligs.vertices[61:71]
# MCLs_osim = interpolate_lig_points(MCLs,6)
# MCLd = ligs.vertices[71:73]
# MCLd_osim = interpolate_lig_points(MCLd,5)
# post_obl = ligs.vertices[73:81]
# post_obl_osim = interpolate_lig_points(post_obl,5)
# ACL = ligs.vertices[81:100]
# al = [0,1,2,4,7,9,12,15,18]
# pm = [item for item in list(range(19)) if item not in al]
# ACLal_osim = interpolate_lig_points(ACL[al,:],6)
# ACLpm_osim = interpolate_lig_points(ACL[pm,:],6)
# LCL = ligs.vertices[100:105]
# LCL_osim = interpolate_lig_points(LCL,4)
#
# osim_points_fem = np.concatenate([np.asarray(PCLal_osim),np.asarray(PCLpm_osim), np.asarray(MCLs_osim),
#                               np.asarray(MCLd_osim), np.asarray(post_obl_osim), np.asarray(ACLal_osim),
#                               np.asarray(ACLpm_osim), np.asarray(LCL_osim)],1)
# np.savetxt(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\femur\SSM_femur_pred_points_osim_interp.xyz', osim_points_fem, delimiter=" ")
#
# # fig2 = plt.figure()
# # ax3d = fig2.add_subplot(111, projection='3d')
# # ax3d.plot(ACL[al, 0], ACL[al, 1], ACL[al, 2], 'r*')
# # ax3d.plot(ACL[pm, 0], ACL[pm, 1], ACL[pm, 2], 'bo')
# # fig2.show()
# # plt.show()

"""tibia"""
# # run ICP to get final position SSM point cloud on original mesh
# mesh1 = trimesh.load_mesh('C:\opensim-jam\jam-resources\jam-resources-main\models\knee_healthy\smith2019\Geometry\smith2019-R-tibia-bone_remesh.stl')
# mesh2 = trimesh.load_mesh(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\bone_tibia_2_bone_rot_remesh.STL')
# # points = trimesh.load_mesh(path + '\SSM_' + segment + '_transform.xyz')
#
# M = trimesh.transformations.scale_and_translate((1,1,-1))
# mesh1.apply_transform(M)
# T = trimesh.transformations.translation_matrix([101.562462, -72.768566, -17.893391])
# origin, xaxis, yaxis, zaxis = [0,0,0], [1, 0, 0], [0, 1, 0], [0, 0, 1]
# Rx = trimesh.transformations.rotation_matrix(-90/(180/np.pi), xaxis)
# Ry = trimesh.transformations.rotation_matrix(90/(180/np.pi), yaxis)
# R = trimesh.transformations.concatenate_matrices(Ry, Rx)
# mesh2.apply_transform(T)
# mesh2.apply_transform(R)
# mesh2.export(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\bone_tibia_2_bone_rot_remesh_test.STL')
#
# kwargs = {"scale": False}
# icp1 = trimesh.registration.icp(mesh2.vertices,mesh1,initial=np.identity(4),threshold=1e-5,max_iterations=20,**kwargs)
#
# kwargs = {"scale": True}
# # mesh2.export(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\bone_tibia_2_bone_rot_icp.STL')
# icp = trimesh.registration.icp(mesh2.vertices, mesh1, initial=icp1[0], threshold=1e-5, max_iterations=20,**kwargs)
# mesh2.apply_transform(icp[0])
# mesh2.export(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\bone_tibia_2_bone_rot_icp.STL')
# scale, shear, angles, trans, persp = trimesh.transformations.decompose_matrix(icp[0])
# mesh1.export(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\test_tib.STL')
#
# ligs = trimesh.load_mesh(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\SSM_tibia_short_pred_points.xyz')
# ligs.apply_transform(T)
# ligs.apply_transform(R)
# ligs.apply_transform(icp[0])
# np.savetxt(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\tibia\SSM_tibia_short_pred_points_osim.xyz', ligs.vertices, delimiter=" ")


ligs = trimesh.load_mesh(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\tibia\SSM_tibia_short_pred_points_osim.xyz')
PCL = ligs.vertices[0:71]
al = [0,1,2,4,6,7,10,12,13,19,21,22,23,24,25,26,27,31,32,34,35,40,45,46,47,50,52,56,59,60,61,62,63,65,70]
pm = [item for item in list(range(71)) if item not in al]
PCLal_osim = interpolate_lig_points(PCL[al,:],5)
PCLpm_osim = interpolate_lig_points(PCL[pm,:],5)
MCLd = ligs.vertices[71:82]
MCLd_osim = interpolate_lig_points(MCLd,5)
post_obl = ligs.vertices[82:86]
post_obl_osim = interpolate_lig_points(post_obl,5)
ACL = ligs.vertices[86:150]
al = [0,1,2,4,6,7,10,12,13,19,21,22,23,24,25,26,27,31,32,34,35,40,45,46,47,50,52,56,59,60,61,62,63]
pm = [item for item in list(range(64)) if item not in al]
ACLal_osim = interpolate_lig_points(ACL[al,:],6)
ACLpm_osim = interpolate_lig_points(ACL[pm,:],6)

osim_points_tib = np.concatenate([np.asarray(PCLal_osim),np.asarray(PCLpm_osim),
                              np.asarray(MCLd_osim), np.asarray(post_obl_osim), np.asarray(ACLal_osim),
                              np.asarray(ACLpm_osim)],1)
# np.savetxt(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\tibia\SSM_tibia_pred_points_osim_interp.xyz', osim_points_tib, delimiter=" ")


"""fibula"""
# run ICP to get final position SSM point cloud on original mesh
# mesh OpenSim model -  make sure this is high quality
mesh1 = trimesh.load_mesh('C:\opensim-jam\jam-resources\jam-resources-main\models\knee_healthy\smith2019\Geometry\smith2019-R-fibula-bone_remesh.stl')
# mesh segmented from MRI
mesh2 = trimesh.load_mesh(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\bone_fibula_1_tissue_rot_remesh.STL')

# Mirror if needed (only for left as SSM/model is right? - check how to deal with left model)
M = trimesh.transformations.scale_and_translate((1,-1,1))
mesh2.apply_transform(M)
# Rotate segmented bone (check why needed?)
origin, xaxis, yaxis, zaxis = [0,0,0], [1, 0, 0], [0, 1, 0], [0, 0, 1]
Rx = trimesh.transformations.rotation_matrix(-90/(180/np.pi), xaxis)
Ry = trimesh.transformations.rotation_matrix(-90/(180/np.pi), yaxis)
Rz = trimesh.transformations.rotation_matrix(180/(180/np.pi), zaxis)
R = trimesh.transformations.concatenate_matrices(Ry, Rx, Rz)
mesh2.apply_transform(R)
# Translate segmented mesh to OpenSim bone location
T = trimesh.transformations.translation_matrix(mesh1.center_mass-mesh2.center_mass)
mesh2.apply_transform(T)
mesh2.export(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\bone_fibula_1_tissue_rot_remesh_test.STL')

# ICP to fit segmented bone to OpenSim mesh
kwargs = {"scale": False}
icp1 = trimesh.registration.icp(mesh2.vertices,mesh1,initial=np.identity(4),threshold=1e-5,max_iterations=20,**kwargs)
kwargs = {"scale": True}
icp = trimesh.registration.icp(mesh2.vertices, mesh1, initial=icp1[0], threshold=1e-5, max_iterations=20,**kwargs)
mesh2.apply_transform(icp[0])
mesh2.export(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\bone_fibula_1_tissue_rot_remesh_icp.STL')
scale, shear, angles, trans, persp = trimesh.transformations.decompose_matrix(icp[0])

ligs = trimesh.load_mesh(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\SSM_fibula_short_pred_points.xyz')
ligs.apply_transform(M)
ligs.apply_transform(R)
ligs.apply_transform(T)
ligs.apply_transform(icp[0])
np.savetxt(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\fibula\SSM_fibula_pred_points_osim.xyz', ligs.vertices, delimiter=" ")

ligs = trimesh.load_mesh(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\fibula\SSM_fibula_pred_points_osim.xyz')

LCL = ligs.vertices[0:79]
LCL_osim = interpolate_lig_points(LCL,4)

osim_points_fib = np.concatenate([np.asarray(LCL_osim)],1)
# np.savetxt(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\fibula\SSM_fibula_pred_points_osim_interp.xyz', osim_points_tib, delimiter=" ")
