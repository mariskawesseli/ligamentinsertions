import os
import vtk
import trimesh
import numpy as np
import seaborn as sns


def load_stl(filename):
	reader = vtk.vtkSTLReader()
	reader.SetFileName(filename)

	mapper = vtk.vtkPolyDataMapper()
	if vtk.VTK_MAJOR_VERSION <= 5:
		mapper.SetInput(reader.GetOutput())
	else:
		mapper.SetInputConnection(reader.GetOutputPort())

	actor = vtk.vtkActor()
	actor.SetMapper(mapper)

	return actor


subjects = ['9'] #['9','13','19','23','26','29','32','35','37','41'] #, S0 [100] #

segments = ['tibia']  #'femur',
ligaments_fem = [[1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
				 [6, 5, 6, 6, 6, 6, 4, 4, 5, 5],
				 [3, 2, 5, 3, 3, 2, 2, 0, 3, 3],
				 [0, 8, 0, 0, 0, 0, 0, 0, 0, 0],  # MCLd2
				 [7, 3, 7, 7, 7, 5, 7, 6, 7, 0],
				 [0, 0, 8, 0, 0, 0, 0, 0, 0, 0],  # POL2
				 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # POL3
				 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # POL4
				 [4, 6, 3, 5, 4, 0, 0, 3, 4, 4],
				 [5, 7, 4, 4, 5, 7, 6, 5, 6, 6],
				 [2, 4, 2, 2, 2, 3, 3, 2, 2, 2]]

ligaments_tib = [[5, 7, 6, 5, 3, 4, 4, 5, 5, 4],
				 [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
				 [3, 3, 8, 3, 5, 3, 5, 0, 3, 3],
				 [0, 4, 0, 0, 0, 0, 0, 0, 0, 0],  # MCLd2
				 [4, 5, 3, 4, 4, 5, 3, 2, 4, 0],
				 [0, 6, 4, 0, 0, 0, 0, 0, 0, 0],  # POL2
				 [0, 0, 5, 0, 0, 0, 0, 0, 0, 0],  # POL3
				 [0, 0, 7, 0, 0, 0, 0, 0, 0, 0],  # POL4
				 [6, 8, 9, 6, 6, 6, 6, 6, 6, 5],
				 [2, 2, 2, 2, 2, 2, 2, 3, 2, 2],
				 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]

ligaments_fib = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # PCL
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # MCLp
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # MCLd
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # MCLd2
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # POL
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # POL2
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # POL3
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # POL4
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # ACL
                 [2, 2, 2, 2, 2, 2, 2, 3, 2, 2],  # LCL
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]  # POP

for segment in segments:
	SSMpoints = [[] for i in range(11)]
	for ind in range(0,11):
		SSMpoints[ind] = [[] for i in range(10)]

	for ind, subject in enumerate(subjects):
		if subject==100:
			path = os.path.join(r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone\new_bone\shape_models')
		elif subject == 'S0':
			path = os.path.join(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim')
		else:
			path = os.path.join(r"C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData", str(subject))

		if subject in [9, 13, 26, 29, 32]:
			side = 'R'
			reflect = ''
		else:
			side = 'L'
			reflect = '.reflect'

		# points = trimesh.load_mesh(r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\femur_bone\new_bone\shape_models\meanshape_bone_no_lig.xyz')
		# point_cloud = create_pointcloud_polydata(points)
		# pointCloud = VtkPointCloud()
		# pointCloud = load_data(point_cloud, pointCloud)
		# points_lig = trimesh.load_mesh(r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\femur_bone\new_bone\shape_models\meanshape_ligs.xyz')
		if subject==100:
			# points_lig = trimesh.load_mesh(path + '\meanshape_ligs.xyz')
			# point_cloud_lig = create_pointcloud_polydata(points_lig)
			bone_actor = load_stl(path + '/mean_shape.stl')
			# bone_actor.GetProperty().SetOpacity(0.75)
		else:
			if subject == 'S0':
				# bone_actor = load_stl(path + '/bone_femur2_2_bone_rot.stl')
				bone_actor = load_stl(path + '/bone_tibia_2_bone_rot.stl')
			else:
				bone_actor = load_stl(path + '/Segmentation_' + segment + '_transform.stl')
				if segment == 'fibula':
					segment_temp = 'tibia'
				else:
					segment_temp = segment
				wire_actor = load_stl(path + '/Segmentation_' + segment_temp + '_wires.stl')
				wire_actor.GetProperty().SetColor(1, 1, 0)
			bone_actor.GetProperty().SetOpacity(0.85)

		# actor.GetProperty().SetOpacity(1.0)
		bone_actor.GetProperty().SetColor(0.89, 0.85, 0.79)
		# bone_actor.GetProperty().LightingOff()

		c = sns.color_palette("viridis_r", n_colors=10, as_cmap=False)
		lut = vtk.vtkLookupTable()
		lut.SetNumberOfColors(10)
		lut.SetTableRange(1, 10)
		for j in range(0,10):
			lut.SetTableValue(int(j), c[j][0], c[j][1], c[j][2])

		legend = vtk.vtkScalarBarActor()
		legend.SetNumberOfLabels(10)
		lut.SetTableRange(1, 10)
		legend.SetLookupTable(lut)
		# pos = legend.GetPositionCoordinate()
		# pos.SetCoordinateSystemToNormalizedViewport()
		legend.SetTitle("Specimens \n")

		text_prop_cb = legend.GetLabelTextProperty()
		text_prop_cb.SetFontFamilyAsString('Arial')
		text_prop_cb.SetFontFamilyToArial()
		text_prop_cb.SetColor(0,0,0)
		# text_prop_cb.SetFontSize(500)
		text_prop_cb.ShadowOff()
		legend.SetLabelTextProperty(text_prop_cb)
		legend.SetMaximumWidthInPixels(75)
		legend.SetMaximumHeightInPixels(300)
		legend.SetTitleTextProperty(text_prop_cb)
		legend.SetPosition(0.85,0.6)

		axes = vtk.vtkAxesActor()
		axes.SetTotalLength(75,75,100)
		axes.SetXAxisLabelText('M-L')
		axes.SetYAxisLabelText('A-P')
		axes.SetZAxisLabelText('S-I')
		axes.GetXAxisCaptionActor2D().GetTextActor().SetTextScaleMode(vtk.vtkTextActor.TEXT_SCALE_MODE_NONE)
		axes.GetXAxisCaptionActor2D().GetCaptionTextProperty().SetFontSize(25)
		axes.GetYAxisCaptionActor2D().GetTextActor().SetTextScaleMode(vtk.vtkTextActor.TEXT_SCALE_MODE_NONE)
		axes.GetYAxisCaptionActor2D().GetCaptionTextProperty().SetFontSize(25)
		axes.GetZAxisCaptionActor2D().GetTextActor().SetTextScaleMode(vtk.vtkTextActor.TEXT_SCALE_MODE_NONE)
		axes.GetZAxisCaptionActor2D().GetCaptionTextProperty().SetFontSize(25)

		# Renderer
		renderer = vtk.vtkRenderer()
		# renderer.AddActor(actor)
		renderer.AddActor(bone_actor)
		# if not subject == 100 and not subject == 'S0':
		# 	renderer.AddActor(wire_actor)
		# renderer.AddActor(legend)
		renderer.AddActor(axes)
		# renderer.SetBackground(.2, .3, .4)
		renderer.SetBackground(1.0, 1.0, 1.0)
		renderer.ResetCamera()
		# light = vtk.vtkLight()
		# light.SetIntensity(1)
		# renderer.AddLight(light)

		# Render Window
		renderWindow = vtk.vtkRenderWindow()
		renderWindow.AddRenderer(renderer)
		renderWindow.SetSize(750, 750)

		# Interactor
		renderWindowInteractor = vtk.vtkRenderWindowInteractor()
		renderWindowInteractor.SetRenderWindow(renderWindow)
		renderWindowInteractor.GetInteractorStyle().SetCurrentStyleToTrackballCamera()

		# Begin Interaction
		renderWindow.Render()
		renderWindow.SetWindowName("XYZ Data Viewer " + str(subject))
		renderWindowInteractor.Start()
