import os
import vtk

# Define the input and output directories
input_dir = r"C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\tibia_bone_short\new_bone\shape_models"
output_dir = input_dir

# Create a VTK STL reader
stl_reader = vtk.vtkSTLReader()

# Loop through all STL files in the input directory
for filename in os.listdir(input_dir):
    if filename.endswith("mean_shape.stl"):
        # Load the STL file
        stl_path = os.path.join(input_dir, filename)
        stl_reader.SetFileName(stl_path)
        stl_reader.Update()

        # Convert the STL data to polydata
        polydata_filter = vtk.vtkDataSetSurfaceFilter()
        polydata_filter.SetInputConnection(stl_reader.GetOutputPort())
        polydata_filter.Update()
        polydata = polydata_filter.GetOutput()

        # Modify the VTK file header to version 4.2
        header = "# vtk DataFile Version 4.2\n"

        # Save the polydata as a VTK file in format 4.2
        vtk_path = os.path.join(output_dir, filename.replace(".stl", ".vtk"))
        vtk_writer = vtk.vtkPolyDataWriter()
        vtk_writer.SetFileName(vtk_path)
        vtk_writer.SetInputData(polydata)
        vtk_writer.SetFileTypeToBinary()
        vtk_writer.SetHeader(header)
        vtk_writer.Update()
