import os
import vtk
import trimesh
import numpy as np
from vtk.util.numpy_support import vtk_to_numpy, numpy_to_vtk
import seaborn as sns

segment = 'femur'
renderer = vtk.vtkRenderer()

rw = vtk.vtkRenderWindow()
# xmins = [0, .5, 0, .5, 0, .5]
# xmaxs = [0.5, 1, 0.5, 1, .5, 1]
# ymins = [.66, .66, .33, .33, 0, 0, ]
# ymaxs = [1, 1, .66, .66, 0.33, 0.33]

xmins = [0, 0, .33, .33, .66, .66]
xmaxs = [.33, .33, .66, .66, 1, 1]
ymins = [0, .5, 0, .5, 0, .5]
ymaxs = [0.5, 1, 0.5, 1, .5, 1]
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(rw)

tel=0

for modes in range(1,4):
    path = os.path.join(r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + r'_bone/')
    mean_shape = 'mean_shape.stl'
    mode_plus = 'mode' + str(modes) + '_+2SD_8192.stl'
    mode_min = 'mode' + str(modes) + '_-2SD_8192.stl'

    # determine signed distance
    plus2sd = trimesh.load_mesh(path + mode_plus)
    min2sd = trimesh.load_mesh(path + mode_min)
    mean = trimesh.load_mesh(path + mean_shape)
    # signed_distance = trimesh.proximity.signed_distance(plus2sd, min2sd.vertices) # improve this?
    # signed_distance2 = trimesh.proximity.signed_distance(plus2sd, mean.vertices)
    # signed_distance3 = trimesh.proximity.signed_distance(min2sd, mean.vertices)
    # signed_distance = signed_distance2 + -signed_distance3
    signed_distance4 = trimesh.proximity.signed_distance(plus2sd, min2sd.vertices)
    signed_distance = signed_distance4

    # load mesh via trimesh to get the correct order for distance transform
    reader = vtk.vtkSTLReader()
    reader.SetFileName(path + mode_min)
    reader.Update()
    obj = reader.GetOutputDataObject(0)

    # create lookup table
    c = sns.diverging_palette(25, 262, s=60, n=100, as_cmap=False)
    lut = vtk.vtkLookupTable()
    lut.SetNumberOfColors(100)
    lut.SetTableRange(max(abs(signed_distance))*-1, max(abs(signed_distance)))
    for j in range(0,100):
        lut.SetTableValue(int(j), c[j][0], c[j][1], c[j][2])
    lut.Build()

    # fix order signed distance transform as these are from trimesh
    vtk_nodes = vtk_to_numpy(obj.GetPoints().GetData())
    trimesh_nodes = min2sd.vertices
    dist = np.zeros((obj.GetNumberOfPoints(),1))
    for i in range(obj.GetNumberOfPoints()):
        # np.linalg.norm(vtk_nodes - trimesh_nodes)
        # idx = (np.hypot(*(vtk_nodes - trimesh_nodes[i]).T)).argmin()
        result = np.where((vtk_nodes[:,0] == trimesh_nodes[i][0]) & (vtk_nodes[:,1] == trimesh_nodes[i][1]) & (vtk_nodes[:,2] == trimesh_nodes[i][2]))
        # result = idx
        dist[result[0][0]] = signed_distance[i]

    vtk_dist = vtk.vtkDoubleArray()
    # z = np.zeros((obj.GetNumberOfPoints(),1))
    for i in range(obj.GetNumberOfPoints()):
        vtk_dist.InsertNextValue(dist[i])
    obj.GetPointData().SetScalars(vtk_dist)

    # mapper
    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputDataObject(obj)
    mapper.SetScalarRange(max(abs(signed_distance))*-1, max(abs(signed_distance)))
    mapper.SetLookupTable(lut)
    mapper2 = vtk.vtkPolyDataMapper()
    mapper2.SetInputDataObject(obj)
    mapper2.SetScalarRange(max(abs(signed_distance))*-1, max(abs(signed_distance)))
    mapper2.SetLookupTable(lut)

    if segment == 'fibula':
        d = -40
    else:
        d = -100
    # translation
    transform = vtk.vtkTransform()
    transform.Identity()
    # transform.Translate(0,modes * d, 0)
    transform.RotateX(90)
    transform.RotateY(180)
    transform.RotateZ(0)
    transformFilter = vtk.vtkTransformPolyDataFilter()
    transformFilter.SetInputConnection(reader.GetOutputPort())
    transformFilter.SetTransform(transform)
    transformFilter.Update()

    transform2 = vtk.vtkTransform()
    transform2.Identity()
    # transform2.Translate(d*-1, modes*d, 0)
    transform2.RotateX(90)
    transform2.RotateY(180)
    transform2.RotateZ(-90)
    transformFilter2 = vtk.vtkTransformPolyDataFilter()
    transformFilter2.SetInputConnection(reader.GetOutputPort())
    transformFilter2.SetTransform(transform2)
    transformFilter2.Update()

    # actors
    bone_actor = vtk.vtkActor()
    bone_actor.SetMapper(mapper)
    mapper.SetInputConnection(transformFilter.GetOutputPort())
    bone_actor.SetMapper(mapper)
    legend = vtk.vtkScalarBarActor()
    legend.SetLookupTable(lut)
    bone_actor2 = vtk.vtkActor()
    mapper2.SetInputConnection(transformFilter2.GetOutputPort())
    bone_actor2.SetMapper(mapper2)

    for ind in range(2):
        ren = vtk.vtkRenderer()
        rw.AddRenderer(ren)
        ren.SetViewport(xmins[tel], ymins[tel], xmaxs[tel], ymaxs[tel])

        # Share the camera between viewports.
        if tel == 0:
            camera = ren.GetActiveCamera()
        else:
            ren.SetActiveCamera(camera)

        # Create a mapper and actor
        if tel == 0 or tel == 2 or tel == 4:
            ren.AddActor(bone_actor)
            # ren.AddActor(actor2lig)
        else:
            ren.AddActor(bone_actor2)
            # ren.AddActor(actor3)

        ren.SetBackground(1.0, 1.0, 1.0)

        ren.ResetCamera()

        tel+=1

    # # Renderer
    # renderer.AddActor(bone_actor)
    # renderer.AddActor(bone_actor2)
    # # renderer.AddActor(legend)
    # renderer.SetBackground(1.0, 1.0, 1.0)
    # renderer.ResetCamera()

# # Render Window
# renderWindow = vtk.vtkRenderWindow()
# renderWindow.AddRenderer(renderer)
# renderWindow.SetSize(750, 750)
#
# # Interactor
# renderWindowInteractor = vtk.vtkRenderWindowInteractor()
# renderWindowInteractor.SetRenderWindow(renderWindow)
# renderWindowInteractor.GetInteractorStyle().SetCurrentStyleToTrackballCamera()
#
# # Begin Interaction
# renderWindow.Render()
# renderWindow.SetWindowName("SSM distances")
# renderWindowInteractor.Start()

rw.Render()
rw.SetWindowName('MultipleViewPorts')
rw.SetSize(850, 400)
iren.GetInteractorStyle().SetCurrentStyleToTrackballCamera()
iren.Start()