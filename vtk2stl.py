import os
import vtk

# Define the input and output directories
input_dir = r"C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\tibia_bone_short"
output_dir = r"C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\tibia_bone_short"

# Create a renderer and a render window
renderer = vtk.vtkRenderer()
render_window = vtk.vtkRenderWindow()
render_window.AddRenderer(renderer)

# Loop through all VTK files in the input directory
for filename in os.listdir(input_dir):
    if filename.endswith(".vtk"):
        # Load the VTK file
        vtk_path = os.path.join(input_dir, filename)
        reader = vtk.vtkDataSetReader()
        reader.SetFileName(vtk_path)
        reader.Update()
        polydata = reader.GetOutput()

        # Convert the polydata to a stl file
        stl_path = os.path.join(output_dir, filename.replace(".vtk", ".stl"))
        writer = vtk.vtkSTLWriter()
        writer.SetFileName(stl_path)
        writer.SetInputData(polydata)
        writer.Write()

        # Add the polydata to the renderer for visualization
        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInputData(polydata)
        actor = vtk.vtkActor()
        actor.SetMapper(mapper)
        renderer.AddActor(actor)

# Set up the interactor and start the rendering loop
interactor = vtk.vtkRenderWindowInteractor()
interactor.SetRenderWindow(render_window)
render_window.Render()
interactor.Start()
